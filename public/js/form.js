/*
 * функции для работы с формами
 */
function resetForm(id) {
        
        
		var $form = $(id);

		$form.find("input:text").val('');
		$form.find("input[type='number']").val('');	
		$form.find("input[type='date']").val('');
		$form.find('select').prop('selectedIndex', 0);
        
        if (typeof(map) !== 'undefined')
            if (map.userMarker) {
                map.removeUserMarker();
                map.eraseUserMarker();
            }
}
