function initHotelMap() {
		
		// as a suggestion you could use the event listener to save the state when zoom changes or drag ends
		
        var center, mapProps;
        
        if (typeof(providerHotelData) != 'undefined')
        {
            center = {lat: providerHotelData.position.lat, lng: providerHotelData.position.lng };
            zoom = 14;
            
        }
        else {
                
            var savedMapProps = loadMapState();

            var defaultProps = {lat: 55, lng: 50, zoom: 3 };
                
            center = { 
                lat: savedMapProps.lat ? savedMapProps.lat : defaultProps.lat,
                lng: savedMapProps.lng ? savedMapProps.lng : defaultProps.lng
            };

            zoom = savedMapProps.zoom ? savedMapProps.zoom : defaultProps.zoom;
        }
		
        mapProps = {
                zoom: zoom,
                center: center,
                scrollwheel: false
        };
                
		map = new google.maps.Map(document.getElementById('hotels-gmap'), mapProps);
        
        // поиск с автозаполнением на карте
        
        if (google.maps.places) {
            
            var input = document.getElementById('pac-input');
            
            var searchBox = new google.maps.places.SearchBox(input);
        
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // при выборе места смещаемся к координатам
            searchBox.addListener('places_changed', function(e) {

               var location = searchBox.getPlaces()[0].geometry.location;

               map.setCenter({lat: location.lat(), lng: location.lng()});
               
               saveMapState();
               //console.log(searchBox.getPlaces()); 
            });
        }
        
        
		map.setMarkerRadius = function (){
			
			if (map.userMarker && map.userMarker.circle)
				map.userMarker.circle.setRadius(searchDistance);
		};
		
		map.removeUserMarker = function () {
			map.userMarker.setMap(null);
			map.userMarker.circle.setMap(null);
		}
		//google.maps.event.addListener(map, 'tilesloaded', tilesLoaded);
		
		// выставляем маркеры отелей
        
        if (typeof(markerData) != 'undefined') 
            markerData.forEach(function (data){

                marker = new google.maps.Marker({
                      position: data.position,
                      title: data.title,
                      map: map,
                      url: data.url,
                      opacity: 0.6
                 });

                // переход к отелю 
                marker.addListener('click', function() {
                      window.location.href = this.url;
                });
                // выделение при наведении
                marker.addListener('mouseover', function() {
                      this.setOpacity(1);
                });
                marker.addListener('mouseout', function() {
                      this.setOpacity(0.6);
                });
            });
		
        // а также маркеры похожих отелей если есть
        // 
        if (typeof(simsMarkerData) != 'undefined') 
            
            simsMarkerData.forEach(function (data){

                marker = new google.maps.Marker({
                      position: data.position,
                      title: data.title,
                      map: map,
                      icon: '/images/sim-hotel-marker.png'
                 });
            });
        
        // ставим маркер отеля поставщика если есть
        
        if (typeof(providerHotelData) != 'undefined') 
            
            new google.maps.Marker({
                position: providerHotelData.position, 
                map: map,
                icon: '/images/p-hotel-marker.png',
                title: providerHotelData.title
            });
                    
		// есть ли в куках пользовательский маркер?
		
		var userMarker = loadUserMarker();
		
		if (userMarker) {
			placeMarker(map, userMarker);
			setLocation(userMarker);
			
			google.maps.event.addListener(map.userMarker, 'dragend', function(event) {
		
				// сохраняем координаты в форме поиска
				setLocation({lat: event.latLng.lat(), lng: event.latLng.lng()});
				// запоминаем в куки
				saveUserMarker(event.latLng);
		
            });

		}
		
		// изменения параметров карты сохраняются в куки
		google.maps.event.addListener(map, 'zoom_changed', saveMapState);
		google.maps.event.addListener(map, 'dragend', saveMapState);
			
		// Пользовательская отметка на карте по клику
		google.maps.event.addListener(map, 'click', function(event) {
		
			if (map.userMarker) {
				map.removeUserMarker();
				
			}

			// ставим пользовательский маркер
			placeMarker(map, event.latLng);
			// сохраняем координаты в форме поиска
			setLocation({lat: event.latLng.lat(), lng: event.latLng.lng()});
			// запоминаем в куки
			saveUserMarker(event.latLng);
		
		});
		
		function placeMarker(map, location) {
			
			map.userMarker = new google.maps.Marker({
				position: location, 
				map: map,
				draggable: true,
				icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
			});
			
			map.userMarker.circle = new google.maps.Circle({
				map: map,
				radius: searchDistance,    // 10 miles in metres
				strokeColor: '#FF0000',
				strokeOpacity: 0.4,
				strokeWeight: 1,
				fillColor: '#FF0000',
				fillOpacity: 0.1
			});
			map.userMarker.circle.bindTo('center', map.userMarker, 'position');

			google.maps.event.addListener(map.userMarker, 'click', function(event) {
				map.userMarker.setMap(null);
				map.userMarker.circle.setMap(null);
				
				map.eraseUserMarker();
				$('#radius-field').attr('disabled', true); // будет передаваться?
				$('#coords').val('');
			});
			
			$radiusField = $('#radius-field');
			$radiusField.removeAttr('disabled');
			
			
			if (!$radiusField.val())
				$radiusField.val(searchDistance);
		}
		
		function setLocation(latLng) {
			//console.log(latLng);
			
			var coords = latLng.lat  + ',' + latLng.lng;
			
			$('input[name="coords"]').val(coords);
		};
		  

		// functions below

		function saveMapState(event) { 
			var mapZoom=map.getZoom(); 
			var mapCentre=map.getCenter(); 
			var mapLat=mapCentre.lat(); 
			var mapLng=mapCentre.lng(); 
			var cookiestring=mapLat+"_"+mapLng+"_"+mapZoom; 
			setCookie("mtl_gMapCookie",cookiestring, 30); 
		} 

		function loadMapState() { 
			var gotCookieString=getCookie("mtl_gMapCookie"); 
			var splitStr = gotCookieString.split("_");
			var savedMapLat = parseFloat(splitStr[0]);
			var savedMapLng = parseFloat(splitStr[1]);
			var savedMapZoom = parseFloat(splitStr[2]);
			
			return {
				lat: !isNaN(savedMapLat) ? savedMapLat : null,
				lng: !isNaN(savedMapLng) ? savedMapLng : null,
				zoom: !isNaN(savedMapZoom) ? savedMapZoom : null
			}
		}
		
		function loadUserMarker() {
			var gotCookieString=getCookie("mtl_gMapUserMark"); 
			var splitStr = gotCookieString.split("_");
			var savedLat = parseFloat(splitStr[0]);
			var savedLng = parseFloat(splitStr[1]);
			
			if (!isNaN(savedLat) && !isNaN(savedLng))
			
				return { lat: savedLat, lng: savedLng }
		}
		
		function saveUserMarker(location) {
			var cookiestring = location.lat() + "_" + location.lng(); 
			setCookie("mtl_gMapUserMark", cookiestring, 30); 
		}
		
		map.eraseUserMarker = function () {
			eraseCookie('mtl_gMapUserMark');
		}
		
	}
    