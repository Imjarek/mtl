<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'login' => 'Вход',
	'registration' => 'Регистрация',
	'name' => 'Имя',
	'email-address' => 'E-Mail адрес',
	'password' => 'Пароль',
	'confirm-password' => 'Подтвердить пароль',
	'submit' => 'Регистрация',
    'failed' => 'Введенные учетные данные неверны',
    'throttle' => 'Превышено количество попыток входа. Попробуйте через  :seconds секунд',
	
	'remember' => 'Запомнить меня',
	
	'forgotten' => 'Забыли пароль?'
];
