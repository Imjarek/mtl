@extends('layouts.app')

@section('content')
 
<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
    
<div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
		
        <div class="panel panel-default">
            <div class="panel-heading">
                
			{{ $country->name_ru }}
			</div>
	
	<div class="panel-body">	

	@if (count($provider_countries) > 0)
		<h4>Страна у поставщиков</h4>
		<table class="table table-striped task-table">

            <thead>
				<th>Поставщик</th>
                <th>Идентификатор страны у поставщика</th>
            </thead>
            <tbody>
				@foreach ($provider_countries as $country)
					<tr>
						<td>
						   {{ $country->provider->name }}
						</td>
						<td class="table-text">
							{{ $country->provider_country_id }}
					   </td>
					</tr>
				@endforeach
                    </tbody>
                </table>
	
	@else <h4>Этот страна не связана со странами поставщиков</h4>
	@endif
	</div>
	     
	</div>	    

@endsection