@extends('layouts.app')

@section('content')

   <script>
	
	searchDistance = {{ old('radius') ? old('radius') : 50  }};
	
	markerData = [];
	
	@foreach ($hotels as $hotel)
		@if ($hotel->latitude && $hotel->longitude)
			markerData.push({
				position: {lat: {{ $hotel->latitude}}, lng: {{ $hotel->longitude }} },
				title: "{{ $hotel->hotel_name }} ({{ $hotel->city_name or '' }})",
				url: '/provider_hotel/{{ $hotel->hotel_id }}'
			});
		@endif

	@endforeach
</script>

<script type='text/javascript' src='{{ URL::asset('js/init_hotel_map.js') }}'></script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMLm-fqO3_-sT-m0oFbY-a7CvBpyopqVI&callback=initHotelMap&libraries=places"/>
	
</script>
    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
	<div class="col-md-7">
		<input id="pac-input" class="controls" type="text" placeholder="Искать">
	    <div class="gmap" id="hotels-gmap"></div>
	</div>
        <form action="/provider_hotels" method="GET" class="form-vertical" id="search-hotels-form">
            {{ csrf_field() }}
		<div class="col-md-2">
		<div class="form-group">
			
		    <label for="radius-field" control-label>Радиус
				<i class="fa fa-question-circle-o fa-fw" data-toggle="tooltip" title="Укажите точку на карте и радиус в метрах для поиска отелей">
			</i></label>
			<input disabled type="number" name="radius" id="radius-field" class="form-control" value = "{{ old('radius') }}" min="50" max="5000000" step="10">
			<label for="hotel-id-field" control-label>№ MTL</label>
			<input type="number" name="hotel_id" id="hotel-id-field" class="form-control" value = "{{ old('hotel_id') }}">
			
			<label for="provider-hotel-id-field" control-label>Код поставщика</label>
			<input type="text" name="provider_hotel_id" id="provider-hotel-id-field" class="form-control" value = "{{ old('provider_hotel_id') }}">
			
			<label for="hotel-name-field" control-label>Отель</label>
			<input type="text" name="hotel_name" id="hotel-name-field" class="form-control" value = "{{ old('hotel_name') }}">

			<label for="country-field" control-label>Страна</label>
			<input type="text" name="country" id="country-field" class="form-control" value="{{ old('country') }}">

			<label for="city-field" control-label>Город</label>
			<input type="text" name="city" id="city-field" class="form-control" value="{{ old('city') }}">
			<label for="provider-field" control-label">Поставщик</label>
			<select class="form-control" name="provider_code" id="provider_field">
				<option {{ old('provider_code') ? '' : 'selected' }} value="">Все</option>
				<option value="ostrovok" {{ old('provider_code') == 'ostrovok' ? 'selected' : '' }}>Островок</option>
				<option value="acase" {{ old('provider_code') == 'acase' ? 'selected' : '' }}>Академсервис</option>
				<option value="bvk" {{ old('provider_code') == 'bvk' ? 'selected' : '' }}>Броневик</option>
				<option value="amadeus" {{ old('provider_code') == 'amadeus' ? 'selected' : '' }}>Амадеус</option>
				<option value="bol" {{ old('provider_code') == 'bol' ? 'selected' : '' }}>Beds Online</option>
				<option value="hotelbook" {{ old('provider_code') == 'hotelbook' ? 'selected' : '' }}>HotelBook</option>
			    </select>
			<label control-label for="date-before-field">Добавлен</label><br>
			<input type="date" name="date_before" id="date-before-field" class="form-control" value="{{ old('date_before') }}">
			<br>
			<input type="date" name="date_after" id="date-after-field" class="form-control" value="{{ old('date_after') }}">
			<label for="match-field" control-label>Есть совпадения</label>
			<select class="form-control" name="matches">
				<option {{ !old('matches') ? 'selected' : '' }} value="">Все</option>
				<option {{ old('matches') == 'yes' ? 'selected' : '' }} value="yes">Да</option>
				<option {{ old('matches') == 'no' ? 'selected' : '' }} value="no">Нет</option>
			</select>
			<br>
			<input type="hidden" name="sorting" value="{{ old('sorting') }}">
			<input type="hidden" name="sorting_param" value="{{ old('sorting_param') }}">
			<input type="hidden" name="coords" value="{{ old('coords') }}">
			
			<button class="btn btn-block btn-alert" type="button" onclick="resetForm('#search-hotels-form')">
				<i class="fa fa-times fa-fw"></i>Сбросить фильтры
			</button>
			</div>		
		</div>	
		<div class="col-md-2" style="height:360px;">
			<button type="submit" class="btn btn-block btn-info">
				<i class="fa fa-search fa-fw"></i>Найти
			</button>
					
		    </div>
		
        </form>
	</div>
	</div>
	
		@if (count($hotels) > 0)
		<div class="col-md-12">
		    <p><strong>Всего отелей: {{ $total }}</strong></p>
		    @if (method_exists($hotels, 'total'))
			<p><strong>Найдено отелей: {{ $hotels->total() }}</strong></p>
			@endif
		</div>
		@endif
		
		@if (count($hotels) > 0)
		
		<div class="page-links">{{ $hotels->appends(Request::except('page'))->links() }}</div>
        <div class="panel panel-default">
           		
            <div class="panel-body">
                <table class="table table-striped table-hover hotel-table">

                    <!-- Table Headings -->
                    <thead>
						<th>
							<a class="sorting-switch" data-sorting-param="hotel_id">№ MTL</a>
							<b id="sorting-icon" class="{{ old('sorting_param') == 'hotel_id' ? old('sorting') : ''}}"></b></th>
						<th>Код поставщика</th>
						<th>
							<a class="sorting-switch" data-sorting-param="hotel_name">Отель</a>
							<b id="sorting-icon" class="{{ old('sorting_param') == 'hotel_name' ? old('sorting') : ''}}"></b>
						</th>
						<th>Страна</th>
						<th>
							<a class="sorting-switch" data-sorting-param="city_name">Город</a>
							<b id="sorting-icon" class="{{ old('sorting_param') == 'city_name' ? old('sorting') : ''}}"></b>
						</th>
						<th>Поставщик</th>
						<th>
							<a class="sorting-switch" data-sorting-param="created_at">Добавлен</a>
							<b id="sorting-icon" class="{{ old('sorting_param') == 'created_at' ? old('sorting') : ''}}"></b>
						</th>
						<th>Совпадения</th>

                    </thead>

                    <!-- Table Body -->
                    <tbody>
                        @foreach ($hotels as $hotel)
                            <tr>
								<td>
									{{ $hotel->hotel_id }}
								</td>
								<td>
									{{ $hotel->provider_hotel_id }}
								</td>
								<td class="table-text">
									<a href="/provider_hotel/{{ $hotel->hotel_id }}"> {{ $hotel->hotel_name }} </a>
								</td>
								<td>
									{{ $hotel->hotel_country_name or '-' }}
								</td>
								<td>
								   {{ $hotel->hotel_city_name or '-' }}
								</td>
								<td>
									{{ $hotel->provider_name }}
								</td>
								<td>
									{{ $hotel->created_at }}
								</td>
								<td>
									@if (count($hotel->matches))
										<ul>
										@foreach ($hotel->matches as $match)
										<li>{{ $match->data->match_level }} %</li>	
										@endforeach
										</ul>
									@else -
									@endif
								</td>
							</tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
		<div class="page-links">{{ $hotels->appends(Request::except('page'))->links() }}</div>
	@else
	<div class="row">
	    <h4>Ничего не найдено<h4>
	</div>
</div>
    @endif
    
 <script>	
	// связываем поле радиуса с картой
	$( "#radius-field" ).change(function() {

		searchDistance = parseInt($(this).val());
		map.setMarkerRadius();
	});
	
</script>


<script>
	
	
	// сортировка
		
		$('.sorting-switch').click(function () {
			
			var form = $('#search-hotels-form'),
				icon = $(this).next('#sorting-icon'),
				order = icon.attr('class');
		
			if (!order || order === 'desc')
				order = 'asc';
			else
				order = 'desc';
			
			form.find('input[name="sorting"]').val(order);
			
			if (order)
				form.find('input[name="sorting_param"]').val($(this).data('sorting-param'));
			
			form.submit();
			
		});
		
	// отличается только url (сделать рефакторинг)
	$('#country-field').autocomplete({
		source: function (request, response) {
			$.ajax({
				url: "services/provider_countries",
				data: { q: request.term },
				success: function (data) {
					response(data.suggestions);
				},
				error: function () {
				    response([]);
				}
			});
			
		},
		minLength: 3,
		delimiter: /(,|;)\s*/,
		maxHeight: 400,
		width: 300,
		zIndex: 9999,
		params: { },
		onSelect: function(data, value){ },
//		lookup: []
	});

	$('#city-field').autocomplete({
		source: function (request, response) {
			$.ajax({
				url: "services/provider_cities",
				data: { q: request.term },
				success: function (data) {
					response(data.suggestions);
				},
				error: function () {
				    response([]);
				}
			});
			
		},
		minLength: 3,
		delimiter: /(,|;)\s*/,
		maxHeight: 400,
		width: 300,
		zIndex: 9999,
		params: { },
		onSelect: function(data, value){ },
//		lookup: []
	});
	
	$('#hotel-name-field').autocomplete({
			source: function (request, response) {
				$.ajax({
					url: "services/hotels",
					data: { q: request.term },
					success: function (data) {
						response(data.suggestions);
					},
					error: function () {
					    response([]);
					}
				});

			},
			minLength: 3,
			delimiter: /(,|;)\s*/,
			maxHeight: 400,
			width: 300,
			zIndex: 9999,
			params: { },
			onSelect: function(data, value){ },
	//		lookup: []
		});
</script>


<script type="text/javascript" src="{{ URL::asset('js/form.js') }}"></script>

    <!-- TODO: Current Tasks -->
@endsection