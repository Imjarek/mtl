@extends('layouts.app')

@section('content')
<script>
	
	function initHotelMapTest() {
		
		// as a suggestion you could use the event listener to save the state when zoom changes or drag ends
		
        var center, mapProps;
        
        if (typeof(providerHotelData) != 'undefined')
        {
            center = {lat: providerHotelData.position.lat, lng: providerHotelData.position.lng };
            zoom = 14;
            
        }
        else {
                
            var savedMapProps = loadMapState();

            var defaultProps = {lat: 55, lng: 50, zoom: 3 };
                
            center = { 
                lat: savedMapProps.lat ? savedMapProps.lat : defaultProps.lat,
                lng: savedMapProps.lng ? savedMapProps.lng : defaultProps.lng
            };

            zoom = savedMapProps.zoom ? savedMapProps.zoom : defaultProps.zoom;
        }
		
        mapProps = {
                zoom: zoom,
                center: center,
                scrollwheel: false
        };
                
		map = new google.maps.Map(document.getElementById('hotels-gmap'), mapProps);
        
        // поиск с автозаполнением на карте
        
        if (google.maps.places) {
            
            var input = document.getElementById('pac-input');
            
            var searchBox = new google.maps.places.SearchBox(input);
        
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // при выборе места смещаемся к координатам
            searchBox.addListener('places_changed', function(e) {

               var location = searchBox.getPlaces()[0].geometry.location;

               map.setCenter({lat: location.lat(), lng: location.lng()});
               
               saveMapState();
               //console.log(searchBox.getPlaces()); 
            });
        }
        
        
		map.setMarkerRadius = function (){
			
			if (map.userMarker && map.userMarker.circle)
				map.userMarker.circle.setRadius(searchDistance);
		};
		
		map.removeUserMarker = function () {
			map.userMarker.setMap(null);
			map.userMarker.circle.setMap(null);
		}
		//google.maps.event.addListener(map, 'tilesloaded', tilesLoaded);
		
		// выставляем маркеры отелей
        
        if (typeof(markerData) != 'undefined') 
            markerData.forEach(function (data){

                marker = new google.maps.Marker({
                      position: data.position,
                      title: data.title,
                      map: map,
                      url: data.url,
                      opacity: 0.6
                 });

                // переход к отелю 
                marker.addListener('click', function() {
                      window.location.href = this.url;
                });
                // выделение при наведении
                marker.addListener('mouseover', function() {
                      this.setOpacity(1);
                });
                marker.addListener('mouseout', function() {
                      this.setOpacity(0.6);
                });
            });
		
		if (typeof(_markerData) != 'undefined') 
            _markerData.forEach(function (data){

                marker = new google.maps.Marker({
                      position: data.position,
                      title: data.title,
                      map: map,
                      url: data.url,
					  icon: '/images/sim-hotel-marker.png',
                      opacity: 0.6
                 });
			});
				 
        // а также маркеры похожих отелей если есть
        // 
        if (typeof(simsMarkerData) != 'undefined') 
            
            simsMarkerData.forEach(function (data){

                marker = new google.maps.Marker({
                      position: data.position,
                      title: data.title,
                      map: map,
                      icon: '/images/sim-hotel-marker.png'
                 });
            });
        
        // ставим маркер отеля поставщика если есть
        
        if (typeof(providerHotelData) != 'undefined') 
            
            new google.maps.Marker({
                position: providerHotelData.position, 
                map: map,
                icon: '/images/p-hotel-marker.png',
                title: providerHotelData.title
            });
                    
		// есть ли в куках пользовательский маркер?
		
		var userMarker = loadUserMarker();
		
		if (userMarker) {
			placeMarker(map, userMarker);
			setLocation(userMarker);
			
			google.maps.event.addListener(map.userMarker, 'dragend', function(event) {
		
				// сохраняем координаты в форме поиска
				setLocation({lat: event.latLng.lat(), lng: event.latLng.lng()});
				// запоминаем в куки
				saveUserMarker(event.latLng);
		
            });

		}
		
		// изменения параметров карты сохраняются в куки
		google.maps.event.addListener(map, 'zoom_changed', saveMapState);
		google.maps.event.addListener(map, 'dragend', saveMapState);
			
		// Пользовательская отметка на карте по клику
		google.maps.event.addListener(map, 'click', function(event) {
		
			if (map.userMarker) {
				map.removeUserMarker();
				
			}

			// ставим пользовательский маркер
			placeMarker(map, event.latLng);
			// сохраняем координаты в форме поиска
			setLocation({lat: event.latLng.lat(), lng: event.latLng.lng()});
			// запоминаем в куки
			saveUserMarker(event.latLng);
		
		});
		
		function placeMarker(map, location) {
			
			map.userMarker = new google.maps.Marker({
				position: location, 
				map: map,
				draggable: true,
				icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
			});
			
			map.userMarker.circle = new google.maps.Circle({
				map: map,
				radius: searchDistance,    // 10 miles in metres
				strokeColor: '#FF0000',
				strokeOpacity: 0.4,
				strokeWeight: 1,
				fillColor: '#FF0000',
				fillOpacity: 0.1
			});
			map.userMarker.circle.bindTo('center', map.userMarker, 'position');

			google.maps.event.addListener(map.userMarker, 'click', function(event) {
				map.userMarker.setMap(null);
				map.userMarker.circle.setMap(null);
				
				map.eraseUserMarker();
				$('#radius-field').attr('disabled', true); // будет передаваться?
				$('#coords').val('');
			});
			
			$radiusField = $('#radius-field');
			$radiusField.removeAttr('disabled');
			
			
			if (!$radiusField.val())
				$radiusField.val(searchDistance);
		}
		
		function setLocation(latLng) {
			//console.log(latLng);
			
			var coords = latLng.lat  + ',' + latLng.lng;
			
			$('input[name="coords"]').val(coords);
		};
		  

		// functions below

		function saveMapState(event) { 
			var mapZoom=map.getZoom(); 
			var mapCentre=map.getCenter(); 
			var mapLat=mapCentre.lat(); 
			var mapLng=mapCentre.lng(); 
			var cookiestring=mapLat+"_"+mapLng+"_"+mapZoom; 
			setCookie("mtl_gMapCookie",cookiestring, 30); 
		} 

		function loadMapState() { 
			var gotCookieString=getCookie("mtl_gMapCookie"); 
			var splitStr = gotCookieString.split("_");
			var savedMapLat = parseFloat(splitStr[0]);
			var savedMapLng = parseFloat(splitStr[1]);
			var savedMapZoom = parseFloat(splitStr[2]);
			
			return {
				lat: !isNaN(savedMapLat) ? savedMapLat : null,
				lng: !isNaN(savedMapLng) ? savedMapLng : null,
				zoom: !isNaN(savedMapZoom) ? savedMapZoom : null
			}
		}
		
		function loadUserMarker() {
			var gotCookieString=getCookie("mtl_gMapUserMark"); 
			var splitStr = gotCookieString.split("_");
			var savedLat = parseFloat(splitStr[0]);
			var savedLng = parseFloat(splitStr[1]);
			
			if (!isNaN(savedLat) && !isNaN(savedLng))
			
				return { lat: savedLat, lng: savedLng }
		}
		
		function saveUserMarker(location) {
			var cookiestring = location.lat() + "_" + location.lng(); 
			setCookie("mtl_gMapUserMark", cookiestring, 30); 
		}
		
		map.eraseUserMarker = function () {
			eraseCookie('mtl_gMapUserMark');
		}
		
	}
    
</script>
   <script>
	
	searchDistance = {{ old('radius') ? old('radius') : 50  }};
	
	markerData = [];
	
	@foreach ($hotels as $hotel)
		@if ($hotel->latitude && $hotel->longitude)
			markerData.push({
				position: {lat: {{ $hotel->latitude}}, lng: {{ $hotel->longitude }} },
				title: "{{ $hotel->hotel_name }} ({{ $hotel->city_name or '' }})",
				url: '/provider_hotel/{{ $hotel->hotel_id }}'
			});
		@endif

	@endforeach
	
	_markerData = [];
	
	@foreach ($hotels as $hotel)
		@if ($hotel->g_latitude && $hotel->g_longitude)
			_markerData.push({
				position: {lat: {{ $hotel->g_latitude}}, lng: {{ $hotel->g_longitude }} },
				title: "{{ $hotel->hotel_name }} ({{ $hotel->city_name or '' }})",
				url: '/provider_hotel/{{ $hotel->hotel_id }}'
			});
		@endif

	@endforeach
</script>

<script type='text/javascript' src='{{ URL::asset('js/init_hotel_map.js') }}'></script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMLm-fqO3_-sT-m0oFbY-a7CvBpyopqVI&callback=initHotelMapTest&libraries=places"/>
	
</script>
    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
	<div class="col-md-7">
		<input id="pac-input" class="controls" type="text" placeholder="Искать">
	    <div class="gmap" id="hotels-gmap"></div>
	</div>
        <form action="/provider_hotels" method="GET" class="form-vertical" id="search-hotels-form">
            {{ csrf_field() }}
		<div class="col-md-2">
		<div class="form-group">
			
		    <label for="radius-field" control-label>Радиус
				<i class="fa fa-question-circle-o fa-fw" data-toggle="tooltip" title="Укажите точку на карте и радиус в метрах для поиска отелей">
			</i></label>
			<input disabled type="number" name="radius" id="radius-field" class="form-control" value = "{{ old('radius') }}" min="50" max="5000000" step="10">
			<label for="hotel-id-field" control-label>№ MTL</label>
			<input type="number" name="hotel_id" id="hotel-id-field" class="form-control" value = "{{ old('hotel_id') }}">
			
			<label for="provider-hotel-id-field" control-label>Код поставщика</label>
			<input type="text" name="provider_hotel_id" id="provider-hotel-id-field" class="form-control" value = "{{ old('provider_hotel_id') }}">
			
			<label for="hotel-name-field" control-label>Отель</label>
			<input type="text" name="hotel_name" id="hotel-name-field" class="form-control" value = "{{ old('hotel_name') }}">

			<label for="country-field" control-label>Страна</label>
			<input type="text" name="country" id="country-field" class="form-control" value="{{ old('country') }}">

			<label for="city-field" control-label>Город</label>
			<input type="text" name="city" id="city-field" class="form-control" value="{{ old('city') }}">
			<label for="provider-field" control-label">Поставщик</label>
			<select class="form-control" name="provider_code" id="provider_field">
				<option {{ old('provider_code') ? '' : 'selected' }} value="">Все</option>
				<option value="ostrovok" {{ old('provider_code') == 'ostrovok' ? 'selected' : '' }}>Островок</option>
				<option value="acase" {{ old('provider_code') == 'acase' ? 'selected' : '' }}>Академсервис</option>
				<option value="bvk" {{ old('provider_code') == 'bvk' ? 'selected' : '' }}>Броневик</option>
				<option value="amadeus" {{ old('provider_code') == 'amadeus' ? 'selected' : '' }}>Амадеус</option>
				<option value="bol" {{ old('provider_code') == 'bol' ? 'selected' : '' }}>Beds Online</option>
				<option value="hotelbook" {{ old('provider_code') == 'hotelbook' ? 'selected' : '' }}>HotelBook</option>
			    </select>
			<label control-label for="date-before-field">Добавлен</label><br>
			<input type="date" name="date_before" id="date-before-field" class="form-control" value="{{ old('date_before') }}">
			<br>
			<input type="date" name="date_after" id="date-after-field" class="form-control" value="{{ old('date_after') }}">
			<label for="match-field" control-label>Есть совпадения</label>
			<select class="form-control" name="matches">
				<option {{ !old('matches') ? 'selected' : '' }} value="">Все</option>
				<option {{ old('matches') == 'yes' ? 'selected' : '' }} value="yes">Да</option>
				<option {{ old('matches') == 'no' ? 'selected' : '' }} value="no">Нет</option>
			</select>
			<br>
			<input type="hidden" name="sorting" value="{{ old('sorting') }}">
			<input type="hidden" name="sorting_param" value="{{ old('sorting_param') }}">
			<input type="hidden" name="coords" value="{{ old('coords') }}">
			
			<button class="btn btn-block btn-alert" type="button" onclick="resetForm('#search-hotels-form')">
				<i class="fa fa-times fa-fw"></i>Сбросить фильтры
			</button>
			</div>		
		</div>	
		<div class="col-md-2" style="height:360px;">
			<button type="submit" class="btn btn-block btn-info">
				<i class="fa fa-search fa-fw"></i>Найти
			</button>
					
		    </div>
		
        </form>
	</div>
	</div>
	
		@if (count($hotels) > 0)
		<div class="col-md-12">
		    <p><strong>Всего отелей: {{ $total }}</strong></p>
		    @if (method_exists($hotels, 'total'))
			<p><strong>Найдено отелей: {{ $hotels->total() }}</strong></p>
			@endif
		</div>
		@endif
		
		@if (count($hotels) > 0)
		
		<div class="page-links">{{ $hotels->appends(Request::except('page'))->links() }}</div>
        <div class="panel panel-default">
           		
            <div class="panel-body">
                <table class="table table-striped table-hover hotel-table">

                    <!-- Table Headings -->
                    <thead>
						<th>
							<a class="sorting-switch" data-sorting-param="hotel_id">№ MTL</a>
							<b id="sorting-icon" class="{{ old('sorting_param') == 'hotel_id' ? old('sorting') : ''}}"></b></th>
						<th>Код поставщика</th>
						<th>
							<a class="sorting-switch" data-sorting-param="hotel_name">Отель</a>
							<b id="sorting-icon" class="{{ old('sorting_param') == 'hotel_name' ? old('sorting') : ''}}"></b>
						</th>
						<th>Страна</th>
						<th>Город</th>
						<th>Поставщик</th>
						<th>
							<a class="sorting-switch" data-sorting-param="created_at">Добавлен</a>
							<b id="sorting-icon" class="{{ old('sorting_param') == 'created_at' ? old('sorting') : ''}}"></b>
						</th>
						<th>Совпадения</th>

                    </thead>

                    <!-- Table Body -->
                    <tbody>
                        @foreach ($hotels as $hotel)
                            <tr>
								<td>
									{{ $hotel->hotel_id }}
								</td>
								<td>
									{{ $hotel->provider_hotel_id }}
								</td>
								<td class="table-text">
									<a href="/provider_hotel/{{ $hotel->hotel_id }}"> {{ $hotel->hotel_name }} </a>
								</td>
								<td>
									{{ $hotel->country_name or '-' }}
								</td>
								<td>
								   {{ $hotel->city_name or '-' }}
								</td>
								<td>
									{{ $hotel->provider_name }}
								</td>
								<td>
									{{ $hotel->created_at }}
								</td>
								<td>
									@if (count($hotel->matches))
										<ul>
										@foreach ($hotel->matches as $match)
										<li>{{ $match->data->match_level }} %</li>	
										@endforeach
										</ul>
									@else -
									@endif
								</td>
							</tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
		<div class="page-links">{{ $hotels->appends(Request::except('page'))->links() }}</div>
	@else
	<div class="row">
	    <h4>Ничего не найдено<h4>
	</div>
</div>
    @endif
    
 <script>	
	// связываем поле радиуса с картой
	$( "#radius-field" ).change(function() {

		searchDistance = parseInt($(this).val());
		map.setMarkerRadius();
	});
	
</script>


<script>
	
	
	// сортировка
		
		$('.sorting-switch').click(function () {
			
			var form = $('#search-hotels-form'),
				icon = $(this).next('#sorting-icon'),
				order = icon.attr('class');
		
			if (!order || order === 'desc')
				order = 'asc';
			else
				order = 'desc';
			
			form.find('input[name="sorting"]').val(order);
			
			if (order)
				form.find('input[name="sorting_param"]').val($(this).data('sorting-param'));
			
			form.submit();
			
		});
		
	// отличается только url (сделать рефакторинг)
	$('#country-field').autocomplete({
		source: function (request, response) {
			$.ajax({
				url: "services/provider_countries",
				data: { q: request.term },
				success: function (data) {
					response(data.suggestions);
				},
				error: function () {
				    response([]);
				}
			});
			
		},
		minLength: 3,
		delimiter: /(,|;)\s*/,
		maxHeight: 400,
		width: 300,
		zIndex: 9999,
		params: { },
		onSelect: function(data, value){ },
//		lookup: []
	});

	$('#city-field').autocomplete({
		source: function (request, response) {
			$.ajax({
				url: "services/provider_cities",
				data: { q: request.term },
				success: function (data) {
					response(data.suggestions);
				},
				error: function () {
				    response([]);
				}
			});
			
		},
		minLength: 3,
		delimiter: /(,|;)\s*/,
		maxHeight: 400,
		width: 300,
		zIndex: 9999,
		params: { },
		onSelect: function(data, value){ },
//		lookup: []
	});
	
	$('#hotel-name-field').autocomplete({
			source: function (request, response) {
				$.ajax({
					url: "services/hotels",
					data: { q: request.term },
					success: function (data) {
						response(data.suggestions);
					},
					error: function () {
					    response([]);
					}
				});

			},
			minLength: 3,
			delimiter: /(,|;)\s*/,
			maxHeight: 400,
			width: 300,
			zIndex: 9999,
			params: { },
			onSelect: function(data, value){ },
	//		lookup: []
		});
</script>


<script type="text/javascript" src="{{ URL::asset('js/form.js') }}"></script>

    <!-- TODO: Current Tasks -->
@endsection