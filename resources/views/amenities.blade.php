@extends('layouts.app')

@section('content')
 

<div class="page-links">{{ $amenities->appends(Request::except('page'))->links() }}</div>

        <div class="panel panel-default">
           		
            <div class="panel-body">
                <table class="table table-striped table-hover hotel-table">

                    <!-- Table Headings -->
                    <thead>
						<th>Отметить</th>
						<th>#</th>
                        <th>Услуга</th>
			<th>Группа</th>
                    </thead>

                    <!-- Table Body -->
                    <tbody>
                        @foreach ($amenities as $amenity)
                            <tr>
								<td>
                                    <input type="checkbox" title="Отметить" onclick="$.get(&quot;addToCompare.php?id=1}{&amp;state=&quot;+this.checked);">
                                </td>
								<td>
									{{ $amenity->id }}
								</td>
                                <td class="table-text">
				    {{ $amenity->name }}
                                </td>
								
								<td>
									Общее
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
	<div class="page-links">{{ $amenities->appends(Request::except('page'))->links() }}</div>

@endsection