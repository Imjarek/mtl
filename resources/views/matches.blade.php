@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
		
		@if (count($matches) > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                Совпадения
			</div>
			<div class="page-links">{{ $matches->links() }}</div>
            <div class="panel-body">
                <table class="table table-striped task-table">

                    <!-- Table Headings -->
                    <thead>
                        <th>#</th>
                        <th>Отель</th>
			<th>№</th>
			<th>Поставщик</th>
			<th>Код</th>
			<th>Отель(пост.)</th>
                    </thead>

                    <!-- Table Body -->
                    <tbody>
                        @foreach ($matches as $match)
                            <tr>
                                <!-- Task Name -->
                                <td class="table-text">
                                    <div>{{ $match->id }}</div>
                                </td>
				<td>
				     <div><a href="/hotel/{{ $match->global_id}}">{{ $match->global_hotel_name }}</div>
                                </td>
                                <td>
                                    <div>{{ $match->ref_id }}</div>
                                </td>
				<td>
                                    <div>{{ $match->provider_name }}</div>
                                </td>
				<td>
                                    <div>{{ $match->provider_hotel_id }}</div>
                                </td>
				<td>
				     <div><a href="/provider_hotel/{{ $match->ref_id}}">{{ $match->provider_hotel_name }}</div>
                                </td>
				<td>
                                    <div><a href="/match/{{ $match->id}}">{{ 'Совпадение' }} {{ $match->match_level * 100}}%</a></div>
                                
				</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
			<div class="page-links">{{ $matches->links() }}</div>
        </div>
		@else 
		<div>
			Совпадений не найдено
		</div>
    @endif
	
        <!-- New Task Form -->
<!--        <form action="/task" method="POST" class="form-horizontal">
            {{ csrf_field() }}

             Task Name 
            <div class="form-group">
                <label for="task" class="col-sm-3 control-label">Task</label>

                <div class="col-sm-6">
                    <input type="text" name="name" id="task-name" class="form-control">
                </div>
            </div>

             Add Task Button 
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> Add Task
                    </button>
                </div>
            </div>
        </form>
    </div>-->

    <!-- TODO: Current Tasks -->
@endsection