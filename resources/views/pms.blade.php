@extends('layouts.app')

@section('content')

    <div class="panel-body">
        @include('common.errors')
			<button class="btn btn-info" data-toggle="modal" data-target="#createPmsWindow">Добавить PMS</button>
			<br><br>
		@if (count($pmsystems) > 0)
		<!--<div class="page-links">{{-- $pmss->links() --}}</div>-->
        <div class="panel panel-default">
         			
            <div class="panel-body">
                <table class="table table-striped task-table">

                    <!-- Table Headings -->
                    <thead>
                        <th>№</th>
                        <th>PMS</th>
                    </thead>

                    <!-- Table Body -->
                    <tbody>
                        @foreach ($pmsystems as $pms)
                            <tr>
                                <!-- Task Name -->
                                <td class="table-text">
                                    <div>{{ $pms->id }}</div>
                                </td>

                                <td>
									<a href="/pms/{{ $pms->id }}">{{ $pms->name_ru }}</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
	<!--<div class="page-links">{{-- $pmss->links() --}}</div>-->
    @endif
	
	<!-- create pms form window -->
		<div id="createPmsWindow" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<div class="modal-content" style="width: 100%; margin-left: 0px;">
					<div class="modal-header">
			<!--			<button type="button" class="close" data-dismiss="modal">&times;</button>-->
						<h4 class="modal-title">Новая PMS</h4>
					 </div>
					
					<form action="/pms/create">
						<div class="modal-body">

							  <label for="pms-name-field" control-label>Название</label>
							  <input type="text" name="name" id="pms-name-field" class="form-control" value = "">

						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-info">Сохранить</button>
							<button class="btn btn-default" data-dismiss="modal">Отмена</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	
@endsection