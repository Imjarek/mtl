@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
		<form action="/log" method="GET" class="form-horizontal" role="form" id="search-log-form">
		  {{ csrf_field() }}
		      
		      <div class="form-group">
				  <div class="col-md-2">
			      <label for="log-id-field" control-label>№ MTL</label>
			      <input type="number" name="log_id" id="hotel-id-field" class="form-control" value = "{{ old('log_id') }}">
				  </div>
				  <div class="col-md-2">
			      <label for="object-type-field"control-label>Объект</label>
			      <select name="object_type"  class="form-control" id="object-type-field">
					   <option selected value> -- Выберите объект -- </option>
					   <option value="hotel" {{ old('object_type') == 'hotel' ? 'selected' : '' }}>Отель</option>
					   <option value="city" {{ old('object_type') == 'city' ? 'selected' : '' }}>Город</option>
			      </select>
				  
				  </div>
				  <div class="col-md-2">
			      <label for="use-field" control-label>Пользователь</label>
				      <select class="form-control" name="user_id" id="provider_field">
					   <option selected value> -- Выберите пользователя -- </option>
					  @foreach($users as $user)
						<option value="{{ $user->id }}" {{ old('user_id') == $user->id ? 'selected' : '' }}>{{ $user->name_ru}}
							{{ $user->email }}
						</option>
					  @endforeach
				      </select>
				  </div>
				  <div class="col-md-2">
			      <label control-label for="date-before-field">Дата с</label><br>
			      <input type="date" name="date_before" id="date-before-field" class="form-control" value="{{ old('date_before') }}">
				  </div>
				  <div class="col-md-2">
			      <label control-label for="date-after-field">Дата по</label><br>
			      <input type="date" name="date_after" id="date-after-field" class="form-control" value="{{ old('date_after') }}">
				  </div>
				  <div class="col-md-2">
				  <label control-label for="action-type-field">Действие</label><br>
				  <select name="action_type" class="form-control">
					  <option value {{ old('action_type') ? '' : 'selected' }}>-- Выберите действие --</option>
					  <option value="create" {{ old('action_type') == 'create' ? 'selected' : '' }}>Создание</option>
					  <option value="delete" {{ old('action_type') == 'delete' ? 'selected' : '' }}>Удаление</option>
					  <option value="union" {{ old('action_type') == 'union' ? 'selected' : '' }}>Объединение</option>
					  <option value="undo_union" {{ old('action_type') == 'undo_union' ? 'selected' : '' }}>Отмена объединения</option>
					  <option value="link" {{ old('action_type') == 'link' ? 'selected' : '' }}>Привязка</option>
				  </select>
				  </div>
				  <div class="col-md-8">
				  </div>
				  <div class="col-md-2">
					  <input type="hidden" name="sorting" value="{{ old('sorting') }}">
					  <input type="hidden" name="sorting_param" value="{{ old('sorting_param') }}">
			      <button class="btn btn-block btn-alert" type="button" onclick="resetForm('#search-log-form')" style="margin-top:10px;>
				      <i class="fa fa-times fa-fw"></i>Сбросить фильтры
			      </button>
				  </div>
				  <div class="col-md-2">
			      <button type="submit" class="btn btn-block btn-info" style="margin-top:10px;>
					  <i class="fa fa-search fa-fw"></i>Найти
			      </button>
				  </div>
			 
		</form>
</div>
	<!--</div>-->
	
		@if (count($entries) > 0)
			<div class="page-links">{{ $entries->appends(Request::except('page'))->links() }}</div>
			<div class="panel panel-default">
			
				<div class="panel-body">
				    <table class="table table-striped task-table">

					<!-- Table Headings -->
					<thead>
					<th><a class="sorting-switch" data-sorting-param="id">№ события</a>
						<b id="sorting-icon" class="{{ old('sorting_param') == 'id' ? old('sorting') : ''}}"></b>
					</th>
					<th>
						<a class="sorting-switch" data-sorting-param="action_type">Тип события</a>
						<b id="sorting-icon" class="{{ old('sorting_param') == 'action_type' ? old('sorting') : ''}}"></b>
					</th>
					    <th>Объект</th>
					    <th>Описание</th>
					    <th>
							<a class="sorting-switch" data-sorting-param="user_name">Пользователь</a>
							<b id="sorting-icon" class="{{ old('sorting_param') == 'user_name' ? old('sorting') : ''}}"></b>
						</th>
					    <th>
							<a class="sorting-switch" data-sorting-param="created_at">Дата и время</a>
							<b id="sorting-icon" class="{{ old('sorting_param') == 'created_at' ? old('sorting') : ''}}"></b>
						</th>
					</thead>

					<!-- Table Body -->
					<tbody>
					    @foreach ($entries as $entry)
						<tr>
						    <td>
						       {{ $entry->id }}
						    </td>
							<td>
						       {{ $entry->action_type }}
						    </td>
						    <td class="table-text">
							 {!! $entry->object_name !!}
						    </td>
						    <td class="table-text">
							 {!! $entry->comment !!}
						    </td>
						    <td>
								{{ $entry->user_name }}<br>{{ $entry->user_email }}
						    </td>
						    <td>{{ $entry->created_at }}</td>


						</tr>
					    @endforeach
					</tbody>
				    </table>
				</div>
			
			   </div>
			<div class="page-links">{{ $entries->appends(Request::except('page'))->links() }}</div>
		@else
		<h4>Нет записей<h4>
		@endif

		

<script type="text/javascript" src="{{ URL::asset('js/form.js') }}"></script>

<script>
	$('.sorting-switch').click(function () {
			
			var form = $('#search-log-form'),
				icon = $(this).next('#sorting-icon'),
				order = icon.attr('class');
		
			if (!order || order === 'desc')
				order = 'asc';
			else
				order = 'desc';
			
			form.find('input[name="sorting"]').val(order);
			
			if (order)
				form.find('input[name="sorting_param"]').val($(this).data('sorting-param'));
			
			form.submit();
			
	});
</script>
@endsection