@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->
<!--Инициализация карты-->
<!-- вынести скрипт с виджетом в отдельных файл!!! -->

<!--<script type="text/javascript" src="geomap.js">-->
<script>
	// если возвращаемся со страницы объединения - принудительная перезагрузка
	if (getCookie("union_page_visited")) {
          eraseCookie("union_page_visited");
          window.location.reload(true);
    }
</script>
	
<script>
	
	searchDistance = {{ old('radius') ? old('radius') : 50  }};
	
	markerData = [];
	
	@foreach ($hotels as $hotel)
		@if ($hotel->latitude && $hotel->longitude)
			markerData.push({
				position: {lat: {{ $hotel->latitude}}, lng: {{ $hotel->longitude }} },
				title: '{{ $hotel->hotel_name }} ( {{ $hotel->city_name }} )',
				url: '/hotel/{{ $hotel->hotel_id }}'
			});
		@endif

	@endforeach
</script>

<script type='text/javascript' src='{{ URL::asset('js/init_hotel_map.js') }}'></script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMLm-fqO3_-sT-m0oFbY-a7CvBpyopqVI&callback=initHotelMap&libraries=places"/>
	
</script>
    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
	<div class="col-md-7">
		 <input id="pac-input" class="controls" type="text" placeholder="Искать">
	    <div class="gmap" id="hotels-gmap"></div>
	</div>
	
        <form action="/hotels" method="GET" class="form-vertical" id="search-hotels-form" autocomplete="off">
            {{ csrf_field() }}
		<div class="col-md-2">
		<div class="form-group">
			
			<label for="radius-field" control-label>Радиус<i class="fa fa-question-circle-o fa-fw" data-toggle="tooltip" title="Укажите точку на карте и радиус в метрах для поиска отелей">	
			</i></label>
			<input disabled type="number" name="radius" id="radius-field" class="form-control" value = "{{ old('radius') }}" min="50" max="5000000" step="10">
			<label for="hotel-id-field" control-label>№ MTL</label>
			<input type="number" name="hotel_id" id="hotel-id-field" class="form-control" value = "{{ old('hotel_id') }}">

			<label for="hotel-name-field" control-label>Отель</label>
			<input type="text" name="hotel_name" id="hotel-name-field" class="form-control" value = "{{ old('hotel_name') }}">

			<label for="country-field" control-label>Страна</label>
			<input type="text" name="country" id="country-field" class="form-control" value="{{ old('country') }}" />
			<label for="city-field" control-label>Город</label>
			<input type="text" name="city" id="city-field" class="form-control" value="{{ old('city') }}">
			<label control-label for="date-before-field">Добавлен</label><br>
			<input type="date" name="date_before" id="date-before-field" class="form-control" value="{{ old('date_before') }}">
			<br>
			<input type="date" name="date_after" id="date-after-field" class="form-control" value="{{ old('date_after') }}">

			<br>
			<input type="hidden" name="sorting" value="{{ old('sorting') }}">
			<input type="hidden" name="sorting_param" value="{{ old('sorting_param') }}">
			<input type="hidden" name="coords" value="{{ old('coords') }}">
			
			<button class="btn btn-block btn-alert" type="button" onclick="resetForm('#search-hotels-form')">
			    <i class="fa fa-times fa-fw"></i>Сбросить фильтры
			</button>
			</div>		
		</div>	
		<div class="col-md-2" style="height:470px;">
			<button type="submit" class="btn btn-block btn-info">
				<i class="fa fa-search fa-fw"></i>Найти
			</button>
				<div style="position: absolute; bottom: 0;">

				    <p><b>Выбрано отелей: <span id="selected-hotels-cnt"> {{ $checked_count }}</span></b></p>
					<a href="/union" class="btn btn-block btn-primary {{ $checked_count > 1 ? '' : 'disabled'}}" id="go-to-union" >
						Перейти к объединению
					</a>
				<br>
				<button class="btn btn-block btn-info" onclick="return resetSelection();">
					Cнять выделение
				</button>
		    </div>
		</div>
        </form>
	
	</div>
    </div>

		@if (count($hotels) > 0)
		<div class="col-md-10">
		    <p><strong>Всего отелей: {{ $total }}</strong></p>
		    @if (method_exists($hotels, 'total'))
			<p><strong>Найдено отелей: {{ $hotels->total() }}</strong></p>
			@endif
		</div>
		
		
		<div class="page-links">{{ $hotels->appends(Request::except('page'))->links() }}</div>
        <div class="panel panel-default">
           		
            <div class="panel-body">
                <table class="table table-striped table-hover hotel-table">

                    <thead>
						<th>
							<a class="sorting-switch" data-sorting-param="hotel_id">№ MTL</a>
							<b id="sorting-icon" class="{{ old('sorting_param') == 'hotel_id' ? old('sorting') : ''}}"></b>
						</th>
						<th>
							<a class="sorting-switch" data-sorting-param="hotel_name">Отель</a>
							<b id="sorting-icon" class="{{ old('sorting_param') == 'hotel_name' ? old('sorting') : ''}}"></b>
						</th>
						<th>
							Поставщики отелей
						</th>
						<th>Страна</th>
						<th>
							<a class="sorting-switch" data-sorting-param="city_name">Город</a>
							<b id="sorting-icon" class="{{ old('sorting_param') == 'city_name' ? old('sorting') : ''}}"></b>
						</th>
						<th>
							<a class="sorting-switch" data-sorting-param="created_at">Добавлен</a>
							<b id="sorting-icon" class="{{ old('sorting_param') == 'created_at' ? old('sorting') : ''}}"></b>
						</th>
						<th>Выбрать</th>
                    </thead>

                    <tbody>
                        @foreach ($hotels as $hotel)
                            <tr>
								
				<td>
					{{ $hotel->hotel_id }}
				</td>
                                <td class="table-text">
					<a href="/hotel/{{ $hotel->hotel_id }}"> {{ $hotel->hotel_name }} </a>
                </td>
				<td>
						@if (count($hotel->providers))
						<ul>
							@foreach ($hotel->providers as $provider)
							<li>
								<a href="/provider_hotel/{{ $provider->id }}">{{ $provider->provider }}</a>
							</li>
							@endforeach
						</ul>
						@endif
				</td>
				<td>
					{{ $hotel->country_name or '-' }}
                </td>
				<td>
				    <a href="/city/{{ $hotel->city_id }}">{{ $hotel->city_name }}</a>
				</td>
				<td>
					{{ $hotel->created_at }}
				</td>
				<td>
                                    <input type="checkbox" title="Выбрать" {{ $hotel->checked ? 'checked' : '' }}  onclick="selectHotel({{ $hotel->hotel_id }}, this);">
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
		<div class="page-links">{{ $hotels->appends(Request::except('page'))->links() }}</div>
	@else
	<div>
		Ничего не найдено
	</div>
    @endif
    
	<script>
		
		// связываем поле радиуса с картой
		$( "#radius-field" ).change(function() {
			
			searchDistance = parseInt($(this).val());
			map.setMarkerRadius();
		});
	
	
		var checkCnt = 0;
		
		
		function selectHotel(hotelId, me) {
			
			var $query = '/addToBuffer/' + hotelId + '/checked/' + (me.checked ? 1 : 0);
			
			$.get($query, function(data) {
				
				// если уже выделено 2 то отмена выделения
				if (!data.result) {
					me.checked = false;
					return;
				}
				
				$('#selected-hotels-cnt').text(data.cnt);
				
				if (data.cnt > 1)
					$('#go-to-union').removeClass('disabled');
				else 
					$('#go-to-union').addClass('disabled')
			});
			

		}
		
		function resetSelection() {
		
			$('table input:checked').each(function() {
				this.checked = false;			

			});

			$.get('/resetBuffer');
			$('#selected-hotels-cnt').text(0);
			$('#go-to-union').addClass('disabled');
			return false;
		}
		
	</script>
	
	<script type="text/javascript" src="{{ URL::asset('js/form.js') }}"></script>
	
	<script>
		
		// сортировка
		
		$('.sorting-switch').click(function () {
			
			var form = $('#search-hotels-form'),
				icon = $(this).next('#sorting-icon'),
				order = icon.attr('class');
		
			if (!order || order === 'desc')
				order = 'asc';
			else
				order = 'desc';
			
			form.find('input[name="sorting"]').val(order);
			
			if (order)
				form.find('input[name="sorting_param"]').val($(this).data('sorting-param'));
			
			form.submit();
			
		});
		
		
		// автозаполнение полей формы
		
		$('#hotel-name-field').autocomplete({
			source: function (request, response) {
				$.ajax({
					url: "services/hotels",
					data: { q: request.term },
					success: function (data) {
						response(data.suggestions);
					},
					error: function () {
					    response([]);
					}
				});
			},
			minLength: 3,
			delimiter: /(,|;)\s*/,
			maxHeight: 400,
			width: 300,
			zIndex: 9999,
			params: { },
			onSelect: function(data, value){ },
	//		lookup: []
		});
		
		$('#country-field').autocomplete({
			source: function (request, response) {
				$.ajax({
					url: "services/countries",
					data: { q: request.term },
					success: function (data) {
						response(data.suggestions);
					},
					error: function () {
					    response([]);
					}
				});

			},
			minLength: 3,
			delimiter: /(,|;)\s*/,
			maxHeight: 400,
			width: 300,
			zIndex: 9999,
			params: { },
			onSelect: function(data, value){ },
	//		lookup: []
		});
		
		// отличается только url (сделать рефакторинг)
		
		
		$('#city-field').autocomplete({
			source: function (request, response) {
				$.ajax({
					url: "services/cities",
					data: { q: request.term },
					success: function (data) {
						response(data.suggestions);
					},
					error: function () {
					    response([]);
					}
				});

			},
			minLength: 3,
			delimiter: /(,|;)\s*/,
			maxHeight: 400,
			width: 300,
			zIndex: 9999,
			params: { },
			onSelect: function(data, value){ },
	//		lookup: []
		});
	</script>

    
@endsection