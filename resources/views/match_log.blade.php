
@extends('layouts.app')

@section('content')

 <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
		
					
@if (count($entries) > 0)


<div class="panel panel-default">

	<div class="panel-body">
		<table class="table table-striped task-table">
	
			<thead>
				<th>ID</th>
				<th>№ MTL</th>
				<th>Поставщик</th>
				<th>ID (пост.)</th>
				<th>Название</th>
				<th>Название (пост.)</th>
				<th>Название (точн.)</th>
				<th>Название (част.)</th>
				<th>Гугл плейс</th>
				<th>Телефон</th>
				<th>Емейл</th>
				<th>Сайт</th>
				<th>Ранг</th>
				
			</thead>

			@foreach($entries as $entry)
				<tr>
					<td>{{ $entry->log_id }}</td>
					<td>{{ $entry->hotel_id }}</td>
					<td>{{ $entry->provider }}</td>
					<td>{{ $entry->provider_hotel_id }}</td>
					<td>{{ $entry->hotel_name }}</td>
					<td>{{ $entry->provider_hotel_name }}</td>
					<td>{!! $entry->exact_title ? '&#x25CF' : '&#x25CB' !!}</td>
					<td>{!! $entry->title ? '&#x25CF' : '&#x25CB' !!}</td>
					<td>{!! $entry->google_place_id ? '&#x25CF' : '&#x25CB' !!}</td>
					<td>{!! $entry->phone ? '&#x25CF' : '&#x25CB' !!}</td>
					<td>{!! $entry->email ? '&#x25CF' : '&#x25CB' !!}</td>
					<td>{!! $entry->url ? '&#x25CF' : '&#x25CB' !!}</td>
					<td>{{ $entry->range }}</td>
				</tr>
			@endforeach
</table>
	</div>
</div>
@else
Нет записей
@endif
@endsection