@extends('layouts.app')

@section('content')

	 <div class="panel-body">
        <form action="/request_rates" method="POST" class="form-vertical" id="request_rates_form" autocomplete="off">
            {{ csrf_field() }}
		<div class="col-md-2">
		<div class="form-group">
			
			
			<label control-label>ID отеля поставщика</label>
			<input type="text" name="hotel_id" class="form-control" value = "{{ old('hotel_id') }}">
			
			<label control-label>Взрослых</label>
			<input type="number" name="adults" class="form-control" value = "{{ old('adults') }}">
			<label control-label>Детей</label>
			<input type="number" name="children" class="form-control" value = "{{ old('children') }}">
			<label control-label>Комнат</label>
			<input type="number" name="rooms" class="form-control" value = "{{ old('rooms') }}">
			<!--<input type="hidden" name="sorting" value="{{ old('param1') }}">-->
			<label>Период проживания</label><br>
			<input type="date" name="check_in" class="form-control" value="{{ old('check_in') }}">
			<br>
			<input type="date" name="check_out" class="form-control" value="{{ old('check_out') }}">
			
			<label control-label">Поставщик</label>
			<select class="form-control" name="provider">
				<option value="ostrovok" {{ old('provider') == 'ostrovok' ? 'selected' : '' }}>Островок</option>
				<option value="hotelbook" {{ old('provider') == 'hotelbook' ? 'selected' : '' }}>HotelBook</option>
				<option value="acase" {{ old('provider') == 'acase' ? 'selected' : '' }}>Академсервис</option>
				<option value="bvk" {{ old('provider') == 'bvk' ? 'selected' : '' }}>Броневик</option>
				<option value="amadeus" {{ old('provider') == 'amadeus' ? 'selected' : '' }}>Амадеус</option>
				<option value="bol" {{ old('provider') == 'bol' ? 'selected' : '' }}>Beds Online</option>
			    </select>
			</div>		
		</div>	
		<div class="col-md-2">
			<button type="submit" class="btn btn-block btn-info">
				<i class="fa fa-search fa-fw"></i>Найти
			</button>
			<button class="btn btn-block btn-alert" type="button" onclick="resetForm('#request_rates_form')">
			    <i class="fa fa-times fa-fw"></i>Сбросить форму
			</button>
		</div>
        </form>
	</div>
	
	@if ($response)
	<div class="response">
		<pre>
			<code class="{{ $format }}}">{{ $response }}</code>
		</pre>
	@endif
	
<!--	</div>
    </div>-->
<script>
	// повтор кода - см. form.js
	function resetForm() {
		$form = $('#request_rates_form');
		$form.find("input:text").val('');
		$form.find("input[type='number']").val('');	
		$form.find("input[type='date']").val('');
		$form.find('select').prop('selectedIndex', 0);
	}
</script>
    
@endsection