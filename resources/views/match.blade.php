@extends('layouts.app')

@section('content')

<script>
	$( document ).ready(function() {
		$( "#actionForm" ).submit(function(event) {
			
			var url, data;
			
			var action = $('input[name=action]:checked').val();
			
			if (action === 'create') {

				url = '/create_hotel';
				data = {
					"providerHotelId": "{{ $providerHotel->id }}",
					"linkId": "{{ $match->link_id }}"
				};
			}
			
			if (action === 'link') {
				
				url = '/link_hotels';
				data = {
					"providerHotelId": "{{ $providerHotel->id }}",
					"hotelId": "{{ $hotel->id }}",
					"linkId": "{{ $match->link_id }}"
				}
			}
			
			if (data)
				$.ajax({
					type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
					url         : url, // the url where we want to POST
					data        : data, // our data object
					dataType    : 'json', // what type of data do we expect back from the server
					encode      : true
				}).done(function(response) {
	//
	//					// log data to the console so we can see
	//
	//					$('#dialog').dialog();
	//
	//					// here we will handle errors and validation messages
					
					if (response.result)
						window.location.href = "/matches";
					else
						alert('Произошла ошибка');
				});
				
			event.preventDefault();
			return false;
		});
	});
    
</script>

<!--Инициализация карт-->

<script>
	function initMaps() {
	  var uluru = {lat: {{ $hotel->latitude }}, lng: {{ $hotel->longitude }}};
	  var map = new google.maps.Map(document.getElementById('hotel-gmap'), {
		zoom: 14,
		center: uluru
	  });
	  var marker = new google.maps.Marker({
		position: uluru,
		map: map
	  });
	  
	  var uluru = {lat: {{ $providerHotel->latitude }}, lng: {{ $providerHotel->longitude }}};
	  var map = new google.maps.Map(document.getElementById('provider-hotel-gmap'), {
		zoom: 14,
		center: uluru
	  });
	  var marker = new google.maps.Marker({
		position: uluru,
		map: map
	  });
	}
</script>

<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMLm-fqO3_-sT-m0oFbY-a7CvBpyopqVI&callback=initMaps">
</script>

<!--<div id="dialog" title="Внимание!">
	<p>Информация</p>
</div>-->

<div class="panel-body">
	@include('common.errors')
	
	<div><h3>Совпадение {{ $match->match_level * 100}}%</h3></div>
	<div class="panel panel-info">
	<div class="panel-body">	

		<h4>{{ $providerHotel->provider->name }}</h4>
		<form class="form-horizontal" id="actionForm">
			{{ csrf_field() }}

			<fieldset>
				<div class="radio">
					<label><input name="action" value="link" type="radio" checked/> Связать <b>{{ $hotel->name_ru }}</b> c <b>{{ $providerHotel->name_ru }}</b>
					</label>
				</div>
				<div class="radio">
				    <label><input name="action" value="create" type="radio"/> Создать новый отель <b>{{ $providerHotel->name_ru }}</b></label>
				</div>	
				<br>
				<button type="submit" class="btn btn-info">Подтвердить</button>
			</fieldset>
		</form>

	 </div>
	</div>
	<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Отель № {{ $hotel->id }}</div>
				<div class="panel-body">
					<div>
						<img class="thumbnail" style="margin-left: auto; margin-right: auto;" src="" />
					</div>
				    <h3><a href="/hotel/{{ $hotel->id }}">{{ $hotel->name_ru }}<sup></sup></a></h3>
					 <div>
						  <img class="thumbnail" style="margin-left: auto; margin-right: auto;" src="{{ $hotel->photo}}" />
					  </div>
					<p><strong>Поставщик: </strong>MTL</p>
					<p><strong>Город: </strong>{{ $hotel->city->name_ru or '-' }}</p>
					<p><strong>Адрес: </strong>{{ $hotel->address }}</p>
					<p><strong>Описание: </strong>{{ $hotel->description }}</p>
					<p><strong>Координаты: </strong>{{ $hotel->latitude }} / {{ $hotel->longitude }}</p>
					<!--<div class="readmore">Описание</div>-->
				</div>
				<div class="panel-footer" style="padding: 0px;">
					<div class = "gmap" id="hotel-gmap"></div>
				</div>
			</div>
	</div>

	<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="panel panel-primary">
			  <div class="panel-heading">Отель поставщика № {{ $providerHotel->id }}</div>
			  <div class="panel-body">
				<div>
					<img class="thumbnail" style="margin-left: auto; margin-right: auto;" src="" />
				</div>
				<h3><a href="/provider_hotel/{{ $providerHotel->id }} ">{{ $providerHotel->name_ru }}</a><sup></sup></h3>
				 <div>
						  <img class="thumbnail" style="margin-left: auto; margin-right: auto;" src="{{ $providerHotel->photo}}" />
					  </div>
				<p><strong>Код отеля: </strong>{{ $providerHotel->provider_hotel_id }}</p>
				<p><strong>Поставщик: </strong>{{ $providerHotel->provider->name }}</p>
				<p><strong>Город: </strong>{{ $providerHotel->city->name_ru or '-' }}</p>
				<p><strong>Адрес: </strong>{{ $providerHotel->address }}</p>
				<p><strong>Описание: </strong>{{ $providerHotel->description }}</p>
				<p><strong>Координаты: </strong>{{ $providerHotel->latitude }} / {{ $providerHotel->longitude }}</p>
				<!--<div class="readmore">Описание</div>-->
			  </div>
			  <div class="panel-footer" style="padding: 0px;">
				<div class="gmap" id="provider-hotel-gmap"></div>
			  </div>
	</div>
	
</div>

@endsection