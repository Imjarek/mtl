@extends('layouts.app')

@section('content')


	@include('common.errors')
		
		<div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
		
			<form action="/reports/request" class="form-vertical" id="request-report-form" autocomplete="off">
				{{ csrf_field() }}
				<div class="col-md-4">
					<div class="form-group">
						<input type="hidden" name="format" value="xlsx">
						<label for="report-type-field" control-label>Вид отчета</label>
						<select id ="report-type-field" class="form-control" name="report_type" id="provider_field">
							<!--<option disabled value {{ old('report_type') ? '' : 'selected' }}>--- Выберите тип отчета ---</option>-->
							<!--<option value="matched_hotels" {{ old('report_type') == 'matched_hotels' ? 'selected' : ''}}>Сопоставленные отели</option>-->
							<option value="matched_hotels" selected>Сопоставленные отели</option>
						</select>
						<label for="report-format-field" control-label>Формат отчета</label>
						<select id ="report-format-field" class="form-control" name="format">
							<option value="csv" {{ old('format') ? 'csv' : 'selected' }}>CSV</option>
							<option value="xlsx" {{ old('format') ? 'xlsx' : 'selected' }}>Excel</option>
						</select>
						<label for="country-field" control-label>Страна</label>
						<input type="text" name="country" id="country-field" class="form-control" value="{{ old('country') }}">
						<label for="city-field" control-label>Город</label>
						<input type="text" name="city" id="city-field" class="form-control" value="{{ old('city') }}">
						<label for="manager-field" control-label>Менеджер</label>
						<select id ="manager-field" class="form-control" name="manager_id">
							<option value="" {{ old('manager_id') ? null : 'selected' }}>Все</option>
							@foreach ($managers as $manager)
								<option value="{{ $manager->id }}" {{ old('manager_id') == $manager->id ? 'selected' : ''}}>
										{{ $manager->fio}}
								</option>
							@endforeach
							<option value="nobody" {{ old('manager_id') == 'nobody' ? 'selected' : ''}}>
								Без менеджера
							</option>
						</select>
						<label for="split-field" control-label>Разбивать xls-файлы
						<input type="checkbox" name="split" checked="true" id="split-field" class="form-control"
							   style="width: 20px; height: 20px">
						</label>
					</div>

					<button type="submit" class="btn btn-info">
						<span id="status">
							<i class="fa fa-send fa-fw"></i>
						</span>Отправить
					</button>
				</div>
			</form>
		</div>

	<script>
			
			$('#report-format-field').on('change', function (){
		
				if ($(this).val() === 'xlsx')
					$('#split-field').removeAttr('disabled');
				else
					$('#split-field').attr('disabled', 'disabled');
			});
	
			$( "#request-report-form" ).submit(function( e ) {
			
				var btn = $(this).find('button[type=submit]'),
					status = btn.find('#status'),
					sender = '<i class="fa fa-send fa-fw"></i>', 
					spinner = '<i class="fa fa-spinner fa-fw fa-spin"></i>';
				
				status.find('i').remove();
				status.append(spinner);
				
				$.ajax({
					url: $(this).attr('action'),
					data : $(this).serialize(),
					success: function(data){
						
						status.find('i').remove();
						status.append(sender);
						btn.blur();
						var file = '/reports/download/' + data.filename;
						window.location = file;
					},
					error: function (data) {
						status.find('i').remove();
						status.append(sender);
						alert('Ошибка при формировании отчета');
						console.log(data);
					}
				});
				
				e.preventDefault();
				return false;
			});
			
	</script>
	<script>
		$('#country-field').autocomplete({
			source: function (request, response) {
				$.ajax({
					url: "services/countries",
					data: { q: request.term },
					success: function (data) {
						response(data.suggestions);
					},
					error: function () {
					    response([]);
					}
				});

			},
			minLength: 3,
			delimiter: /(,|;)\s*/,
			maxHeight: 400,
			width: 300,
			zIndex: 9999,
			params: { },
			onSelect: function(data, value){ },
	//		lookup: []
		});
		
		// отличается только url (сделать рефакторинг)
		
		
		$('#city-field').autocomplete({
			source: function (request, response) {
				$.ajax({
					url: "services/cities",
					data: { q: request.term },
					success: function (data) {
						response(data.suggestions);
					},
					error: function () {
					    response([]);
					}
				});

			},
			minLength: 3,
			delimiter: /(,|;)\s*/,
			maxHeight: 400,
			width: 300,
			zIndex: 9999,
			params: { },
			onSelect: function(data, value){ },
	//		lookup: []
		});
	</script>
	<script>
			
			
			var btn = $("#request-report-form").find('button[type=submit]');
			
			$(function (){
				
				submitState = function () {
					
					var btn = $("#request-report-form").find('button[type=submit]');
				
					if (!$( "select option:selected" ).val())
						btn.attr('disabled', true);
					else
						btn.attr('disabled', false);
				};
				
				submitState();
				
				$( "#request-report-form" ).find('select[name=report_type]').change(function (e){
				
					submitState();
				});
				
			});
			
	</script>
@endsection