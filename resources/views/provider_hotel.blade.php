@extends('layouts.app')

@section('content')


@include('common.errors')

@if ($providerHotel)
	<!--Инициализация карты-->
	
	@if ($providerHotel->global_hotel_id)
		
		<script>
			function initProviderHotelMap() {

				var place = {lat: {{ $providerHotel->latitude}}, lng: {{ $providerHotel->longitude }} };

				var map = new google.maps.Map(document.getElementById('provider-hotel-gmap'), {
				  zoom: 14,
				  center: place,
				  scrollwheel: false
				});
				var marker = new google.maps.Marker({
				  position: place,
				  map: map
				});
			}
		</script>
		<div class="panel-body">
			<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-primary">

					<div class="panel-heading">
						{{ $providerHotel->hotel_name }} {{ $providerHotel->not_exists ? '[Отсутствует в новых данных]' : '' }}
					</div>
					<div class="panel-body">

						<p>
							<img class="thumbnail" style="margin-left: auto; margin-right: auto;" src="{{ $providerHotel->photo }}" />
						<p>
						@if ($globalHotel)
						<p>
							<strong>Глобальный отель: </strong>
							<a href="/hotel/{{ $providerHotel->global_hotel_id }}">
								{{ $globalHotel->name_ru }}
							</a>
						</p>
						@endif
						<p><strong>Поставщик: </strong>{{ $providerHotel->provider_name }}</p>
						<p><strong>Код поставщика: </strong>{{ $providerHotel->provider_hotel_id}}</p>
						<p><strong>№ MTL: </strong>{{ $providerHotel->hotel_id }}</p>
						<p><strong>Страна: </strong>{{ $providerHotel->country_name }}</p>
						<p><strong>Город: </strong>{{ $providerHotel->city_name }}</p>
						<p><strong>Добавлен: </strong>{{ $providerHotel->created_at }}</p>
						<p><strong>Адрес: </strong>{{ $providerHotel->address }}</p>
						<p><strong>Описание: </strong>{{ $providerHotel->description }}</p>
						<p><strong>Телефон: </strong>{{ $providerHotel->phone or '-' }}</p>
						@if ($providerHotel->g_latitude && $providerHotel->g_longitude)
							<p><strong>Координаты: </strong>{{ $providerHotel->g_latitude }} / {{ $providerHotel->g_longitude }}&nbsp
								<img style="width: 16px;" src="/images/gmaps_icon.ico">
						@else
							<p><strong>Координаты: </strong>{{ $providerHotel->latitude }} / {{ $providerHotel->longitude }}
						@endif
						<p><strong>Email: </strong>{{ $providerHotel->email or '-' }}</p>
						<p><strong>Сайт отеля: </strong>{{ $providerHotel->url or '-' }}</p>
					</div>
					<div class="panel-footer" style="padding: 0px;">
						
						  <div class="middle-gmap" id="provider-hotel-gmap"></div>

						  <script async defer
						  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMLm-fqO3_-sT-m0oFbY-a7CvBpyopqVI&callback=initProviderHotelMap&libraries=places">
						  </script>
					</div>
				</div>
				@if ($providerHotel->raw_data)
				<button class="btn btn-info" id="data-expand">Отобразить данные</button>
				@endif
			</div>
		</div>
		
	@else
		
	<script>
	
		searchDistance = {{ old('radius') ? old('radius') : 50  }};
		
		providerHotelData = { 
			title: '{{ $providerHotel->name_ru }}',
			position: {lat: {{ $providerHotel->latitude }}, lng: {{ $providerHotel->longitude }} }
		};
		
		markerData = [];
		
		@if (count($hotels) <= 10)
			@foreach ($hotels as $hotel)
				@if ($hotel->latitude && $hotel->longitude)
					markerData.push({
						position: {lat: {{ $hotel->latitude}}, lng: {{ $hotel->longitude }} },
						title: '{{ $hotel->hotel_name }} ({{ $hotel->city_name }})',
						url: '/hotel/{{ $hotel->hotel_id }}'
					});
				@endif

			@endforeach
		@endif
		
		@if (count($similarHotels) > 0)
				simsMarkerData = [];

				@foreach ($similarHotels as $simHotel)

					simsMarkerData.push({
						title: "{{ $simHotel->name_ru }}",
						position: { lat: {{ $simHotel->latitude }}, lng: {{ $simHotel->longitude }} },
					});

				@endforeach
		@endif
		
	</script>

	<script type='text/javascript' src='{{ URL::asset('js/init_hotel_map.js') }}'></script>

	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMLm-fqO3_-sT-m0oFbY-a7CvBpyopqVI&callback=initHotelMap&libraries=places"/>

	</script>
		<div class="panel-body">
			<!-- Display Validation Errors -->
			@include('common.errors')
		<div class="col-md-7">
			<input id="pac-input" class="controls" type="text" placeholder="Искать">
			<div class="gmap" id="hotels-gmap"></div>
		</div>

			<form action="/provider_hotel/{{ $providerHotel->id }}" method="GET" class="form-vertical" id="search-hotels-form">
				{{ csrf_field() }}
			<div class="col-md-2">
			<div class="form-group">

				<label for="radius-field" control-label>Радиус
					<i class="fa fa-question-circle-o fa-fw" data-toggle="tooltip" title="Укажите точку на карте и радиус в метрах для поиска отелей">
				</i></label>
				<input disabled type="number" name="radius" id="radius-field" class="form-control" value = "{{ old('radius') }}" min="50" max="5000000" step="10">
				<label for="hotel-id-field" control-label>№ MTL</label>
				<input type="number" name="hotel_id" id="hotel-id-field" class="form-control" value = "{{ old('hotel_id') }}">

				<label for="hotel-name-field" control-label>Отель</label>
				<input type="text" name="hotel_name" id="hotel-name-field" class="form-control" value = "{{ old('hotel_name') }}">

				<label for="country-field" control-label>Страна</label>
				<input type="text" name="country" id="country-field" class="form-control" value="{{ old('country') }}" />
				<label for="city-field" control-label>Город</label>
				<input type="text" name="city" id="city-field" class="form-control" value="{{ old('city') }}">
				<label control-label for="date-before-field">Добавлен</label><br>
				<input type="date" name="date_before" id="date-before-field" class="form-control" value="{{ old('date_before') }}">
				<br>
				<input type="date" name="date_after" id="date-after-field" class="form-control" value="{{ old('date_after') }}">

				<br>
				<input type="hidden" name="coords" value="{{ old('coords') }}">

				<button class="btn btn-block btn-alert" type="button" onclick="resetForm('#search-hotels-form')">
					<i class="fa fa-times fa-fw"></i>Сбросить фильтры
				</button>
				</div>		
			</div>	
			<div class="col-md-2" style="height:470px;">
				<button type="submit" class="btn btn-block btn-info">
					<i class="fa fa-search fa-fw"></i>Найти
				</button>
				</div>
			</div>
			</form>

		</div>
	<div class="panel-body">
		
		<!--Карточка непривязанного отеля-->
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="panel panel-primary">

				<div class="panel-heading">{{ $providerHotel->name_ru }} {{ $providerHotel->not_exists ? '[Отсутствует в новых данных]' : '' }}</div>
				<div class="panel-body">

					<div class="control-group" style="margin-bottom: 10px;">
						<button id="create-button" class="btn btn-info" 
								data-id="{{ $providerHotel->id }}" 
								href="#confirmation-window"
								data-toggle="modal"
								data-id="{{ $providerHotel->id }}"
								data-title="Подтверждение создания глобального отеля"
								data-text="Подтвердите создание глобального отеля"
								data-handler="createHotel"
								onclick="return askConfirmation(this);">
							<i class="fa fa-plus-square fa-fw" ></i>
							Создать глобальный отель
						</button>							
						<button id="search-sims-btn" class="btn btn-info" data-id="{{ $providerHotel->id }}">
							<span id="status">
								<i class="fa fa-search-plus fa-fw"></i>
							</span>
							Найти похожие отели
						</button>
					</div>
					<p>
						<img class="thumbnail" style="margin-left: auto; margin-right: auto;" src="{{ $providerHotel->photo }}" />
					<p>
					<p><strong>Поставщик: </strong>{{ $providerHotel->provider->name }}</p>
					<p><strong>Код поставщика: </strong>{{ $providerHotel->provider_hotel_id}}</p>
					<p><strong>№ MTL: </strong>{{ $providerHotel->id }}</p>
					<p><strong>Страна: </strong>{{ $providerHotel->country_name or '-' }}</p>
					<p><strong>Город: </strong>{{ $providerHotel->city_name or '-'}}</p>
					<p><strong>Добавлен: </strong>{{ $providerHotel->created_at }}</p>
					<p><strong>Адрес: </strong>{{ $providerHotel->address }}</p>
					<p><strong>Описание: </strong>{{ $providerHotel->description }}</p>
					<p><strong>Телефон: </strong>{{ $providerHotel->phone or '-' }}</p>
					@if ($providerHotel->g_latitude && $providerHotel->g_longitude)
							<p><strong>Координаты: </strong>{{ $providerHotel->g_latitude }} / {{ $providerHotel->g_longitude }}&nbsp
								<img style="width: 16px;" src="/images/gmaps_icon.ico">
						@else
							<p><strong>Координаты: </strong>{{ $providerHotel->latitude }} / {{ $providerHotel->longitude }}
						@endif
					<p><strong>Email: </strong>{{ $providerHotel->email or '-' }}</p>
					<p><strong>Сайт отеля: </strong>{{ $providerHotel->url or '-' }}</p>

					<!--<div class="readmore">Описание</div>-->
				</div>

			</div>
			@if ($providerHotel->raw_data)
			<button class="btn btn-info" id="data-expand">Отобразить данные</button>
			@endif
		</div>

				<!--// Карточки похожих отелей-->

				<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

					@if (count($hotels) > 10)

					<p><b>Выборка слишком большая. Уточните критерии поиска</b></p>
					@else
					<p><b>Найдено отелей: {{ count($hotels) }}</b></p>

					@foreach ($hotels as $hotel)
					<div class="panel panel-primary">
						<div class="panel-heading"><a href="/hotel/{{ $hotel->hotel_id }}" style="color:#FFF;">{{ $hotel->hotel_name }}</a>
							<span>
								<a class="link-button" 
									id="global-link-button" 
									href="#confirmation-window"
									data-toggle="modal"
									data-id="{{ $hotel->hotel_id }}"
									data-title="Подтверждение привязки"
									data-text="Подтвердите привязку отеля поставщика к &laquo{{ $hotel->hotel_name }}&raquo"
									data-handler="linkToGlobal"
									data-toggle="tooltip"
									title="Привязать"
									onclick="return askConfirmation(this);"
									>
									<i class="fa fa-link fa-lg"></i>
								</a>
							</span>
						</div>
						<div class="panel-body">

							@if ($hotel->photo)
								<p><a class="btn btn-default open-modal-img" href="#modal-image-window" 
								   data-toggle="modal" data-img-url="{{ $hotel->photo }}" data-hotel-name="{{ $hotel->hotel_name }}">
									<i class="fa fa-file-image-o fa-fw"></i>Показать фото
									</a></p>

							@endif
							<p><strong>№ MTL: </strong>{{ $hotel->hotel_id }} </p>
							<p><strong>Страна: </strong><span {!! hilite('country_name', $hotel->hls) !!}>{{ $hotel->country_name }}</p>
							<p><strong>Город: </strong>{{ $hotel->city_name }}<span {!! hilite('city_name', $hotel->hls) !!}></p>
							<p><strong>Добавлен: </strong><span {!! hilite('created_at', $hotel->hls) !!}>{{ $hotel->created_at }}</span></p>
							<p><strong>Адрес: </strong><span {!! hilite('address', $hotel->hls) !!}>{{ $hotel->address }}</span></p>
							<p><strong>Описание: </strong><span class="shorten">
									<span {!! hilite('description', $hotel->hls) !!}>{{ $hotel->description or '-'}}</span>
								</span>
							</p>
							<p><strong>Координаты: </strong>
								<span {!! hilite('coords', $hotel->hls) !!}>{{ $hotel->latitude }} / {{ $hotel->longitude }}</span>
							</p>
							<p><strong>Телефон: </strong><span {!! hilite('phone', $hotel->hls) !!}>{{ $hotel->phone or '-' }}</span></p>
							<p><strong>Email: </strong><span {!! hilite('email', $hotel->hls) !!}>{{ $hotel->email or '-' }}</p>
							<p><strong>Сайт отеля: </strong><span {!! hilite('url', $hotel->hls) !!}>{{ $hotel->url or '-' }}</span></p>
							
						</div>
					</div>
					@endforeach

					@endif


					<!--Похожие отели-->
					<p><b>Найдено похожих отелей: {{ count($similarHotels) }}</b></p>
					@foreach ($similarHotels as $hotel)
					<div class="panel panel-primary">
						<div class="panel-heading"><a href="/hotel/{{ $hotel->id }}" style="color: #FFF">{{ $hotel->name_ru }}</a>
							<span>
								<a class="link-button" id="similar-link-button" data-id = "{{ $hotel->link_id }}" href="#confirmation-window"
									data-toggle="modal"
									data-id="{{ $hotel->link_id }}"
									data-title="Подтверждение привязки"
									data-text="Подтвердите привязку отеля поставщика к &laquo{{ $hotel->name_ru }}&raquo"
									data-handler="linkToSimilar"
									onclick="return askConfirmation(this);"
									data-toggle="tooltip"
									title="Привязать">
									<i class="fa fa-link fa-lg"></i>
								</a>
							</span>
						</div>
						<div class="panel-body">

							@if ($hotel->photo)
								<p><a class="btn btn-default open-modal-img" href="#modal-image-window" 
								   data-toggle="modal" data-img-url="{{ $hotel->photo }}" data-hotel-name="{{ $hotel->name_ru }}">
									<i class="fa fa-file-image-o fa-fw"></i>Показать фото
									</a>
								</p>
							@endif
							<p><strong>Совпадение: </strong>{{ $hotel->match_level * 100 }}%</p>
							<p><strong>№ MTL: </strong>{{ $hotel->id }} </p>
							<p><strong>Страна: </strong><span{!! hilite('country_name', $hotel->hls) !!}>{{ $hotel->country_name }}</span></p>
							<p><strong>Город: </strong><span{!! hilite('city_name', $hotel->hls) !!}>{{ $hotel->city_name }}</span></p>
							<p><strong>Добавлен: </strong><span{!! hilite('created_at', $hotel->hls) !!}>{{ $hotel->created_at }}</span></p>
							<p><strong>Адрес: </strong><span {!! hilite('address', $hotel->hls) !!}>{{ $hotel->address }}</span></p>
							<p><strong>Описание: </strong>
								<span class="shorten"><span{!! hilite('description', $hotel->hls) !!}>{{ $hotel->description or '-' }}</span>
									
								</span>
							</p>
							<p><strong>Координаты: </strong>
								<span{!! hilite('coords', $hotel->hls) !!}>{{ $hotel->latitude }} / {{ $hotel->longitude }}</p>
								<p><strong>Телефон: </strong><span{!! hilite('phone', $hotel->hls) !!}>{{ $hotel->phone or '-' }}</span></p>
								<p><strong>Email: </strong><span{!! hilite('email', $hotel->hls) !!}>{{ $hotel->email or '-' }}</span></p>
							<p><strong>Сайт отеля: </strong><span{!! hilite('url', $hotel->hls) !!}>{{ $hotel->url or '-' }}</p>
							
						</div>
					</div>
					<!--</div>-->
					@endforeach
					
					
				</div>

		<!-- окно подтверждения, в которое нужно передать заголовок, текст, данные и навесить нужный обработчик на кнопку подтверждения -->
		<div id="confirmation-window" class="modal fade" role="dialog">
			  <div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
				  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
				  </div>
				  <div class="modal-body">
				  </div>
				  <div class="modal-footer">
				  <button type="button" class="btn btn-danger" id="confirm-button">Подтвердить</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
				  </div>
				</div>

			  </div>
			</div>
		
		<script>

			// назначаем функцию, которая будет вызвана при нажатии на подтверждение
			function assignHndlr(data, callback) {

				$('#confirmation-window').find('#confirm-button').on('click', data, callback);
			}

			function askConfirmation(el) {

				var win = $('#confirmation-window');

				win.find('.modal-title').html($(el).data('title'));
				win.find('.modal-body').html($(el).data('text'));

				// передаем данные в колбэк-функцию
				var data = {id: $(el).data('id')};

				assignHndlr(data, window[($(el).data('handler'))]);
			}
		// обработчики
		providerHotelId = {{ $providerHotel->id }};

		$('#search-sims-button').click(function (e) {


		});

		// создание нового
		var createHotel = function (e) {
			
			$('#confirmation-window').modal('hide');
			
			$.ajax({
					url: "/provider_hotel/create_hotel",
					data: { providerHotelId: providerHotelId },
					type: 'POST',
					success: function (data) {
						if (data.result)
							window.location.href = '/hotel/' + data.id;
						else alert(data.message);
					},
					error: function () {
						response([]);
					}
			});
		}


		// связывание с найденным глобальным
		var linkToGlobal = function (e) {
			
			$('#confirmation-window').modal('hide');
			
			$.ajax({
					url: "/provider_hotel/link_to_global",
					type: 'POST',
					data: {  globalHotelId: e.data.id, providerHotelId: providerHotelId },
					success: function (data) {
						location.reload();
					},
					error: function () {
						response([]);
					}
			});
		}

		// связывание с похожим
		var linkToSimilar = function (e) {
			
			$('#confirmation-window').modal('hide');
			
			$.ajax({
					url: "/provider_hotel/link_to_similar",
					type: 'POST',
					data: { linkId: e.data.id, providerHotelId: providerHotelId },
					success: function (data) {
						location.reload();
					},
					error: function () {
						response([]);
					}
			}); 
		}

		$('.open-modal-img').click(function (e) {
			$('#modal-image-window #modal-image').attr('src', $(this).attr('data-img-url'));
			$('#modal-image-window #caption').html($(this).attr('data-hotel-name'));
		});

		// связываем поле радиуса с картой
			$( "#radius-field" ).change(function() {

				searchDistance = parseInt($(this).val());
				map.setMarkerRadius();
			});
		
//		// показать данные 
//		$('#data-expand').click(function(){
//				var btn = $(this);
//				$('#raw-data').slideToggle('fast', function (){
//					
//					if ($(this).is(':hidden')) 
//						btn.text('Отобразить данные');
//					else btn.text('Скрыть данные');
//				});
//		});
		
		$('.shorten').shorten({
			moreText: 'Открыть',
			lessText: 'Скрыть'
		});
	</script>
	
	<script>
		$( "#search-sims-btn" ).click(function( e ) {
			
				var btn = $(this),
					status = btn.find('#status'),
					sender = '<i class="fa fa-search-plus fa-fw"></i>', 
					spinner = '<i class="fa fa-spinner fa-fw fa-spin"></i>';
				
					status.find('i').remove();
					status.append(spinner);
				
				$.ajax({
					url: '/search/sims',
					method: 'post',
					data : {id: $(this).data('id')},
					success: function(data){
						console.log(data);
						status.find('i').remove();
						status.append(sender);
						btn.blur();
						if (data.result)
							location.reload();
					},
					error: function () {
						status.find('i').remove();
						status.append(sender);
						alert('Произошла ошибка');
					}
				});
				
				return false;
			});
	</script>
		

	<script type="text/javascript" src="{{ URL::asset('js/form.js') }}"></script>
	@endif
	
	@else
		Отель не найден
	@endif
	
</div>
@if ($providerHotel && $providerHotel->raw_data)

<div class="panel-body" id="raw-data" style="display:none;">
	<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
		<pre><code class="json">{{ $providerHotel->raw_data }}</code></pre>
	</div>
</div>

<script>
			$('#data-expand').click(function(){
				
				var btn = $(this);
				$('#raw-data').slideToggle('fast', function (){
					
					if ($(this).is(':hidden')) 
						btn.text('Отобразить данные');
					else btn.text('Скрыть данные');
				});
			});
</script>
		
	
@endif

@endsection