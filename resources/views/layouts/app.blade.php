@include ('includes.head')
<body>
	
	<!-- Модальное окно с изображением -->
	<div id="modal-image-window" class="modal fade" role="dialog">
		<div class="window">
			
			<div class="modal-content">
				<div class="header">
					<div id="caption"></div>
					<button type="button" id="close" data-dismiss="modal">&times;</button>
				</div>
				<img id="modal-image"></img>
			</div>
		</div>
	</div>
		
    <div id="wrapper">
		
		@if (Auth::check())
        <!-- Navigation -->
			@include('includes.header')

		@endif
		<div class="container">
			
		</div>
        <div id="page-wrapper">
	    
		@if ( Session::has('action_result_message') ) 
			<!--TODO: Сделать нормальные отступы средствами bootstrap-->
			<br>
			<div class="alert alert-success">
				{!! Session::get('action_result_message') !!}
			</div>
		@endif
		
		@if ( Session::has('warning_message') ) 
			<!--TODO: Сделать нормальные отступы средствами bootstrap-->
			<br>
			<div class="alert alert-warning">
				{{ Session::get('warning_message') }}
			</div>
		@endif
		
		@if ( Session::has('error_message') ) 
			<!--TODO: Сделать нормальные отступы средствами bootstrap-->
			<br>
			<div class="alert alert-danger">
				{{ Session::get('error_message') }}
			</div>
		@endif
		
		@if (!empty($page))
			<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header">{{ $page }}</h1>
				</div>
                <!-- /.col-lg-12 -->
			</div>
		@endif
            <!-- /.row -->
	<div class="row">

		    @yield('content')
	</div>
            <!-- /.row -->
		
		<!--Нижнее меню-->
		<!--@include ('includes.footer')-->
        </div>
        <!-- /#page-wrapper -->
	
		
    </div>
    <!-- /#wrapper -->

@include ('includes.bottom')