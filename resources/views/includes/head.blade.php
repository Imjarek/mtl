<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>MTL</title>
		
    <!-- Bootstrap Core CSS -->
    <link href="{{ URL::asset('/sb2/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ URL::asset('/sb2/vendor/metisMenu/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ URL::asset('/sb2/dist/css/sb-admin-2.css') }}" rel="stylesheet">
	<!-- Addition CSS -->
	
	<link href="{{ URL::asset('/css/custom.css') }}" rel="stylesheet">
	
	    
    <!-- Morris Charts CSS -->
    <link href="{{ URL::asset('/sb2/vendor/morrisjs/morris.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ URL::asset('/sb2/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	
	<link href="{{ URL::asset('/css/font-awesome-animation.min.css') }}" rel="stylesheet" type="text/css">
	
	<link rel="stylesheet" href="{{ URL::asset('js/hljs/styles/default.css' ) }}">
	
	<link rel="stylesheet" href="{{ URL::asset('jquery-ui/jquery-ui.min.css') }}">
	
	<script src="{{ URL::asset('js/common.js') }}"></script>
	<script src="{{ URL::asset('js/hljs/highlight.pack.js') }}"></script>
	<script>hljs.initHighlightingOnLoad();</script>
	
	<!-- jQuery -->
    <script src="{{ URL::asset('/sb2/vendor/jquery/jquery.min.js') }}"></script>
    
    <script src="{{ URL::asset('jquery-ui/jquery-ui.min.js') }}"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/jqueryui-editable/css/jqueryui-editable.css" rel="stylesheet"/>
	<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/jqueryui-editable/js/jqueryui-editable.min.js"></script>
<!--	<script src="{{ URL::asset('x-edit/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>
	<link href="{{ URL::asset('x-edit/bootstrap3-editable/css/bootstrap-editable.css' ) }}" rel="stylesheet"/>-->
	
	<link rel="stylesheet" href="{{ URL::asset('css/overrides.css' ) }}">
	
	<script src="{{ URL::asset('js/jquery.shorten.min.js') }}"></script>
    <script>
	    $.ajaxSetup({
				headers: {
				    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
    </script>
	 
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		
</head>
