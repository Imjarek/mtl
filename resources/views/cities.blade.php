@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
	
		<form action="/cities" method="GET" class="form-horizontal" autocomplete="off" id="seach-cities-form">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="city-field" class="col-md-1 control-label">Город</label>

                <div class="col-md-3">
                    <input type="text" name="city_name" id="city-field" class="form-control" value="{{ old('city_name') }}">
                </div>
				
				 <label for="country-field" class="col-md-1 control-label">Страна</label>

                <div class="col-md-3">
                    <input type="text" name="country_name" id="country-field" class="form-control" value = "{{ old('country_name') }}" data-toggle="tooltip" title="Выберите страну из списка">
                </div>
				<input type="hidden" name="country_id">
				<input type="hidden" name="sorting" value="{{ old('sorting') }}">
				<input type="hidden" name="sorting_param" value="{{ old('sorting_param') }}">
				<button type="submit" class="btn btn-info">
					<i class="fa fa-search fa-fw"></i>Найти
                </button>
            </div>
			
        </form>
		
		<div class="row">
			<div class="col-md-2"><b>Всего городов: {{ $total }}</b></div>
		</div>
		
		@if (count($cities) > 0)
		<div class="page-links">{{ $cities->appends(Request::except('page'))->links() }}</div>
        <div class="panel panel-default">
            
			
            <div class="panel-body">
                <table class="table table-striped task-table">

	                <thead>
						<th style="width: 15%">№</th>
                        <th style="width: 50%">
							<a class="sorting-switch" data-sorting-param="name_ru">Город</a>
							<b id="sorting-icon" class="{{ old('sorting_param') == 'name_ru' ? old('sorting') : ''}}"></b>
						</th>
						<th>
							<a class="sorting-switch" data-sorting-param="country_name">Страна</a>
							<b id="sorting-icon" class="{{ old('sorting_param') == 'country_name' ? old('sorting') : ''}}"></b>
						</th>
                    </thead>

                    <tbody>
                        @foreach ($cities as $city)
                            <tr>
                                <td>
                                   {{ $city->id }}
                                </td>
                                <td class="table-text">
                                    <div><a href="/city/{{ $city->id }}">{{ $city->name_ru }}</a></div>
                                </td>
								<td>
									{{ $city->country->name_ru }}
								</td>

                                
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
			
        </div>
		<div class="page-links">{{ $cities->appends(Request::except('page'))->links() }}</div>
	@else
		<strong>Ничего не найдено</strong>
    @endif
	
	
	<script>
		
		// сортировка
		
		$('.sorting-switch').click(function () {
			
			var form = $('#seach-cities-form'),
				icon = $(this).next('#sorting-icon'),
				order = icon.attr('class');
		
			if (!order || order === 'desc')
				order = 'asc';
			else
				order = 'desc';
			
			form.find('input[name="sorting"]').val(order);
			
			if (order)
				form.find('input[name="sorting_param"]').val($(this).data('sorting-param'));
			
			form.submit();
			
		});
		
		
		$('#search-cities-form').submit(function (e) {
			
			var field = $('#country-field');
			if (field.val() && !$('[name=country_id]').val()) {
				
				field.tooltip();
				return false;
			}
		});
		
		$('#country-field').autocomplete({
			source: function (request, response) {
				$.ajax({
					url: "services/countries",
					data: { q: request.term },
					success: function (data) {
						response(data.suggestions);
					},
					error: function () {
					    response([]);
					}
				});

			},
			minLength: 3,
			delimiter: /(,|;)\s*/,
			maxHeight: 400,
			width: 300,
			zIndex: 9999,
			params: { },
			select: function(data, ui){
				$('[name=country_id]').val(ui.item.id);
			},
			change: function () {
				$('[name=country_id]').val(null);
			}
	//		lookup: []
		});
		
		// отличается только url (сделать рефакторинг)
		
		
		$('#city-field').autocomplete({
			source: function (request, response) {
				$.ajax({
					url: "services/cities",
					data: { q: request.term },
					success: function (data) {
						response(data.suggestions);
					},
					error: function () {
					    response([]);
					}
				});

			},
			minLength: 3,
			delimiter: /(,|;)\s*/,
			maxHeight: 400,
			width: 300,
			zIndex: 9999,
			params: { },
			onSelect: function(data, value){ 
				
			}
	//		lookup: []
		});
	</script>					  

@endsection