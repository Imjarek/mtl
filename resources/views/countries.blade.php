@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
		
		@if (count($countries) > 0)
		<form action="/countries" id="seach-countries-form" method="GET">
			<input type="hidden" name="sorting" value="{{ old('sorting') }}">
			<input type="hidden" name="sorting_param" value="{{ old('sorting_param') }}">
		</form>
				
		<div class="page-links">{{ $countries->links() }}</div>
        <div class="panel panel-default">
         			
            <div class="panel-body">
                <table class="table table-striped task-table">

                    <!-- Table Headings -->
                    <thead>
						<th>№</th>
                        <th style="width: 60%">
							<a class="sorting-switch" data-sorting-param="name_ru">Страна</a>
							<b id="sorting-icon" class="{{ old('sorting_param') == 'name_ru' ? old('sorting') : ''}}"></b>
						</th>
                        <th></th>
                    </thead>

                    <tbody>
                        @foreach ($countries as $country)
                            <tr>
								<td>{{ $country->id }}</td>
                                <td>
                                    <div><a href="/country/{{ $country->id }}">{{ $country->name_ru }}</a></div>
                                </td>

                                <td>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
	<div class="page-links">{{ $countries->links() }}</div>
	<script>
		
		// сортировка
		$('.sorting-switch').click(function () {
			
			var form = $('#seach-countries-form'),
				icon = $(this).next('#sorting-icon'),
				order = icon.attr('class');
		
			if (!order || order === 'desc')
				order = 'asc';
			else
				order = 'desc';
			
			form.find('input[name="sorting"]').val(order);
			
			if (order)
				form.find('input[name="sorting_param"]').val($(this).data('sorting-param'));
			
			form.submit();
			
		});
	</script>
	
    @endif
	
        <!-- New Task Form -->
<!--        <form action="/task" method="POST" class="form-horizontal">
            {{ csrf_field() }}

             Task Name 
            <div class="form-group">
                <label for="task" class="col-sm-3 control-label">Task</label>

                <div class="col-sm-6">
                    <input type="text" name="name" id="task-name" class="form-control">
                </div>
            </div>

             Add Task Button 
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> Add Task
                    </button>
                </div>
            </div>
        </form>
    </div>-->

    <!-- TODO: Current Tasks -->
@endsection