<!--loahhotel mail report template-->

<div style="font-weight: bold;"><h2>Отчет сопоставленных отелей</h2></div>

@if (count($report->cities) > 0)
	@foreach ($report->cities as $city)

	<div class = "city-section">
		<p style="color: green; font-weight: bold;">
			<h3>MTL: {{ $city->name_ru }} ({{ $city->id }})</h3>
		</p>
		@if (count($city->hotels))
			<table cellpadding="5" style="border: solid; border-width: 1px; border-color: grey; padding: 10px; margin: 5px 0px 5px 0px;">
			    
			    <thead style="background-color: #98fef4;">
			    <th>ID (MTL)</th><th>Название (MTL)<th>Адрес (MTL)</th><th>Совпадение</th><th>Поставщик</th><th>ID (пост.)</th><th>Отель (пост.)</th><th>Адрес (пост.)</th>
			</thead>
			<tbody>
				@foreach ($city->hotels as $hotel)
				<tr @if ($loop->iteration % 2 == 0)style="background-color: #d7f0f5" @endif>
				    <td>{{ $hotel->hotel_id }} </td>
				    <td><a href="{{ $report->url }}/hotel/{{ $hotel->hotel_id}}">{{ $hotel->hotel_name_ru }}</a></td>
				    <td>{{ mb_substr($hotel->hotel_address, 0, 24).'...' }}</td>
				    <td><a href="{{ $report->url }}/match/{{ $hotel->match_id }}">{{ $hotel->match_level * 100 }}%</a></td>
				    <td><b>{{ $hotel->provider }}</b></td>
				    <td>{{ $hotel->provider_hotel_id }}</td>
				    <td><a href="{{ $report->url }}/provider_hotel/{{ $hotel->pid }}">{{ $hotel->provider_hotel_name_ru }}</a></td>
				    <td>{{ mb_substr($hotel->provider_hotel_address, 0, 24).'...' }}</td>
				</tr>
				@endforeach
			</tbody>
			</table>
		@else <span style="font-size: 12px">Нет сопоставленных отелей в этом городе</span>
		@endif
	</div>

	@endforeach
	@else <div>Нет данных для отчета</div>
@endif
	

