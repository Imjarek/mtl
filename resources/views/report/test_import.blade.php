<!--loahhotel mail report template-->

<h2 style="height: 32px; vertical-align: middle;"><strong>MTL: Отчет сопоставленных отелей</strong></h2>

@if (count($report->managers) > 0)
	@foreach ($report->managers as $manager)

	<div class = "city-section">
		
		@if (count($manager->hotels) > 0)
			<p style="color: green;">
				<h3><strong>Менеджер: </strong> {{ $manager->fio }}</h3>
			</p>
			<table style="width: 100%; vertical-align: middle; border: solid; border-width: 1px; border-color: grey; padding: 10px; margin: 5px 0px 5px 0px;">
			    
				<thead style="background-color: #f2b91c; height: 24px;">
					<th style="width: 25%">Город</th>
					<th style="width: 25%">Отель</th>
					<th style="width: 25%">Отель ACASE</th>
					<th style="width: 15%">Контактная информация ACASE</th>
					<th>Отель BVK</th>
					<th>Контактная информация BVK</th>
					<th>Отель OSTROVOK</th>
					<th>Контактная информация OSTROVOK</th>
					<th>Отель BedsOnline</th>
					<th>Контактная информация BedsOnline</th>
					<th>Отель HotelBook</th>
					<th>Контактная информация HotelBook</th>
				</thead>
				<tbody>
					@foreach ($manager->hotels as $hotel)

					<tr style="height: 24px; @if ($loop->iteration % 2 == 0) background-color: #d7f0f5"; @endif">
						<td>
							{{ $hotel->city }}
						</td>
						<td>
<!--							<a href="{{-- $report->url }}/hotel/{{ $hotel->id --}}"
								style="color: black; text-decoration: none; font-weight: bold">-->
								{{ $hotel->name }}
							<!--</a>-->
						</td>
						<td>{{ $hotel->acase_name or '-' }}</td>
						<td>{{ $hotel->acase_info or '-' }}</td>
						@if ($hotel->bvk_name)
							<td>{{ $hotel->bvk_name }}</td>
						@else
							<td style="color:red; font-weight: bold;">-
							</td>
						@endif
						<td>{{ $hotel->bvk_info or '-' }}</td>
						<td>{{ $hotel->ostrovok_name or '-' }}</td>
						<td>{{ $hotel->ostrovok_info or '-' }}</td>
						<td>{{ $hotel->bol_name or '-' }}</td>
						<td>{{ $hotel->bol_info or '-' }}</td>
						<td>{{ $hotel->hb_name or '-' }}</td>
						<td>{{ $hotel->hb_info or '-' }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<div style="background-color: #019043; height: 5px; margin-top: 10px;"></div>
		@endif
	</div>

	@endforeach
	@else <div>Нет данных для отчета</div>
@endif
	

