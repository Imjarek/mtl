<!--loahhotel mail report template-->

<h2 style="height: 32px; vertical-align: middle;"><strong>MTL: Отчет сопоставленных отелей</strong></h2>

@if (count($report->cities) > 0)
	@foreach ($report->cities as $city)

	<div class = "city-section">
		
		@if (count($city->hotels) > 0)
			<p style="color: green;">
				<h3><strong>Город: </strong> {{ $city->name }} ({{ $city->id }})</h3>
				<h3><strong>ACASE: </strong>{{ $city->acase_name }} ({{ $city->acase_id }})</h3>
				<h3><strong>BVK: </strong>{{ $city->bvk_name }} ({{ $city->bvk_id }})</h3>
			</p>
			<table cellpadding="5" style="width: 100%; vertical-align: middle; border: solid; border-width: 1px; border-color: grey; padding: 10px; margin: 5px 0px 5px 0px;">
			    
				<thead style="background-color: #f2b91c; height: 24px;">
					<th style="width: 25%">Отель</th>
					<th>Дата добавления</th>
					<th style="width: 25%">Отель ACASE</th>
					<th style="width: 15%">ACASE</th>
					<th>Отель BVK</th>
					<th>BVK</th>
				</thead>
				<tbody>
					@foreach ($city->hotels as $hotel)

					<tr style="height: 24px; @if ($loop->iteration % 2 == 0) background-color: #d7f0f5"; @endif">
						<td>
							<a href="{{ $report->url }}/hotel/{{ $hotel->id }}"
								style="color: black; text-decoration: none; font-weight: bold">
								{{ $hotel->name }}
							</a>
						</td>
						<td>{{ $hotel->date }}</td>
						<td>{{ $hotel->acase_name or '-' }}</td>
						<td>{{ $hotel->acase_date or '-' }}</td>
						@if ($hotel->bvk_name)
							<td>{{ $hotel->bvk_name }}</td>
						@else
							<td style="color:red; font-weight: bold;">Отсутствует в BVK
							</td>
						@endif
						<td>{{ $hotel->bvk_date or '-' }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<div style="background-color: #019043; height: 5px; margin-top: 10px;"></div>
		@endif
	</div>

	@endforeach
	@else <div>Нет данных для отчета</div>
@endif
	

