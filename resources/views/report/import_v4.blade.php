<!--loahhotel mail report template-->

<h3 style="height: 32px; vertical-align: middle;"><strong>MTL: Отчет сопоставленных отелей</strong></h3>

		@if (count($manager->hotels) > 0)
			<p style="color: green;">
				<h4><strong>Менеджер: </strong> {{ $manager->fio }}</h4>
			</p>
			<table style="width: 100%;">
				<tbody>
					<tr style="font-weight: bold">
						<td style="width: 20%;">Менеджер</td>
						<td style="width: 20%;">Страна</td>
						<td style="width: 20%;">Город</td>
						<td style="width: 30%;">Отель</td>
						<td style="width: 30%;">Отель ACASE</td>
						<td style="width: 40%;">Контактная информация ACASE</td>
						<td style="width: 30%;">Отель BVK</td>
						<td style="width: 40%;">Контактная информация BVK</td>
						<td style="width: 30%;">Отель OSTROVOK</td>
						<td style="width: 40%;">Контактная информация OSTROVOK</td>
						<td style="width: 30%;">Отель BedsOnline</td>
						<td style="width: 40%;">Контактная информация BedsOnline</td>
						<td style="width: 30%;">Отель HotelBook</td>
						<td style="width: 40%;">Контактная информация HotelBook</td>
					</tr>
					@foreach ($manager->hotels as $hotel)

					<tr @if ($loop->iteration % 2 == 0) style="background-color: #e6ffff"; @endif">
						<td>
							{{ $hotel->fio }}
						</td>
						<td>
								{{ $hotel->country }}
						</td>
						<td>
							{{ $hotel->city }}
						</td>
						<td>
								{{ $hotel->name }}
						</td>
						<td>{{ $hotel->acase_name or '-' }}</td>
						<td>{{ $hotel->acase_info or '-' }}</td>
						@if ($hotel->bvk_name)
							<td>{{ $hotel->bvk_name }}</td>
						@else
							<td style="color:red; font-weight: bold;">-
							</td>
						@endif
						<td>{{ $hotel->bvk_info or '-' }}</td>
						<td>{{ $hotel->ostrovok_name or '-' }}</td>
						<td>{{ $hotel->ostrovok_info or '-' }}</td>
						<td>{{ $hotel->bol_name or '-' }}</td>
						<td>{{ $hotel->bol_info or '-' }}</td>
						<td>{{ $hotel->hb_name or '-' }}</td>
						<td>{{ $hotel->hb_info or '-' }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
	@else <p>Нет данных для отчета</p>
@endif
	

