<!--loahhotel mail report template-->

<div style="font-weight: bold;"><h2>Отчет сопоставленных отелей</h2></div>

@if (count($report->cities) > 0)
	@foreach ($report->cities as $city)

	<div class = "city-section">
		
		@if (count($city->hotels) > 0)
			<p style="color: green; font-weight: bold;">
			<h3>MTL: {{ $city->name_ru }} ({{ $city->id }})</h3>
			</p>
			<table cellpadding="5" style="border: solid; border-width: 1px; border-color: grey; padding: 10px; margin: 5px 0px 5px 0px;">
			    
			    <thead style="background-color: #98fef4;">
			    <th>ID</th><th>Название<th>Адрес</th><th>Поставщики</th>
			</thead>
			<tbody>
				@foreach ($city->hotels as $hotel)
					
			
				<tr @if ($loop->iteration % 2 == 0)style="background-color: #d7f0f5" @endif>
				    <td>{{ $hotel->id }} </td>
				    <td><a href="{{ $report->url }}/hotel/{{ $hotel->id }}">{{ $hotel->name_ru }} </td>
				    <td>{{ $hotel->address }} </td>
				    <td>
					@if ($hotel->providers)
						{{ $hotel->providers }}
					@endif
				    </td>
				</tr>
				@endforeach
			</tbody>
			</table>
		@endif
	</div>

	@endforeach
	@else <div>Нет данных для отчета</div>
@endif
	

