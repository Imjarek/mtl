@extends('layouts.app')

@section('content')


@if (count($unions))
	<!-- Union Undo Confirmation Window -->
		<div id="confirmWindow" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					 <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Подтверждение отмены объединения</h4>
						  </div>
						  <div class="modal-body">
						<p>Подтвердите отмену объединения</p>
						  </div>
					 <div class="modal-footer">
						<button type="button" class="btn btn-danger" onclick="return undoUnion($('#confirmWindow').data('log-id'));">Подтвердить</button>
					  <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
				  </div>
				</div>

			</div>
		</div>
@endif


<div class="panel-body">
	@include('common.errors')
	
	@if (!$hotel)
		<div>
			Отель не найден
		</div>	
	@else
	
	<!-- Union Undo Confirmation Window -->
		<div id="confirmDeleteWindow" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					 <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Подтверждение удаления отеля</h4>
						  </div>
						  <div class="modal-body">
						<p>Подтвердите удаление отеля</p>
						  </div>
					 <div class="modal-footer">
						<button type="button" class="btn btn-danger" onclick="return deleteHotel({{ $hotel->id }});">Подтвердить</button>
					  <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
				  </div>
				</div>

			</div>
		</div>
	
	
	<script>
		function initMap() {
			
			var place = {lat: {{ $hotel->latitude }}, lng: {{ $hotel->longitude }}};
			var map = new google.maps.Map(document.getElementById('hotel-gmap'), {
				zoom: 14,
				center: place,
				scrollwheel: false
			});
			
			// маркер гл. отеля
			
			new google.maps.Marker({
				position: place,
				map: map,
				zIndex: 0
			});
				
			// маркеры отелей поставщиков

			pHMarkerData = [];

			@foreach ($providerHotels as $providerHotel)

				pHMarkerData.push({
					position: {lat: {{ $providerHotel->latitude}}, lng: {{ $providerHotel->longitude }} },
					title: '{{ $providerHotel->hotel_name }} ({{ $providerHotel->city_name or '-' }})',
					url: '/hotel/{{ $providerHotel->hotel_id }}'
				});

			@endforeach
			
			if (pHMarkerData.length)

				pHMarkerData.forEach(function (marker) {
					new google.maps.Marker({
						position: marker.position, 
						map: map,
						icon: '/images/p-hotel-marker.png',
						title: marker.title
					});
				});
				
		}
	</script>
		
	<script>
		var id = {{ $hotel->id }};
	</script>
		<div class="panel-body">
			
			<!--Карточка отеля-->
			<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-primary">
				  <div class="panel-heading">{{ $hotel->name_ru }}</div>
				  <div class="panel-body">
					  <div style="margin-bottom: 25px;">	
					  <button id="edit-btn" class="btn btn-success">
						   <i class = "fa fa-pencil fa-fw"></i>
						   Редактировать
					  </button>
					  <button id="cancel-edit-btn" class="btn btn-default" style ="margin-left: 12px; display: none!important;">
						  <i class="fa fa-undo fa-fw"></i>
						  Отмена</button>
					  <button 
								class="btn btn-danger pull-right"
								data-target="#confirmDeleteWindow"
								data-toggle="modal"
								title="Удалить отель">
							<i class="fa fa-trash-o fa-fw"></i>
							Удалить
						 </button>
					  </div>
					@if ($hotel->photo)
					<div>
						<img class="thumbnail" style="margin-left: auto; margin-right: auto;" src="{{ $hotel->photo }}" />
					</div>
					@endif
						<p><strong>№ MTL: </strong>{{ $hotel->id }}
<!--					<p><strong>Поставщик: </strong>MTL</p>-->
					  <p>
						  <strong>Страна: </strong>
							@if (isset($hotel->country->id))
							<a id="country" href="/country/{{ $hotel->country->id }}">
								{{ $hotel->country->name_ru or '-' }}
							</a>
							@else -
							@endif
					  </p>
					<p><strong>Город: </strong>
						<a class="editable-field" id="city" href="/city/{{ $hotel->city->id or '#'}}">
							{{ $hotel->city->name_ru or '-'}}
						</a>
					</p>
					<p><strong>Добавлен: </strong><span class="editable-field" id="created_at">{{ $hotel->created_at }}</span></p>
					<p><strong>Адрес: </strong><span class="editable-field" id="address">{{ $hotel->address }}</span></p>
					<p><strong>Описание: </strong><span class="editable-field" id="description">{{ $hotel->description }}</span></p>
					<p><strong>Телефон: </strong><span class="editable-field" id="phone">{{ $hotel->phone or '-' }}</span></p>
					<p><strong>Email: </strong><span class="editable-field" id="email">{{ $hotel->email or '-' }}</span></p>
					<p><strong>Сайт: </strong><span class="editable-field" id="url">{{ $hotel->url or '-' }}</span</p>
					<p><strong>PMS: </strong><span class="editable-field" id="pms_id">{{ $hotel->pms->name_ru or '-' }}</span></p>
					<p><strong>Координаты: </strong><span class="editable-field" id="latitude">{{ $hotel->latitude }}</span> / <span class="editable-field" id="longitude">{{ $hotel->longitude }}</span>
						@if (count($unions))
						<p>
							<strong>Объединения:</strong>
						<ul>
							@foreach ($unions as $union)
							<li style="margin: 5px;">Объединен с № {{ $union->hotel_id }} <b>{{ $union->name_ru }}</b>
								<button class="undo-union-link btn btn-default btn-sm" data-id="{{ $union->id }}" data-target="#confirmWindow" data-toggle="modal" title="Отменить объединение">
									<i class="fa fa-undo fa-fw"></i>Отменить
								</button>
							</li>
							@endforeach
						</ul>
						</p>
						@endif
					<!--<div class="readmore">Описание</div>-->
				  </div>
				  <div class="panel-footer" style="padding: 0px;">
					  <div class="middle-gmap" id="hotel-gmap"></div>
					  
					  <script async defer
					  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMLm-fqO3_-sT-m0oFbY-a7CvBpyopqVI&callback=initMap">
					  </script>
				  </div>
				</div>
			
				<!--Cвязанные отели поставщиков -->
			</div>
			@if (count($providerHotels) > 0)
			
			<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
				@foreach ($providerHotels as $hotel)

					<div class="panel panel-primary">
					    <div class="panel-heading" style="background-color: #c8d3d5">
					      
							<a href="/provider_hotel/{{ $hotel->hotel_id }}" style="font-weight: bold">
								{{ $hotel->hotel_name }}
							</a>  {{ $hotel->not_exists ? '[Отсутствует в новых данных]' : '' }}
							<span class="unlink-btn" data-ref-id="{{ $hotel->hotel_id }}" style="float: right">
								<a data-toggle="modal" href="#confirmUnlinkWindow">
									<i class="fa fa-unlink fa-lg"></i>
								</a>
							</span>
					  </div>
					  <div class="panel-body">
							@if ($hotel->photo)
							<p><a class="btn btn-default open-modal-img" href="#modal-image-window" 
							   data-toggle="modal" data-img-url="{{ $hotel->photo }}" data-hotel-name="{{ $hotel->hotel_name }}">
								<i class="fa fa-file-image-o fa-fw"></i>Показать фото
								</a></p>
								
							@endif
							<p><strong>Поставщик: </strong>{{ $hotel->provider_name }}</p>
							<p><strong>Код поставщика: </strong>{{ $hotel->provider_hotel_id }}</p>
							<p><strong>№ MTL: </strong>{{ $hotel->hotel_id }}
							<p><strong>Страна: </strong><span{!! hilite('country_name', $hotel->hls) !!}>{{ $hotel->country_name or '-' }}</span></p>
							<p><strong>Город: </strong><span{!! hilite('city_name', $hotel->hls) !!}>
								{{ $hotel->city_name or '-'}}
							</p>
							<p><strong>Добавлен: </strong><span{!! hilite('created_at', $hotel->hls) !!}>{{ $hotel->created_at }}</span></p>
							<p><strong>Адрес: </strong><span{!! hilite('address', $hotel->hls) !!}>{{ $hotel->address }}</span></p>
							<p><strong>Описание: </strong>
								<span class="shorten">
									<span{!! hilite('description', $hotel->hls) !!}>
										{{ $hotel->description }}
									</span>
								</span>
							</p>
							<p><strong>Телефон: </strong><span{!! hilite('phone', $hotel->hls) !!}>{{ $hotel->phone or '-' }}</span></p>
							@if ($providerHotel->g_latitude && $providerHotel->g_longitude)
								<p><strong>Координаты: </strong>
									<span {!! hilite('coords', $hotel->hls) !!}>
										{{ $providerHotel->g_latitude }} / {{ $providerHotel->g_longitude }}&nbsp
									<img style="width: 16px;" src="/images/gmaps_icon.ico">
							@else
								<p><strong>Координаты: </strong>{{ $providerHotel->latitude }} / {{ $providerHotel->longitude }}
							@endif
							<p><strong>Email: </strong><span {!! hilite('email', $hotel->hls) !!}>{{ $hotel->email or '-' }}</span></p>
							<p><strong>Сайт: </strong><span {!! hilite('url', $hotel->hls) !!}>{{ $hotel->url or '-' }}<span></p>
							

					  </div>

					</div>

				@endforeach
				
				<script>
				$('.open-modal-img').click(function (e) {
					$('#modal-image-window #modal-image').attr('src', $(this).attr('data-img-url'));
					$('#modal-image-window #caption').html($(this).attr('data-hotel-name'));
				});
			</script>
			</div>
			
			<div id="confirmWindow" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					 <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Подтверждение отмены объединения</h4>
						  </div>
						  <div class="modal-body">
						<p>Подтвердите отмену объединения</p>
						  </div>
					 <div class="modal-footer">
						<button type="button" class="btn btn-danger" onclick="return undoUnion($('#confirmWindow').data('log-id'));">Подтвердить</button>
					  <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
				  </div>
				</div>

			</div>
		</div>
			
			<div id="confirmUnlinkWindow" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						 <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Подтверждение отвязки</h4>
							  </div>
							  <div class="modal-body">
							<p>Подтвердите отвязку отеля поставщика</p>
							  </div>
						 <div class="modal-footer">
							<button type="button" id="confirm-unlink-btn" class="btn btn-danger">Подтвердить</button>
						  <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
					  </div>
					</div>

				</div>
			</div>
			@else
			<div>
				Нет связанных отелей
			</div>
			@endif
			
		</div>
	
	<script>
		$.fn.editable.defaults.mode = 'inline';
		
		//$.fn.editable.defaults.ajaxOptions = {};
		
		window.hotelData = {};
		
		var callback = function (response, value) {
			var field = $(this);
			
			window.hotelData[field.attr('id')] = value;
			
			$('#edit-btn').prop('disabled', false);
		};
			
		var url = '/hotel/edit';
		
		var text = {
			type: 'text',
			pk: id,
			text: 'Укажите значение поля',
			success: callback,
			emptytext: '-',
			disabled: true
		};
		
		var textarea = {
			type: 'textarea',
			pk: id,
			text: 'Укажите значение поля',
			success: callback,
			emptytext: '-',
			disabled: true
		};
		
		var date = {
			type: 'date',
			pk: id,
			text: 'Укажите значение поля',
			success: callback,
			emptytext: '-',
			disabled: true
		};
		
		var select = {
			type: 'select',
			pk: id,
			text: 'Укажите значение поля',
			success: callback,
			source: '/services/pms_list',
			emptytext: '-',
			disabled: true
		};
		
		$('.editable-field#description').editable(textarea);
		$('.editable-field#city').editable(text);
		$('.editable-field#country').editable(text);
		$('.editable-field#address').editable(text);
		$('.editable-field#phone').editable(text);
		$('.editable-field#email').editable(text);
		$('.editable-field#url').editable(text);
		$('.editable-field#latitude').editable(text);
		$('.editable-field#longitude').editable(text);
		$('.editable-field#pms_id').editable(select);
		$('.shorten').shorten({
			moreText: 'Открыть',
			lessText: 'Скрыть'
		});
		
	</script>
	
	<script>
	
	saveHotelData = function () {
			
		if (window.hotelData) {
			
			$('#edit-btn').prop('disabled', true);
			
			$.post({
				url: url,
				data: {hotelId: id, data: window.hotelData},
				success: function (response) {
					console.log(response);

					if (response.warnings)
						alert(response.warnings);
					if (!response || !response.result) {
						$('#edit-btn').append('<i class="fa fa-exclamation-circle fa-fw"></i>');
					}
					else {
						$('#edit-btn').text('Редактировать').find('i').remove();
						$('#edit-btn').prepend('<i class="fa fa-edit fa-fw"></i>');
						$('#edit-btn').attr('editMode', 'off');
						$('#cancel-edit-btn').hide();
						toggleEditableMode();
						applyEditBtnHndler();
					}

					$('#edit-btn').prop('disabled', false);
				},
				error: function (result) {
					alert.log(result)
					$('#edit-btn').append('<i class="fa fa-exclamation-circle fa-fw"></i>');
				}
			});
		}
	};
	
	// кнопа редактирование
	// 
	applyEditBtnHndler();
	
	function applyEditBtnHndler() {
		
		$('#edit-btn').one('click', function () {
			
			var editIcon = '<i class="fa fa-edit fa-fw"></i>',
					saveIcon = '<i class="fa fa-save fa-fw"></i>';

			if ($(this).attr('editMode') != 'on') {
				$(this).text('Сохранить');
				$(this).find('i').remove();
				$(this).prepend(saveIcon);
				$(this).attr('editMode', 'on');
				$(this).one('click', saveHotelData);
				$(this).prop('disabled', true);
				$('#cancel-edit-btn').show();
			}
			else {
				$(this).text('Редактировать');
				$(this).find('i').remove();
				$(this).append(editIcon);
				$(this).attr('editMode', 'off');
				$('#cancel-edit-btn').hide();
			}
			toggleEditableMode();

			// обработчик посылающий данные на сервер

		});
	}
	
	$('#cancel-edit-btn').click(function () {
		location.reload();
//		var editIcon = '<i class="fa fa-edit fa-fw"></i>';
//		$('#edit-btn').find('i').remove();
//		$('#edit-btn').text('Редактировать').prepend(editIcon).attr('editMode', 'off');
//		$('#edit-btn').prop('disabled', false);
//		$('#edit-btn').off('click');
//		toggleEditableMode();
//		$(this).hide();
//		applyEditBtnHndler();
		
	});
	// передаем ID записи лога в окно подтверждения
	$('.undo-union-link').click(function () {
		
		$('#confirmWindow').data('log-id', $(this).data('id'));
		console.log($(this).data('id'));
	});
	
	function undoUnion(logId) {
		
		$.post( '/union/undo', {logId: logId}, function(data) {
			
			console.log(data);

			if (!data.result)
				alert(result.message);
			
				window.location.reload();
		});
	}
	
	$('.unlink-btn').click(function () {
		$('#confirm-unlink-btn').data('ref-id', $(this).data('ref-id'))
	});
	
	$('#confirm-unlink-btn').click(function (e) {
		
		var ref_id = $(this).data('ref-id');
		
		$.post('/unlink/', {ref_id: ref_id}, function (data){
			
			if (!data.result)
				alert(data.message);
			window.location.reload();
		});
	});
	
	function deleteHotel (id) {
		
		$.post('/hotel/delete', {id: id}, function (data){
			
			if (!data.result) 
				alert(data.message);
			window.location.href = '/hotels';
		});
		
	}
	
	function toggleEditableMode() {
			
			var fields = ['description', 'city', 'country', 'address', 'phone', 'email', 'url', 'latitude', 'longitude', 'pms_id'];
			
			fields.forEach(function (field){
				var el = '.editable-field#' + field;
				
				$(el).editable('toggleDisabled');
				
			});				
		}
	
</script>
    @endif
</div>

@endsection