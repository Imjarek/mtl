@extends('layouts.app')

@section('content')

	@include('common.errors')
	
	@if ($hotel1 && $hotel2)
		
		<script>
		function initMaps() {
		  var place = {lat: {{ $hotel1->latitude }}, lng: {{ $hotel1->longitude }}};
		  var map = new google.maps.Map(document.getElementById('hotel1-gmap'), {
			zoom: 14,
			center: place,
			scrollwheel: false
		  });
		  var marker = new google.maps.Marker({
			position: place,
			map: map
		  });
		  
		  var place = {lat: {{ $hotel2->latitude }}, lng: {{ $hotel2->longitude }}};
		  var map = new google.maps.Map(document.getElementById('hotel2-gmap'), {
			zoom: 14,
			center: place,
			scrollwheel: false
		  });
		  var marker = new google.maps.Marker({
			position: place,
			map: map
		  });
		}
		</script>
		
		<!-- Union Confirmation Window -->
		<div id="confirmUnionWindow" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Подтвердите объединение отелей:</h4>
		      </div>
		      <div class="modal-body">
			<p>&laquo{{ $hotel1->name_ru}}&raquo и &laquo{{ $hotel2->name_ru}}&raquo</p>
		      </div>
		      <div class="modal-footer">
			  <button type="button" class="btn btn-danger" onclick="return confirmUnion();">Подтвердить</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
		      </div>
		    </div>

		  </div>
		</div>
		
		<!--Карточки отелей-->
		<div class="col-md-12" style="margin-bottom: 10px">
		    <div class="btn btn-primary pull-right" data-toggle="modal" data-target="#confirmUnionWindow">Подтвердить</div>
		</div>
		    
			<!--Отель № 1 (основной)-->
			<div class="col-md-6" id="main-hotel-card">
				<div class="panel panel-primary checkbox-spacing">
					<div class="panel-heading">Основной отель</div>
					<div class="panel-body">
						<div style="display: inline-block; margin-bottom: 10px;">
							<input type="checkbox" id="photo" checked style="float: left;">
							<img class="thumbnail" style="margin-left: auto; margin-right: auto;" src="{{ $hotel1->photo }}" />
						</div>
						<p><span style="margin: 12px;"></span><strong>№ MTL: </strong>{{ $hotel1->id }}</p>
						<p><input type="checkbox" id="name_ru" checked><strong>Название: </strong><a href="/hotel/{{ $hotel1->id }}">{{ $hotel1->name_ru }}</a></p>
						<p><input type="checkbox" id="country" checked><strong>Страна: </strong>{{ $hotel1->country->name_ru or '-'}}</p>
						<p><input type="checkbox" id="city" checked><strong>Город: </strong>
							<a href="/city/{{ $hotel1->city->id or '#'}}">{{ $hotel1->city->name_ru or '-'}}</a>
						</p>
						<p><input type="checkbox" id="created_at" checked><strong>Добавлен: </strong>{{ $hotel1->created_at }}</p>
						<p><input type="checkbox" id="address" checked><strong>Адрес: </strong>{{ $hotel1->address }}</p>
						<p><input type="checkbox" id="description" checked><strong>Описание: </strong>{{ $hotel1->description }}</p>
						<p><input type="checkbox" id="phone" checked><strong>Телефон: </strong>{{ $hotel1->phone or '-'}}</p>
						<p><input type="checkbox" id="email" checked><strong>Email: </strong>{{ $hotel1->email or '-' }}</p>
						<p><input type="checkbox" id="url" checked><strong>Сайт: </strong>{{ $hotel1->url or '-' }}</p>
						<p><input type="checkbox" id="pms_id" checked><strong>PMS: </strong>{{ $hotel1->pms->name_ru or '-' }}</p>
						<p><input type="checkbox" id="coords" checked>
							<strong>Координаты: </strong>{{ $hotel1->latitude }} / {{ $hotel1->longitude }}
						</p>
					  @if (count($unions))
						<p>
							<strong>Объединения:</strong>
						<ul>
							@foreach ($unions as $union)
							<li style="margin: 5px;">Объединен с № {{ $union->hotel_id }} <b>{{ $union->name_ru }}</b>
							</li>
							@endforeach
						</ul>
						</p>
						@endif
					  <!--<div class="readmore">Описание</div>-->
				  </div>
				  <div class="panel-footer" style="padding: 0px;">
					  <div class="middle-gmap" id="hotel1-gmap"></div>
				  </div>
				</div>
			    
				@if (count($hotel1providerHotels))
			
					@foreach ($hotel1providerHotels as $providerHotel)

						<div class="panel panel-primary">
						    <div class="panel-heading" style="background-color: #c8d3d5">
								<a href="/provider_hotel/{{ $providerHotel->hotel_id }}" style="font-weight: bold">
									{{ $providerHotel->hotel_name }}
								</a>
							</div>
						  <div class="panel-body">
							   <p><strong>Поставщик: </strong>{{ $providerHotel->provider_name }}</p>
						      <p><strong>Код поставщика: </strong>{{ $providerHotel->provider_hotel_id }}</p>
						       <p><strong>№ MTL: </strong>{{ $providerHotel->hotel_id }}</p>
						  </div>
						</div>
					@endforeach
				@else
				<div>
					Нет связанных отелей
				</div>
				@endif
			</div>
			<div class="col-md-6">
				<div class="panel panel-primary">
				  <div class="panel-heading">Вспомогательный отель</div>
				  <div class="panel-body">
						<div>
							<img class="thumbnail" style="margin-left: auto; margin-right: auto;" src="{{ $hotel2->photo }}" />
						</div>
					  <p><strong>№ MTL: </strong>
							<span{!! hiLite('id', $hls) !!}>
								{{ $hotel2->id }}
							</span</p>
				  <p><strong>Название: </strong><a href="/hotel/{{ $hotel2->id }}"><span{!! hiLite('name_ru', $hls) !!}>{{ $hotel2->name_ru }}</span></a></p>
						<p><strong>Страна: </strong><span{!! hiLite('country_name', $hls) !!}>{{ $hotel2->country->name_ru or '-'}}</span></p>
						<p><strong>Город: </strong><span{!! hiLite('city_name', $hls) !!}><a href="/city/{{ $hotel2->city->id or '#'}}">{{ $hotel2->city->name_ru or '-' }}</a></p>
						<p><strong>Добавлен: </strong><span{!! hiLite('created_at', $hls) !!}>{{ $hotel2->created_at }}</span></p>
						<p><strong>Адрес: </strong><span{!! hiLite('address', $hls) !!}>{{ $hotel2->address }}</span></p>
						<p><strong>Описание: </strong><span{!! hiLite('description', $hls) !!}>{{ $hotel2->description }}</span></p>
						<p><strong>Телефон: </strong><span{!! hiLite('phone', $hls) !!}>{{ $hotel2->phone }}</a></p>
						<p><strong>Email: </strong><span{!! hiLite('email', $hls) !!}>{{ $hotel2->email }}</span></p>
						<p><strong>Сайт: </strong><span{!! hiLite('url', $hls) !!}>{{ $hotel2->url }}</p>
						<p><strong>PMS: </strong><span{!! hiLite('pms', $hls) !!}>{{ $hotel2->pms->name_ru or '-' }}</span></p>
						<p><strong>Координаты: </strong>
							<span{!! hiLite('coords', $hls) !!}>
								{{ $hotel2->latitude }} / {{ $hotel2->longitude }}</span></p>
						<!--<div class="readmore">Описание</div>-->
				  </div>
				  <div class="panel-footer" style="padding: 0px;">
					  <div class="middle-gmap" id="hotel2-gmap"></div>
					  
					  <script async defer
					  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMLm-fqO3_-sT-m0oFbY-a7CvBpyopqVI&callback=initMaps">
					  </script>
				  </div>
				</div>
				
				@if (count($hotel2providerHotels))
			
					@foreach ($hotel2providerHotels as $providerHotel)

						<div class="panel panel-primary">
						    <div class="panel-heading" style="background-color: #c8d3d5">
								<a href="/provider_hotel/{{ $providerHotel->hotel_id }}" style="font-weight: bold">
									{{ $providerHotel->hotel_name }}
								</a>
							</div>
						  <div class="panel-body">
							   <p><strong>Поставщик: </strong>{{ $providerHotel->provider_name }}</p>
						      <p><strong>Код поставщика: </strong>{{ $providerHotel->provider_hotel_id }}</p>
						       <p><strong>№ MTL: </strong>{{ $providerHotel->hotel_id }}</p>
						  </div>
						</div>

					@endforeach
				
				@else
				<div>
					Нет связанных отелей
				</div>
				@endif
			</div>
			<!--Отели поставщиков -->
			<script>
				function confirmUnion(mainHotelId, supplementaryHotelId) {

					// что с чем объединяем и список полей
					// находим НЕотмеченные поля
					
					fields = [];
					var arr = $("#main-hotel-card").find('input:checkbox:not(:checked)');
					
					arr.each(function (i){
						fields.push($(this).attr('id'));
					});
					
					console.log(fields);
					
					var data = {
						mainHotelId: {{ $hotel1->id }},
						supplementaryHotelId: {{ $hotel2->id }},
						fields: fields
					};
					
					$.post( '/union/confirm', data, function(data) {
						//	console.log(data);
						
						if (!data.result)
							alert(data.message);
						window.location = '/hotel/{{ $hotel1->id }}';
						
					});
				}
			</script>
			<script>
				setCookie('union_page_visited', 1, 1);
			</script>

	@else
	<div>
		Отели не выбраны
	</div>
	@endif



@endsection

