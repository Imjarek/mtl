@extends('layouts.app')

@section('content')
<style>
	.edit {
		background-color: #6bef70!important;
	};
</style>
<script>
	
	var cityId = {{ $city->id }}, 
		cityName = "{{ $city->name_ru }}";
	
	function initMap() {
		var place = {lat: {{ $city->latitude }}, lng: {{ $city->longitude }}};
		var map = new google.maps.Map(document.getElementById('city-gmap'), {
			zoom: 5,
			center: place
		});
		var marker = new google.maps.Marker({
			position: place,
			title: '{{ $city->name_ru }}',
			map: map
		});
	}
</script>

<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMLm-fqO3_-sT-m0oFbY-a7CvBpyopqVI&callback=initMap">
</script>

<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
    
 <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
		
        <div class="panel panel-default">
            <div class="panel-heading">
                
		{{ $city->name_ru }}
		</div>
	
	<div class="panel-body">	

	@if (count($provider_cities) > 0)
		<h4>Город у поставщиков</h4>
		<button id="edit-btn" class="btn btn-success pull-right">
						   <i class = "fa fa-pencil fa-fw"></i>
						   Редактировать
					  </button>
		<table id="provider-cities-tbl" class="table table-striped task-table">

            <thead>
				<th>Поставщик</th>
                <th>Идентификатор города у поставщика</th>
				<th>Город</th>
            </thead>
            <tbody>
				@foreach ($provider_cities as $provider_city)
					<tr>
						<td>
						   {{ $provider_city->provider->name }}
						</td>
						<td class="editable-field" id="pci-{{ $provider_city->provider_id}}" class="table-text">
							{{ $provider_city->provider_city_id }}
					   </td>
					   <td class="editable-field" id="pcn-{{ $provider_city->provider_id}}" class="table-text">
							{{ $provider_city->name_ru }}
					   </td>
					</tr>
				@endforeach
                    </tbody>
        </table>
	
		
	@else <h4>Этот город не связан с городами поставщиков</h4>
	@endif
	<button id="add-btn" class="btn btn-info pull-left" style ="display: none!important;">
		<i class="fa fa-plus fa-fw"></i>
	Добавить
	</button>
	<button disabled id="save-btn" class="btn btn-danger pull-right" style ="display: none!important; margin-left: 15px;">
		<i class="fa fa-save fa-fw"></i>
	Сохранить
	</button>
	<button id="cancel-btn" class="btn btn-default pull-right" style ="display: none!important;">
		<i class="fa fa-undo fa-fw"></i>
		Отмена
	</button>
	</div>
	     <div class="panel-footer" style="padding: 0px;">
					  <div class="gmap" id="city-gmap"></div>
	     </div>
	</div>	    

<script>
	
	cityData = {};
	
	var callback = function (response, value) {
		
		var field = $(this);
		var data = {};
		var fieldId = field.attr('id');
		var arr = fieldId.split('-');
		var provider_id = arr[1];

		if (!window.cityData[provider_id])
			window.cityData[provider_id] = {};

		if (arr[0] === 'pci') {
			if (!Number.isInteger(parseInt(value))) {
				alert('Укажите правильный ID города поставщика');
				return;
			}
			window.cityData[provider_id].providerCityId = value;
		}

		if (arr[0] === 'pcn') {
			if (!value)
				window.cityData[provider_id] = {unlink: true};
			else
				window.cityData[provider_id].providerCityName = value;
		}

		$('#save-btn').prop('disabled', false);
	};
	
	
	
	$.fn.editable.defaults.mode = 'inline';
	
	// TODO: сформировать только необходимые поля согласно присутствущим поставщикам ($providers)
	var providers = [1,2,3,4,5,6];
	
	// для ID
	providers.forEach(function (pid) {
		
		var IdFieldSelector = '.editable-field#pci-' + pid,
			NameFieldSelector = '.editable-field#pcn-' + pid;
		
		$(IdFieldSelector).editable({
			type: 'text',
			pk: cityId,
			text: 'Укажите значение поля',
			success: callback,
			emptytext: '-',
			disabled: true
		});
		
		$(NameFieldSelector).editable({
			type: 'text',
			pk: cityId,
			text: 'Укажите значение поля',
			success: callback,
			emptytext: '-',
			disabled: true
		});
	});
	
	
	$('#edit-btn').click(function () {
		
		$(this).prop('disabled', true);
		$('#cancel-btn').show();
		$('#save-btn').show();
		$('#add-btn').show();
			
		$('.editable-field').each(function (){
			$(this).editable('option', 'disabled', false);
		});
		
	});
	
	$('#add-btn').click(function () {
		
		var row = '<tr id=\"new-row\"><td>\n\
				<select id="provider-select">\n\
						<option selected>--- Поставщик ---</option>\n\
							@foreach ($providers as $provider)
							<option value={{ $provider->id }}>{{ $provider->name }}</option>\n\
							@endforeach
				</select>\n\
			</td><td></td><td id=\"pcn-1\">{{ $city->name_ru }}</td></tr>';
		
		$('#provider-cities-tbl tr:last').after(row);
		
		$('#provider-select').on('change', function (event, value) {
			
			$('#cancel-btn').prop('disabled', false);
			$('#save-btn').prop('disabled', false);
			
			var newId = 'pcn-' + this.value;
			var cell = $('#new-row').find("td[id*='pcn-']");
	
			cell.attr('id', newId);
			
			window.cityData[this.value] = {newProviderCity: cityName }; // запрос на создание нового города поставщика
		});
	});
	
	$('#cancel-btn').click(function () {
		location.reload();
	});
	
	$('#save-btn').click(function () {
		
		$(this).prop('disabled');
		
		$.post({
				url: '/city/edit',
				data: {cityName: cityName, cityId: cityId, data: window.cityData},
				success: function (response) {
					
					if (response.result)
						location.reload();
					else alert(response.message);
				},
				error: function (response) {
					alert('Произошла ошибка при сохранении');
					console.log(response);
				}
		});
		
	});
</script>
@endsection