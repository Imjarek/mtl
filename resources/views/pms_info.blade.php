@extends('layouts.app')

@section('content')

    <div class="panel-body">
        @include('common.errors')
		
		
		@if ($pms)
		<!--<div class="page-links">{{-- $pmss->links() --}}</div>-->
        <div class="panel panel-default">
         	<div class="panel-heading">{{ $pms->name_ru }}</div>
			<div class="panel-body">
				<p>
					<button class="btn btn-info" data-toggle="modal" data-target="#editPmsWindow"><i class="fa fa-edit fa-fw"></i>Редактировать</button>
					<br>

				</p>
				<p>
				<strong>Название: </strong>{{ $pms->name_ru }}
					
				</p>
				
				@if (count($hotels))
				<p>
					Отели использующие эту систему
				</p>
				<div class="panel-body">
					<table class="table table-striped task-table">

						<!-- Table Headings -->
						<thead>
							<th>№</th>
							<th>Отель</th>
							<th>Город</th>
							<th>Страна</th>
						</thead>

						<!-- Table Body -->
						<tbody>
							@foreach ($hotels as $hotel)
								<tr>
									<!-- Task Name -->
									<td class="table-text">
										<div>{{ $hotel->id }}</div>
									</td>
									<td>
										<a href="/hotel/{{ $hotel->id }}">{{ $hotel->name_ru }}</a>
									</td><td>
									   {{ $hotel->city->name_ru or '-'}}
									</td>
									<td>
									   {{ $hotel->country->name_ru or '-' }}
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				@else
					Не найдены отели использующие эту систему
				@endif
			</div>
        </div>
	<!--<div class="page-links">{{-- $pmss->links() --}}</div>-->
	
	<div id="editPmsWindow" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<div class="modal-content" style="width: 100%; margin-left: 0px;">
					<div class="modal-header">
						<!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
						<h4 class="modal-title">Редактирование свойств PMS</h4>
					 </div>
					
					<form action="/pms/edit/{{ $pms->id }}">
						<div class="modal-body"  style="width: 400px">

							  <label for="pms-name-field" control-label>Название</label>
							  <input type="text" name="name" id="pms-name-field" class="form-control" value="{{ $pms->name_ru }}">

						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-info">Сохранить</button>
							<button class="btn btn-default" data-dismiss="modal">Отмена</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	
	@else
		<h3>Элемент не найден</h3>
    @endif
	
		
	
@endsection