@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
	
<!--		<form action="/cities" method="GET" class="form-horizontal" autocomplete="off" id="seach-cities-form">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="city-field" class="col-md-1 control-label">Город</label>

                <div class="col-md-3">
                    <input type="text" name="city_name" id="city-field" class="form-control" value="{{ old('city_name') }}">
                </div>
				
				 <label for="country-field" class="col-md-1 control-label">Страна</label>

                <div class="col-md-3">
                    <input type="text" name="country_name" id="country-field" class="form-control" value = "{{ old('country_name') }}" data-toggle="tooltip" title="Выберите страну из списка">
                </div>
				<input type="hidden" name="country_id">
				<input type="hidden" name="sorting" value="{{ old('sorting') }}">
				<input type="hidden" name="sorting_param" value="{{ old('sorting_param') }}">
				<button type="submit" class="btn btn-info">
					<i class="fa fa-search fa-fw"></i>Найти
                </button>
            </div>
			
        </form>-->
	
		
		@if (count($entries) > 0)
		<!--<div class="page-links">{!-- $entries->appends(Request::except('page'))->links() --}}</div>-->
        <div class="panel panel-default">
            
			
            <div class="panel-body">
                <table class="table table-striped task-table">

	                <thead>
						<th>ID</th>
                        <th>
							<a class="sorting-switch" data-sorting-param="name_ru">Команда</a>
							<!--<b id="sorting-icon" class="{{ old('sorting_param') == 'name_ru' ? old('sorting') : ''}}"></b>-->
						</th>
						<th>
							<a class="__sorting-switch" data-sorting-param="mtl_country.name_ru">Инициатор</a>
							<!--<b id="sorting-icon" class="{{ old('sorting_param') == 'country.name_ru' ? old('sorting') : ''}}"></b>-->
						</th>
						<th>
							<a class="__sorting-switch" data-sorting-param="mtl_country.name_ru">Отправлена</a>
							<!--<b id="sorting-icon" class="{{ old('sorting_param') == 'country.name_ru' ? old('sorting') : ''}}"></b>-->
						</th>
						<th>
							<a class="__sorting-switch" data-sorting-param="mtl_country.name_ru">Выполнена</a>
							<!--<b id="sorting-icon" class="{{ old('sorting_param') == 'country.name_ru' ? old('sorting') : ''}}"></b>-->
						</th>
						<th style="width:25%">
							<a class="__sorting-switch" data-sorting-param="mtl_country.name_ru">Опции</a>
							<!--<b id="sorting-icon" class="{{ old('sorting_param') == 'country.name_ru' ? old('sorting') : ''}}"></b>-->
						</th>
                    </thead>

                    <tbody>
                        @foreach ($entries as $entry)
                            <tr>
                                <td>
                                   {{ $entry->id }}
                                </td>
                                <td class="table-text">
									{{ $entry->sys_name }} {{ $entry->name ? '('.$entry->name.')' : ''}}
                                </td>
								<td class="table-text">
									{{ $entry->uid == 0 ? 'Робот' : '-'}}
                                </td>
								<td>
									{{ $entry->sent_at }}
								</td>
								<td>
									{{ $entry->completed_at }}
								</td>
								<td>
									{{ $entry->options }}
								</td>

                                
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
			
        </div>
	@else
		<strong>Нет записей</strong>
    @endif
	
	
@endsection