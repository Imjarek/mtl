<?php

namespace App;


//use Illuminate\Mail\Mailer;

use Mail;
use Illuminate\Mail\Message;

class ActivationService
{

    protected $mailer;

    protected $activationRepo;

    protected $resendAfter = 6;

    public function __construct(ActivationRepository $activationRepo)
    {
        $this->activationRepo = $activationRepo;
    }

    public function sendActivationMail($user)
    {

        if ($user->activated || !$this->shouldSend($user)) {
            return;
        }

        $token = $this->activationRepo->createActivation($user);

        $link = route('user.activate', $token);
		
		Mail::send('emails.email_confirm', ['user_name' => $user->name, 'link' => $link], function ($m) use ($user) {

            $m->to($user->email, $user->name)->subject('Подтверждение адреса почты');
        });
		
//        $this->mailer->raw($message, function (Message $m) use ($user) {
//            $m->to($user->email)->subject('Подтверждение адреса почты');
//        });
    }

    public function activateUser($token)
    {
        $activation = $this->activationRepo->getActivationByToken($token);

        if ($activation === null) {
            return null;
        }

        $user = User::find($activation->user_id);

        $user->activated = true;

        $user->save();

        $this->activationRepo->deleteActivation($token);

        return $user;

    }

    private function shouldSend($user)
    {
        $activation = $this->activationRepo->getActivation($user);
        return $activation === null || strtotime($activation->created_at) + 60 * 60 * $this->resendAfter < time();
    }

}