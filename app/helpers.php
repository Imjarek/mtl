<?php

/* 
 * @author Sergey Cherednichenko
 * Помогаторы
 */

// Функция возвращает класс подсветки если найден ключ в массиве (не)соответствий
function hilite($key, $arr) {
	
	if (!is_array($arr))
		return '';
	
	if (in_array($key, $arr))

		//return ' class="hilite"';
		
		return ' style="background-color: #d6f3fe;"';
}
