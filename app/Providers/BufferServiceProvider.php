<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Mtl\Classes\Buffer;

class BufferServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	
	protected $defer = true;
	
	public function boot()
	{
	    //
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('App\Interfaces\BufferInterface', function(){

			return new Buffer();
		});
	}
	
	public function provides () {
		return ['App\Interfaces\BufferInterface'];
	}
}
