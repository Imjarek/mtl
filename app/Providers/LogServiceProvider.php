<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Mtl\Classes\Log;

//use App\Mtl\Interfaces\LogInterface;

class LogServiceProvider extends ServiceProvider
{	
	//protected $defer = true;
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
	    
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('App\Mtl\Interfaces\LogInterface', function ($app) {
			return new Log();
		});
	}

	 /**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return ['App\Mtl\Interfaces\LogInterface'];
	}
}
