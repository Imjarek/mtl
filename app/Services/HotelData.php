<?php

namespace App\Services;

use \App\Mtl\Provider\Ostrovok\OstrovokHotelImporter;
use \App\Mtl\Provider\Acase\AcaseHotelImporter;
use \App\Mtl\Provider\Bronevik\BronevikHotelImporter;
use \App\Mtl\Provider\Amadeus\AmadeusHotelImporter;
use \App\Mtl\Provider\Bol\BolHotelImporter;
use \App\Mtl\Provider\Bol\HotelBookHotelImporter;

class HotelData
{	
	public function __construct() {
	}
	public function import() {
        
		echo "Загрузка отелей...\n";
	
		BronevikHotelImporter::start();
		AcaseHotelImporter::start();
		OstrovokHotelImporter::start();
		AmadeusHotelImporter::start();
		BolHotelImporter::start();
		HotelBookHotelImporter::start();
	}
}
