<?php

namespace App\Services;

use \App\Mtl\Provider\Ostrovok\OstrovokCountryImporter;
use \App\Mtl\Provider\Ostrovok\OstrovokCityImporter;

use \App\Mtl\Provider\Acase\AcaseCountryImporter;
use \App\Mtl\Provider\Acase\AcaseCityImporter;

use \App\Mtl\Provider\Bronevik\BronevikCountryImporter;
use \App\Mtl\Provider\Bronevik\BronevikCityImporter;

//use \App\Mtl\Provider\Amadeus\AmadeusCountryImporter;
//use \App\Mtl\Provider\Amadeus\AmadeusCityImporter;

// нижележащие вызовы нужно разнести по разным скриптам
class GeoData
{	
	public function __construct() {
	}
    public function importCountries ()
    {
        echo "Загрузка стран...\n";
		
		//BronevikCountryImporter::start();
		//OstrovokCountryImporter::start();
		//AcaseCountryImporter::start();
		//BolCountryImporter::start(); // эти и последующие будут уже в отдельных скриптах
		//HotelBookCountryImporter;
		//AmadeusCountryImporter::start();
    }
	public function importCities() {
		
		echo "Загрузка городов...\n";
		
		//BronevikCityImporter::start();
		OstrovokCityImporter::start();
		//AcaseCityImporter::start();
		//BolCityImporter::start(); // эти и последующие будут уже в отдельных скриптах
		//HotelBookCityImporter::start();
		//AmadeusCityImporter::start();
	}
}