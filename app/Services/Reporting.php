<?php

namespace App\Services;

use App\Mtl\Classes\HotelImportReporter;

class Reporting {
	
	public function __construct() {
		
	}
	public function send() {
		
		HotelImportReporter::$toAddress = $this->email;
		
		HotelImportReporter::send();
	}
}