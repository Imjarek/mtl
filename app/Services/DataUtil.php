<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

use App\Models\MtlHotel;

class DataUtil {
	
	public function ExportBvkCountries() {
		
		$filename = base_path('../../bin/dumps/bronevik/bvk_countries.json');
		
		echo "экспорт стран броневика\n";
		
		$data = DB::select("
				SELECT
					id
					,name name_ru
					,latname name_en
					,iso3_alpha2 name_code
				FROM bvk_cities.countrys;
		");
		
		$data = json_encode($data, true);
		
		$res = file_put_contents($filename, $data);
		
		if ($res)
			echo "Данные стран сохранены\n";
		else 
			echo "Ошибка\n";
	}
	public function ExportBvkCities() {
		
		echo "экспорт городов броневика\n";
		
		$filename = base_path('../../bin/dumps/bronevik/bvk_cities.json');
		
		echo "экспорт стран броневика\n";
		
		$data = DB::select("
			SELECT 
				ci.id
				,ci.name name_ru
				,ci.latname name_en
				,ci.latitude
				,ci.longitude
				,co.id country_id
				,co.iso3_alpha2 country_code
			FROM bvk_cities.citys ci
			join bvk_cities.countrys co
			on ci.country = co.id
			where ci.del = 0
			-- and (latitude is not null and latitude > 0)
			order by ci.latname;
			-- limit 1000; -- Убрать ограничение (будет жырный json)
		");
		
		$data = json_encode($data, true);
		
		$res = file_put_contents($filename, $data);
		
		if ($res)
			echo "Данные городов сохранены\n";
		else 
			echo "Ошибка сохранения дампа\n";
		}
	public function ExportBvkHotels() {
		echo "экспорт отелей броневика\n";
		
		$filename = base_path('../../bin/dumps/bronevik/bvk_hotels.json');
		
		$data = DB::select("
			SELECT 
				id
				,title name
				,latitude
				,longitude
				,country
				,city
				,email
				,site
				,address
				,phone
				,description
				,concat('https://bronevik.com/static/photo/', main_image, '_400') photo
			FROM bvk_cities.hotels
			where latitude is not null
			and longitude is not null
			and del != 1;
		");
		
		$data = json_encode($data, true);
		
		$res = file_put_contents($filename, $data);
		
		if ($res)
			echo "Данные отелей сохранены\n";
		else 
			echo "Ошибка\n";
	}
	public function Geocoding () {
		
		// функционал вынес в класс RequestGeocode // можно удалить
		MtlHotel::all()->each(function ($hotel){
		
			
			// 
			$geodata = self::geoCode($hotel->city->name_ru.', ' . $hotel->address);
			
			if ($geodata) {
				
				$hotel->formatted_address = @$geodata->formatted_address ? $geodata->formatted_address : $geodata->address;

				$hotel->google_place_id = $geodata->place_id;

				$hotel->save();

				var_dump(@$geodata->formatted_address);
			}
		});
	}
	public function geoCode($address) {
		
		$address = urlencode($address);
	
		$ch = curl_init("https://maps.googleapis.com/maps/api/geocode/json?language=ru&address=".$address);
				
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		//curl_setopt($ch, CURLOPT_HEADER, TRUE);
		//curl_setopt($ch, CURLOPT_VERBOSE, TRUE); 
		curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, TRUE);

		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array('Accept-Language' => 'ru-ru,ru;q=0.8'));

		//curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		
		$result = json_decode(curl_exec($ch));
		
		curl_close($ch);
		
		if ($result->results)
			return $result->results[0];
		
	}
}
