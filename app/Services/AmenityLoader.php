<?php

namespace App\Services;

use App\Mtl\Provider\Ostrovok\OstrovokAmenityLoader;

use App\Mtl\Provider\Acase\AcaseAmenityLoader;

class AmenityLoader {
	
	public function __construct() {
		
	}
	public function start() {
		
		//OstrovokAmenityLoader::load();
		AcaseAmenityLoader::load();
	}
}