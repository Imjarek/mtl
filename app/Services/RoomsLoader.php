<?php

namespace App\Services;

use App\Mtl\Provider\Ostrovok\OstrovokRoomsLoader;

use App\Mtl\Provider\Acase\AcaseRoomsLoader;

class RoomsLoader {
	
	public function __construct() {
		
	}
	public function start() {
		
		OstrovokRoomsLoader::load();
		//AcaseRoomsLoader::load();
	}
}