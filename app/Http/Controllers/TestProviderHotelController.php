<?php

/*
|--------------------------------------------------------------------------
| Операции с отелями поставщиков
|--------------------------------------------------------------------------
|
| Сопоставление, поиск, фильтрация, найти по id и т.д
|
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\MtlHotel;

use App\Models\MtlProviderHotel;

use App\Models\MtlCity;

use App\Models\MtlCountry;

use App\Http\Controllers\Controller;

use App\Mtl\Repos\HotelRepo;
use App\Mtl\Repos\ProviderHotelRepo;

use Illuminate\Support\Facades\DB;

use App\Mtl\Classes\Log;
use App\Mtl\Classes\LogEntry;

use App\Mtl\Classes\Common;

class TestProviderHotelController extends Controller
{
    /**
     * Show requested and found global hotels
     *
     * @param  int  $id
     * @return Response
     */
	public function __construct()
	{
	    $this->middleware('auth');
		
		$this->log = new Log();
	}
	public function search(Request $request)
	{
        // $hotels = MtlHotel::paginate(10);
//		$countries = MtlCountry::all();
		
		$repo = new ProviderHotelRepo;
		
		$hotels = $repo->search($request);
		
		$total = $repo->totalUnprocessed();
		
		$request->flash();
		
//		dd($hotels->first());
		return view('test_provider_hotels', [
		    'page' => 'Необработанные отели',
		    'hotels' => $hotels,
		    //'countries' => $countries,
		    'total' => $total
		]);
    }
	public function link (Request $request) {
		
		if (!$request->globalHotelId || !$request->providerHotelId) {
			$mess = 'Ошибка! Отели не были связаны';
			\Session::flash('error_message', $mess);
			return array('message' => $mess);
		}
		
		$providerHotel = MtlProviderHotel::find($request->providerHotelId);
		
		if (!$providerHotel) {
			$mess = 'Ошибка! Возможно отель был удален';
			\Session::flash('error_message', $mess);
			return array('message' => $mess);
		}
			
		$providerHotel->global_hotel_id = $request->globalHotelId;
		$providerHotel->save();
		
		$mess = 'Отель поставщика привязан';
		
		\Session::flash('action_result_message', $mess);
		
		// пишем в лог
		
		$params = array(
			'object_id' => $request->globalHotelId,
			'subject_id' => $request->providerHotelId,
			'action_type' => 'link',
			'object_type' => 'hotel',
			'comment' => $mess // сформировать строку с описанием операций
		);
		$this->log->insert(new LogEntry($params));
		
		return array('result' => true, 'message' => $mess);
			
		
	}
	public function show($id, Request $request) {
		
		$request->flash();
		
		$repo = new ProviderHotelRepo;
		
		$globalHotelRepo = new HotelRepo;
		
		$hotel = $repo->getById($id);
		
		// если привязанный показываем его и привяазанный глобальный
		if ($hotel->global_hotel_id) {
			$globalHotel = $globalHotelRepo->getById($hotel->global_hotel_id);
			
			return view(
			'provider_hotel', [
				'providerHotel' => $hotel,
				'globalHotel' => $globalHotel,
			]);
		}
		// иначе собираем остальную информацию - найденные глобальные, похожие, различия полей....
		
		// TODO: костыль - получаем отель пост. через модель (иначе сломается подстветка полей)
		$hotel = $repo->_getById($id);
		
		$hotels = $globalHotelRepo->search($request)->paginate(25); // если больше 10 на клиенте отобразятся только маркеры
		
		// преобразование json
		// TODO: преобразование xml для acase
		if ($hotel->raw_data) 
		{	
			$hotel->raw_data = $this->convRawData($hotel->provider_data_type, $hotel->raw_data);
		}
		
		// найденные отели
		
		$similarHotels = $this->getSimilarHotels($id);
		
		if (count($hotels) > 0) {
			
			$hotels = $this->propsDiff($hotel, $hotels);
			//dd($hotels);

		}
		
		if (count($similarHotels) > 0)
			$similarHotels = $this->propsDiff($hotel, $similarHotels);
		
//		dd($hotels);
//		dd($similarHotels);
		return view(
			'provider_hotel', [
				'providerHotel' => $hotel,
				'hotels' => $hotels,
				'similarHotels' => $similarHotels,
			]);
	}
	public function getSimilarHotels($id) {
		
		// а страна в выводе тоже понадобится
		return DB::table('mtl_link')
			->join('mtl_hotel', 'mtl_hotel.id', '=', 'mtl_link.global_id')
			->leftJoin('mtl_link_data', 'mtl_link.data_id', '=', 'mtl_link_data.id')
			->leftJoin('mtl_city', 'mtl_hotel.city_id', '=', 'mtl_city.id')
			->leftJoin('mtl_country', 'mtl_city.country_id', '=', 'mtl_country.id')
			->where('mtl_link.ref_id', '=', $id)
			->whereNull('mtl_hotel.deleted_at')
			->get([
				'mtl_link.id as link_id',
				'mtl_link_data.match_level',
				'mtl_hotel.*',
				'mtl_country.name_ru as country_name',
				'mtl_city.name_ru as city_name'
			]);
	}
	private function convRawData ($type, $data) {
		
		switch ($type) {
			case 'json': return json_encode(json_decode($data), JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
			break;
			case 'xml': return tidy_repair_string($data, ['input-xml'=> 1, 'indent' => 8, 'wrap' => 1]);
			break;
			default: 'Нет данных';
		}
	}
	private function propsDiff ($hotel, $hotels) {
		
		$hotels->each(function ($item) use ($hotel){
			
			$item->hls = Common::propsDiff($item, $hotel);
		});
		return $hotels;
	}
}