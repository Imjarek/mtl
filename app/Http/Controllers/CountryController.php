<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\MtlCountry;

use App\Models\MtlProviderCountry;

class CountryController extends Controller
{
	public function __construct()
	{
		 $this->middleware('auth');
	}
	public function search(Request $request) {
		
		$request->flash();
		
		$countries = MtlCountry::where('name_ru', 'like', "%{$request->get('country_name')}%");
		
		// если требуется сортировка
		if ($request->sorting && $request->sorting_param)
			$countries->orderBy($request->sorting_param, $request->sorting);
			
		$countries = $countries->paginate(25);
		
		return view('countries', ['page' => 'Страны', 'countries' => $countries]);
	}
	public function show($id) {
		
		$country = MtlCountry::find($id);
		
		$provider_countries = MtlProviderCountry::where('global_country_id', $id)->paginate();
		
		//dd($country);
		return view('country', ['country' => $country, 'provider_countries' => $provider_countries]);
	}
}
