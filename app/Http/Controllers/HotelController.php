<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\MtlHotel;

use App\Models\MtlProviderHotel;

use App\Models\MtlCity;

use App\Models\MtlCountry;

use App\Models\MtlProviderCity;

use App\Models\MtlLink;

use App\Http\Controllers\Controller;

use App\Mtl\Repos\HotelRepo;
use App\Mtl\Repos\ProviderHotelRepo;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Mtl\Classes\Log;
use App\Mtl\Classes\LogEntry;

use App\Mtl\Classes\Common;


class HotelController extends Controller
{
	/**
	 * Show requested and found global hotels
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function __construct(HotelRepo $hotelRepo, ProviderHotelRepo $providerHotelRepo) {
			
		$this->middleware('auth');
		
		$this->hotelRepo = $hotelRepo;
		
		$this->providerHotelRepo = $providerHotelRepo;
		
		$this->log = new Log();
		
	}
	public function search(Request $request) {
		
		$repo = new HotelRepo;
		
		$total = $repo->total();
		
		$hotels = $repo->search($request)->paginate(25);
		
		$request->flash();
		
		$selected = $this->getSelectedHotels();
		
		// для каждого отеля проверяем его наличие в выбранных
		// и есль есть ставим флаг is_selected
		
		$checkedCount = $selected->count();
		if ($checkedCount)
			
			$selected->each(function ($selectedItem) use ($hotels) {
				$found = $hotels->where('hotel_id', $selectedItem)->first();
				if ($found)
					$found->setChecked(true);
			});
		
		// запрашиваем связанные отели поставщиков (сделать это через метод модели не получается изза джойнов в вышележащем запросе)
		$hotels->each(function ($hotel){
			$hotel->providers = $this->getProviderInfo($hotel);
		});
		
		return view('hotels', [
		    'total' => $total,
		    'hotels' => $hotels,
		    'page' => 'Глобальные отели',
		    'checked_count' => $checkedCount
		]);
    }
	private function getProviderInfo($hotel) {
		$res = DB::select("select case when provider_id = 1 then 'Островок'
									when provider_id = 2 then 'Академсервис' 
									when provider_id = 3 then 'Броневик'
									when provider_id = 4 then 'Амадеус' 
									when provider_id = 5 then 'Beds Online'
									when provider_id = 6 then 'HotelBook' 
										end provider,
									id 
				from mtl_provider_hotel ph where global_hotel_id = {$hotel->hotel_id}")
			;
		return $res;
	}
	public function show ($id) {
		
		$repo = new HotelRepo;
		
		$hotel = $repo->getById($id);
		$providerHotelRepo = new ProviderHotelRepo;
		$providerHotels = $providerHotelRepo->getByGlobalHotelId($id);
		
// определяем объединения (вынести в сервис)
		
		$unions = $repo->getUnions($id);
		
		// определяем несовпадающие поля
		
		$providerHotels = $this->propsDiff($hotel, $providerHotels);
		
		return view('hotel', [
			'hotel' => $hotel,
			'providerHotels' => $providerHotels,
			'unions' => $unions
		]);
	}
	
//	public function getUnions ($id) {
//		
//		// запрашиваем записи из лога с типом union и присоединяем данные отелей по object_id
//		// при этом нужно отфильтровать отмененные union_undo
//		return DB::table('mtl_log')
//			->join('mtl_hotel', 'mtl_hotel.id', '=', 'mtl_log.subject_id')
//			->select('mtl_log.id', 'mtl_hotel.id as hotel_id', 'mtl_hotel.name_ru')
//			->where('mtl_log.action_type', '=', 'union')
//			->where('mtl_log.object_id', '=', $id)
//			->get();
//	}
	public function create (Request $request) {
		
		// создаем новый отель на основе данных отеля поставщика
		// удаляем из списка похожих
		// проставляем связь
		
		$result = array('result' => false, 'message' => 'Не найден отель поставщика');
		
		$providerHotelRepo = new ProviderHotelRepo;
		
		$providerHotel = $providerHotelRepo->getById($request->providerHotelId);
		//$providerHotel = MtlProviderHotel::find($request->providerHotelId);
		
		if (!$providerHotel)
			return $result;
		
		$hotel = new MtlHotel;
		
		// маппим город
		// TODO: и страну
		$city_id = MtlProviderCity::where('provider_city_id', $providerHotel->provider_city_id)
			->where('provider_id', $providerHotel->provider_id)
			->first()
			->global_city_id;
		
		$hotel->city_id = $city_id;
		
		$hotel->name_ru = $providerHotel->hotel_name;
		$hotel->latitude = $providerHotel->latitude;
		$hotel->longitude = $providerHotel->longitude;
		$hotel->address = $providerHotel->address;
		$hotel->description = $providerHotel->description;
		$hotel->phone = $providerHotel->phone;
		$hotel->photo = $providerHotel->photo;
		$hotel->email = $providerHotel->email;
		$hotel->url = $providerHotel->url;
		
		$hotel->save();
		
		// линкуем отели
		$providerHotel = MtlProviderHotel::find($providerHotel->hotel_id);
		$providerHotel->global_hotel_id = $hotel->id;
		$providerHotel->save();
		    
		$message = "Был создан новый отель <a href=\"/hotel/{$hotel->id}\"># {$hotel->id} {$hotel->name_ru}</a>";
		
		if ($request->linkId) {
			$link = MtlLink::find($request->linkId);
			$link->destroy; // TODO: нужно не удалять, а помечать как удаленный и сохранять информацию о том кто и когда
		}
		
		\Session::flash('action_result_message', $message);
		
		// пишем в лог
		$params = array(
			'object_id' => $hotel->id,
			'subject_id' => null,
			'action_type' => 'create',
			'object_type' => 'hotel',
			'comment' => $message
		);
		$this->log->insert(new LogEntry($params));
		
		return array('message' => $message, 'result' => true, 'id' => $hotel->id);
	}
	public function getSelectedHotels() {
		
		$uid = Auth::user()->id;
			
		return DB::table('mtl_buffer')->where(['uid' => $uid, 'item_type' => 'hotel'])->pluck('item_id');
		
	}
	public function edit(Request $request) {
		
		$edit = $this->hotelRepo->edit($request);
		
		// пишем в лог
		
		if (isset($edit['result'])) {
			$params = array(
				'object_id' => $request->get('hotelId'),
				'subject_id' => null,
				'action_type' => 'edit',
				'object_type' => 'hotel',
				'comment' => $edit['message']
			);
			
			$this->log->insert(new LogEntry($params));
		}
		
		return $edit;
	}
	
	public function delete(Request $request) {
		
		$id = $request->get('id');
		
		if (!isset($id))
			return array('message' => 'Неверный параметр');
		
		$result = $this->hotelRepo->delete($id);
		
		if ($result['result']) {
			$params = array(
				'object_id' => $id,
				'subject_id' => null,
				'action_type' => 'delete',
				'object_type' => 'hotel',
				'comment' => $result['message']
			);
			
			$this->log->insert(new LogEntry($params));
		}
		
		\Session::flash('action_result_message', $result['message']);
		
		return $result;
	}
	
	private function propsDiff ($hotel, $providerHotels) {
		
		$providerHotels->each(function ($providerHotel) use ($hotel){
			
			$providerHotel->hls = Common::propsDiff($providerHotel, $hotel);
		});
		
		//$providerHotels;
		return $providerHotels;
	}
}