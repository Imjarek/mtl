<?php

namespace App\Http\Controllers;

use App\Mtl\Utils\Robot;
use Illuminate\Http\Request;

use App\Mtl\Classes\RobotLog;


class RobotLogController extends Controller
{	
	public function __construct(RobotLog $robotLog) {
		
		$this->log = $robotLog;
		$this->middleware('auth');
		// $this->middleware('role'); где во фреймоврке нужно зарегистрировать чтоб работало?
		
	}
	public function show () {
		
		if (!\Auth::user()->hasRole('admin'))
			return ('Отсутствуют права на просмотр страницы');
		
		$entries = $this->log->search();
		
		return view('robot_log', ['entries' => $entries]);
	}
}
