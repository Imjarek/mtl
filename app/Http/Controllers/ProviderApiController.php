<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mtl\Provider\Bronevik\BronevikRequest;
use \App\Mtl\Provider\Ostrovok\OstrovokRequest;
use \App\Mtl\Provider\Bol\BolRequest;
use App\Mtl\Provider\HotelBook\HotelBookRequest;
use App\Mtl\Provider\Acase\AcaseRequest;

use Carbon\Carbon;

use App\Models\MtlProviderHotel;

/*
 * 
 * Класс отправляет поставщикам запросы тарифов
 * TODO: вынести код заполняющий параметры объекта ProviderRequest в конструктор
 */
class ProviderApiController extends Controller
{	
	private $response;
	private $format;
	public function __construct(Request $request)
	{
		 $this->middleware('auth');
		 $this->request = $request;
	}
	
	public function requestRates() {
		
		$provider = $this->request->get('provider');
		
		switch ($provider) {
			case 'ostrovok': $this->ostrovok();
			break;
			case 'hotelbook': $this->hotelbook();
			break;
			case 'acase': $this->acase();
			break;
			case 'bol': $this->bol();
			break;
			case 'bvk': $this->bronevik();
		}
		return $this->requestRatesForm();
	}
	public function requestRatesForm() {
		
		$this->request->flash();
		return view('forms.request_rates_form', ['format' => $this->format, 'response' => $this->response]);
	}
    public function ostrovok () {
		
		$this->format = 'json';
		$req = new OstrovokRequest();

		$req->hotelId = $this->request->get('hotel_id');
		
		$req->checkIn = $this->request->get('check_in');
		$req->checkOut = $this->request->get('check_out');
		
		$req->adults = $this->request->get('adults') ? $this->request->get('adults') : 1;
		$req->children = $this->request->get('children') ? $this->request->get('children') : null;
		
//		$req->checkIn = "2017-08-07";
//		$req->checkOut = "2017-08-07";
		
		$this->response = $req->load();
		
	}
	public function hotelbook() {
		
		$this->format = 'xml';
		
		$req = new HotelBookRequest;
		
		$req->hotelId = $this->request->get('hotel_id');
		
		$hotel = MtlProviderHotel::where(['provider_id' => 6, 'provider_hotel_id' => $req->hotelId])
			->first();
		
		if (!$hotel)
			return view('forms.request_rates_form', ['error_message' => 'Отель не найден в системе']);
		
		$req->cityId = $hotel->provider_city_id;
        $req->checkIn = $this->request->get('check_in');
		$req->checkOut= $this->request->get('check_out');
		$req->adults = $this->request->get('adults') ? $this->request->get('adults') : 1;
		$req->children = $this->request->get('children') ? $this->request->get('children') : null;
		$req->rooms = $this->request->get('rooms') ? $this->request->get('rooms') : 1;
		
		$checkIn = Carbon::createFromFormat('Y-m-d', $req->checkIn);
		$checkOut = Carbon::createFromFormat('Y-m-d', $req->checkOut);
		
		$req->duration = $checkOut->diffInDays($checkIn);

		$this->response = $req->load();
	}
	public function acase () {
		
		
		$req = new AcaseRequest;
		
		$req->hotelId = $this->request->get('hotel_id');
		$req->checkIn = $this->request->get('check_in');
		$req->checkOut = $this->request->get('check_out');
		
		$res = $req->load();
		
		echo $res;
		
	}
	public function bol () {
		
		$req = new BolRequest;
		
		$req->hotelId = $this->request->get('hotel_id');
		$req->checkIn = $this->request->get('check_in');
		$req->checkOut = $this->request->get('check_out');
		
		$req->adults = $this->request->get('adults') ? $this->request->get('adults') : 1;
		$req->children = $this->request->get('children') ? $this->request->get('children') : 0;
		$req->rooms = $this->request->get('rooms') ? $this->request->get('rooms') : 1;
		
		$this->response = $req->load();
	}
	public function bronevik () {
		
		$req = new BronevikRequest;
		
		$req->hotelId = $this->request->get('hotel_id');
		$req->checkIn = $this->request->get('check_in');
		$req->checkOut = $this->request->get('check_out');
		
		$this->response = $req->load();
	}
}
