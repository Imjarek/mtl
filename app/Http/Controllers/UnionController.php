<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\MtlHotel;

use App\Mtl\Repos\HotelRepo;
use App\Mtl\Repos\ProviderHotelRepo;

use App\Mtl\Classes\Buffer;

use App\Mtl\Classes\Log;
use App\Mtl\Classes\LogEntry;

use App\Mtl\Classes\Common;

class UnionController extends Controller
{	
	public function __construct(Buffer $buffer, ProviderHotelRepo $providerHotelRepo, HotelRepo $hotelRepo)
	{
		$this->middleware('auth');
		
		$this->providerHotelRepo = $providerHotelRepo;
		$this->hotelRepo = $hotelRepo;
		$this->buffer = $buffer;
		
		$this->log = new Log();
	}
	public function show() {
	    
		$uid = Auth::user()->id;
		
		// основным является тот который был добавлен раньше
		
		$hotels =  MtlHotel::join('mtl_buffer', 'mtl_buffer.item_id', '=', 'id')
			->select()
			->where(['uid' => $uid,
				'mtl_buffer.item_type' => 'hotel'])
			->orderBy('mtl_hotel.created_at', 'asc')
			->get();
		
		if (!count($hotels)) {
			return redirect('hotels')
				->withErrors(array('message' => 'Произошла ошибка. Не найден отель для объединения'));
		}
		$hotel1 = $hotels->first();
		
		$hotel2 = $hotels->last();
			
		$hotel1providerHotels = $this->providerHotelRepo->getByGlobalHotelId($hotel1->id);
		$hotel2providerHotels = $this->providerHotelRepo->getByGlobalHotelId($hotel2->id);
		
		//if ($hotel2->created_at->diffInHours($hotel1->created_at) < 1) {
		if ($hotel2->created_at == $hotel1->created_at) {
			
			// определяем основной отель исходя из большего кол-ва привязанных
			if (count($hotel2providerHotels) > count($hotel1providerHotels)) {
				$tmpHotel = $hotel2;
				$hotel2 = $hotel1;
				$hotel1 = $tmpHotel;
				$tmpPrHotels = $hotel2providerHotels;
				$hotel2providerHotels = $hotel1providerHotels;
				$hotel1providerHotels = $tmpPrHotels;
			}
		}
		
		$unions = $this->hotelRepo->getUnions($hotel1->id);
		
		$hls = Common::propsDiff($hotel1, $hotel2);
		
	return	view('union', [
			'page' => 'Объединение отелей',
			'hotel1' => $hotel1,
			'hotel2' => $hotel2,
			'unions' => $unions,
			'hotel1providerHotels' => $hotel1providerHotels,
			'hotel2providerHotels' => $hotel2providerHotels,
			'hls' => $hls
		]);
	}
	public function checkUnion($srcId, $dstId) {
		
		return DB::table('mtl_union')
			->select()
			->where([
				'src_id' => $srcId,
				'dst_id' => $dstId,
				'item_type' => 'hotel'
			])->first();
	}
	public function confirmUnion (Request $request) {
		
		$result = array('result' => false, 'message' => 'Не удалось выполнить объединение');
		
		$mainHotelId = $request->input('mainHotelId');
		
		$supplementaryHotelId = $request->input('supplementaryHotelId');
		
		$mainHotel = MtlHotel::find($mainHotelId);
		$supplementaryHotel = MtlHotel::find($supplementaryHotelId);
		
		// перед продолжением
		// проверим переданы ли начальный и целевой отели
		if (!$mainHotel || !$supplementaryHotel)
			return $result;
		
		// и не было ли уже произведено их объединение пока мы думали
		if ($this->checkUnion($mainHotel->id, $supplementaryHotel->id))
			 return array('result' => false, 'message' => 'Отели уже объединены');
		
		
		$fields = $request->input('fields');
		
		if (count($fields)) {
			
			$idx = array_search('coords', $fields) !== false;
			
			if ($idx) {
				unset($fields[$idx]);
				array_push($fields, 'latitude', 'longitude');
			}
			// от вспомогательного отеля будут взяты только неотмеченные поля
			// проходим по списку полей
			// делаем присвоение для соответствующих модели и форме полей

			foreach($fields as $field) {

				if ($supplementaryHotel->hasAttribute($field))
					$mainHotel->$field = $supplementaryHotel->$field;
			}
			
			// копирование привязок к геобъектам
			// TODO: нужно переделать чтобы оно работало через ID
			if(array_search('city', $fields) !== false) 
				$this->hotelRepo->changeCity($mainHotel, $supplementaryHotel->city->name_ru);
			
			if(array_search('country', $fields) !== false) 
				$this->hotelRepo->changeCountry($mainHotel, $supplementaryHotel->country->name_ru);
			
			// сохранение
			$mainHotel->save();
		}
		
		
		// Все привязанные к вспом. отелю отели поставщиков привязать к основному отелю
		$this->relinkProviderHotels($mainHotel->id, $supplementaryHotel->id);
		
		$supplementaryHotel->delete(); //soft delete
		// \Session::flash('warning_message', 'Запрошенные данные не найдены');
		
		$this->buffer->reset();
		
		$message = "Объединены отели № {$mainHotel->id} {$mainHotel->name_ru} и № {$supplementaryHotel->id} {$supplementaryHotel->name_ru}";
		
		$result = array('result' => true, 'message' => $message);
		
		\Session::flash('action_result_message', $result['message']);
		// запрос на объединение отелей
		
		DB::table('mtl_union')->insert([
			'dst_id' => $supplementaryHotel->id,
			'src_id' =>  $mainHotel->id,
			'item_type' => 'hotel'
		]);
		
		// пишем в лог
		
		$params = array(
			'object_id' => $mainHotel->id,
			'subject_id' => $supplementaryHotel->id,
			'action_type' => 'union',
			'object_type' => 'hotel',
			'comment' => $message // сформировать строку с описанием операций
		);
		$this->log->insert(new LogEntry($params));
		
		return $result;
		
	}
	private function relinkProviderHotels ($id1, $id2) {
		
		return $this->providerHotelRepo->relink($id1, $id2);
	}
	public function getUnion($id) {
		
		return DB::table('mtl_union')
			->select()
			->where('id', $id)
			->first();
	}
	public function deleteUnion($id) {
		
		return DB::table('mtl_union')
			->where('id', $id)
			->delete();
	}
	public function undoUnion(Request $request) {
		/*
		 * Отмена объединения
		 * Восстановление удаленного отеля
		 * Добавление записи в лог
		 */
		$id = $request->input('logId');
		
		$union = $this->getUnion($id);
		
		if (!$union)
			return array('message' => 'Отели не объединены');
		
		// восстанавливаем отель
		$supplementaryHotel = MtlHotel::withTrashed()->find($union->dst_id);
		
		if ($supplementaryHotel)
			$supplementaryHotel->restore();
		
		$message = "Отменено объединение отеля № {$supplementaryHotel->id} {$supplementaryHotel->name_ru}";
		
		$result = array('result' => true, 'message' => $message, 'id' => $supplementaryHotel->id);
		
		// пишем в лог
		$params = array(
			'object_id' => $union->src_id,
			'subject_id' => $union->dst_id,
			'action_type' => 'union_undo',
			'object_type' => 'hotel',
			'comment' => $message // сформировать строку с описанием операций
		);
		
		$this->log->insert(new LogEntry($params));
		
		\Session::flash('action_result_message', $result['message']);
		
		$this->deleteUnion($id);
		
		return $result;
		
	}
}
