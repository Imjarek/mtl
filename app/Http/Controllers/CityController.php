<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\MtlCity;

use App\Models\MtlProviderCity;
use App\Models\MtlProviderCountry;

use App\Models\MtlProvider;


class CityController extends Controller
{
	public function __construct()
	{
		 $this->middleware('auth');
	}
	public function search(Request $request) {
		
		$countryId = $request->country_id;
		
		$countryName = $request->country_name;
		
		$request->flash();
		
		$total = MtlCity::count();
		
		$cities = MtlCity::where('mtl_city.name_ru', 'like', "%{$request->input('city_name')}%")
			->join('mtl_country', 'mtl_country.id', 'mtl_city.country_id')->select('mtl_city.*');
		
		if ($countryId)
			
			$cities->whereHas('country', function ($q) use ($countryId) {
				$q->where('id', $countryId);
			});
		else 
			if ($countryName)
				$cities->whereHas('country', function ($q) use ($countryName) {
				$q->where('name_ru', $countryName);
			});
		
		// если требуется сортировка
		if ($request->sorting && $request->sorting_param) {
			
			if ($request->sorting_param != 'country_name')
			
				$cities->orderBy($request->sorting_param, $request->sorting);
			else
				$cities->orderBy('mtl_country.name_ru', $request->sorting);
		}
		
		$cities = $cities->paginate(25);
		
		return view('cities', ['page' => 'Справочник глобальных городов', 'cities' => $cities, 'total' => $total]);
	}
	public function show($id) {
		$city = MtlCity::find($id);
	
		$provider_cities = MtlProviderCity::where('global_city_id', $id)->paginate();
		
		$providers = MtlProvider::all();
		
		//dd($provider_cities);
		return view('city', ['city' => $city, 'provider_cities' => $provider_cities, 'providers' => $providers]);
	}
	public function edit(Request $request) {
		
		// если с клиента пришел ID то пишем его в любом случае (приоритет перед названием)
		$cityId = $request->get('cityId');
		
		$data = $request->get('data');
		
		if (!isset($data))
			return array('message' => 'Неверный параметр');
		
		$mess = 'Изменения сохранены';
		$warn = '';
		
		foreach($data as $key=>$item) {
			// если пришел запрос на новый город поставщика на основе текущего гл. города
			
			// если пользователь стер город
			if (isset($item['unlink'])) {
				$this->unlinkProviderCity($cityId, $key);
				$mess .= "<br>Отвязан город поставщика";
				continue;
			}
			
			if (isset($item['newProviderCity'])) {
				if (!$this->linkProviderCity($cityId, $item['newProviderCity'], $key)) {
					$mess .= "<br>Указанный город поставщика \"{$item['newProviderCity']}\" не найден ";;
					continue;
				}
			}
				// добавляем город
			if (isset($item['providerCityId'])) {
				$providerCity = MtlProviderCity::where('provider_id', $key)
					->where('provider_city_id', $item['providerCityId'])
					->first();
			}
			elseif (isset($item['providerCityName'])) {
					
				$providerCity = MtlProviderCity::where('provider_id', $key)
				->where('name_ru', $item['providerCityName'])
				->first();
			}
			else 
				continue;
			
			if ($providerCity) {
				// отвзяваем старый
				$oldCity = MtlProviderCity::where('provider_id', $providerCity->provider_id)->where('global_city_id', $cityId);
				
				if ($oldCity) 
					$oldCity->update(['global_city_id' => null]);

				// привязываем новый
				$providerCity->global_city_id = $cityId;
				$providerCity->save();
				$mess = 'Город изменен';
			}
			else {
				$c = isset($item['providerCityId']) ? $item['providerCityId'] : $item['providerCityName'];
				$warn .= "<br>Указанный город поставщика {$c} не найден ";
				continue;
			}
		}

		$mess .= $warn;
		
		\Session::flash('action_result_message', $mess);
		
		return array('result' => true, 'message' => $mess);
	}
	
	private function linkProviderCity($cityId, $cityName, $providerId) {
		// находим город по названию и связываем его с нашим глобальным городом
		$pCity = MtlProviderCity::where(['provider_id' => $providerId, 'name_ru' => $cityName])->first();
		
		if (!$pCity)
			return;
		// создаем связь
		$pCity->global_city_id = $cityId;
		$pCity->save();
		
		return true;
	}
	
	private function unlinkProviderCity($cityId, $providerId) {
		
		$providerCity = MtlProviderCity::where(['provider_id' => $providerId, 'global_city_id' => $cityId])->first();
		
		if (!$providerCity)
			return;
		
		$providerCity->global_city_id = null;
		$providerCity->save();
		
		return true;
	}
}
