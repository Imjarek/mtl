<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;

use App\Mtl\Classes\Buffer;

class BufferController extends Controller {
	
	public function __construct(Buffer $buffer)
	{
		$this->middleware('auth');
		
		$this->buffer = $buffer;
		
	}
	public function add ($itemId, $state) {
	    
		return $this->buffer->add($itemId, $state);
		
	}
	public function reset() {
		
		// функционал работы с "избранным" вынесен в Сервис BufferService
		$this->buffer->reset();
		
//		$uid = Auth::user()->id;
//		
//		DB::table('mtl_buffer')->where([
//			     'uid' => $uid,
//			     'item_type' => 'hotel'
//			 ])->delete();
	}
}
