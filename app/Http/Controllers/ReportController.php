<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mtl\Services\Reporting;

use App\Models\MtlCountry;

use App\Models\MtlCity;

use App\Mtl\Classes\LoadHotelsReport_v4 as Report;

use DB;

use Excel;

use App\Mtl\Classes\Common;
/*
 * @author Sergey Cherednichenko 2017
 * @email sergey.i.che@gmail.com
 * @description Класс получает запрос на формирование отчета и
 * передает параметры методу Report->fillIn() который наполняет отчет данными
 * после чего отчет передается в рендер для генерации xls, csv или html
 */
class ReportController extends Controller {
	
	private $path;
	
	private $report;
	
	public function __construct() {
		
		$this->middleware('auth');
		$this->report = new Report;
		$this->path = '/files/reports/';

	}
	public function configPage () {
		
		$managers = DB::table('mtl_manager')->select(['id', 'fio'])->orderBy('fio', 'ASC')->get();
		
		return view('reports', ['page'=> 'Отчеты', 'managers' => $managers]);
	}
	public function request (Request $request) {
		
		ini_set("memory_limit", "1024M");
		ini_set('max_execution_time', 600);
	
		if ($request->city) {
			$city = MtlCity::where('name_ru', '=', $request->city)->first();
			$this->report->cityId =	$city ? $city->id : null;
		}
		
		if ($request->country) {
			$country = MtlCountry::where('name_ru', '=', $request->country)->first();
			$this->report->countryId = $country ? $country->id : null;
		}
		
		if ($request->manager_id)
			$this->report->managerId = $request->manager_id;
		
		$this->report->fillIn();
		
		$this->fileName = 'Отчет_' . date('d-m-Y-h-m-s') . '_' . substr(uniqid(), 0, 8);
		
		if ($request->format == 'xlsx') {
			
			if (!$request->manager_id) {
				
				$this->createXlsArchive($request->split);

				return array(
					'result' => true,
					'filename' => $this->fileName . '.zip'
				);
			}
			else
				$this->createXls($this->report->managers[0], $this->fileName);
		}
		elseif ($request->format == 'csv') {
			
			$this->createCsvForAll($this->report->managers);
			
			return array(
					'result' => true,
					'filename' => $this->fileName . '.csv'
				);
		}
		else 
			return array(
					'message' => 'Не указан формат отчета'
			);
		
		return array(
			'result' => true,
			'filename' => $this->fileName . '.' . $request->format
		);
	}
	public function download($fileName) {
		
		$headers = $this->headers($fileName);
		$ext = substr($fileName, strpos($fileName, ".") + 1);
		
		if ($ext === 'xlsx')
			$headers['Content-Type'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
		elseif ($ext === 'zip')
			$headers['Content-Type'] = "application/octet-stream";
		else
			$headers['Content-Type'] = 'text/html; charset=utf-8';
		
		$path = public_path() . $this->path . $fileName;
		
		return response()->download($path, $fileName, $headers);
	}
	private function createCsv() {
		
	}
	private function createXls($data, $fileName, $folder = '') {
		
		$path = public_path(). $this->path;
		
		if ($folder)
			$path .= $folder;
		
		Excel::create($fileName, function($excel) use ($data, $path) {
			
			$doc = $excel->sheet('Лист 1', function($sheet) use ($data) {
				
				$sheet->loadView('report.import_v4', ['url' => $this->report->url, 'manager' => $data]);
			});
			
			$doc->store('xlsx', $path);
		});
	}
	private function createXlsForAll($data, $fileName) {
		
		$path = public_path(). $this->path . "/" . $fileName;
		
		Excel::create($fileName, function($excel) use ($data, $path) {
			
			$doc = $excel->sheet('Лист 1', function($sheet) use ($data) {
				
				$sheet->loadView('report.import_v4_all', ['url' => $this->report->url, 'managers' => $data]);
			});
			
			$doc->store('xlsx', $path);
		});
	}
	private function createXlsArchive($split = null) {
		
		$basePath = '/files/reports';
		$path = public_path().$basePath;	
		$path .= '/'. $this->fileName;
		
		// если split то для каждого менеджера создаем отдельный файл
		if ($split)
			$this->report->managers->each(function ($item, $key){

				$fileName = preg_replace('/\s+/', '_', $item->fio);
				$this->createXls($item, $fileName, $this->fileName);
			});
		else 
			$this->createXlsForAll($this->report->managers, $this->fileName);
		
		$dst = $path . '.zip';
		
		Common::Zip($path, $dst); 
	}
	private function createCsvForAll($data) {
		
		$arr = array();
		foreach ($data as $manager) {
			foreach($manager->hotels as $hotel) {
				array_push($arr, (array)$hotel);
			}
		}
		$path = public_path(). $this->path;

		Excel::create($this->fileName, function($excel) use ($arr) {
			
			$excel->sheet('one', function ($sheet) use ($arr) {

                        $sheet->fromArray($arr);
			});

		})->store('csv', $path);
	}
	private function createHtml() {
		
		$report = $this->header();
		
		$report .= view('report.import_v4', ['report' => $this->report]);
		
		$report .= $this->footer();
		
		file_put_contents(public_path().'/files/reports/'. $this->fileName . '.html', $report);
	}
	private function headers($fileName) {
		
		return array(
			'Content-Disposition' => 'attachment',
			'filename' => $fileName
		);
	}
	private function header() {
		return '<html lang="ru">
					<head>
						<meta charset="utf-8">
						<meta http-equiv="X-UA-Compatible" content="IE=edge">
						<meta name="viewport" content="width=device-width, initial-scale=1">
						<meta name="description" content="">
						<meta name="author" content="">
						<meta name="csrf-token" content="UhyCw0jensWuhwrzu0DsXqlJzZCAQ24rqd553m0J">

						<title>MTL</title>
					<body>';
	}
	private function footer() {
		return '</body></html>';
	}
}