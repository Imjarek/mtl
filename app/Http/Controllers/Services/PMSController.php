<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\MtlPMS;

class PMSController extends Controller
{
	public function __construct()
	{
		 $this->middleware('auth');
	}
	public function get() {
		
		$res = MtlPMS::select('id as value', 'name_ru as text')
			->get();
		
		$res->prepend(['value' => 0, 'text' => 'Не выбрано']);
		
		return $res->toJson();
	}
	
}
