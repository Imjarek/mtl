<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\MtlProviderCity;

/*
 * Контроллер не тестировался
 * 
 */
class ProviderCityController extends Controller
{
	public function __construct()
	{
		 $this->middleware('auth');
	}
	public function search(Request $request) {
		
		$cities = MtlProviderCity::where( 
			'name_ru', 'like', "{$request->get('q')}%"
		)	->distinct()
			->get(['name_ru as label']);
		
		return array('query' => $request->get('country'), 'suggestions' => $cities);
	}
	public function show($id) {
		
		$city = MtlCountry::find($id);
	
		$provider_cities = MtlProviderCity::where('global_city_id', $id)->paginate();
		
		//dd($provider_cities);
		return view('city', ['city' => $city, 'provider_cities' => $provider_cities]);
	}
}
