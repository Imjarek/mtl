<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Mtl\Repos\HotelRepo;

// use App\Models\MtlProviderCountry;

/*
 * Контроллер для предоставление одноколоночного списка отелей
 * 
 */
class HotelController extends Controller
{
	public function __construct()
	{
		 $this->middleware('auth');
	}
	public function search(Request $request) {
		
		$hotelRepo = new HotelRepo;
		
		$hotels = $hotelRepo->searchByName($request->q)->get(['name_ru as label']);
		
		return array('query' => $request->get('country'), 'suggestions' => $hotels);
	}
	public function show($id) {
		
		$city = MtlHotel::find($id);
	
		$provider_cities = MtlProviderCity::where('global_city_id', $id)->paginate();
		
		//dd($provider_cities);
		return view('city', ['city' => $city, 'provider_cities' => $provider_cities]);
	}
}
