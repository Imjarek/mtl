<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\MtlCity;

// use App\Models\MtlProviderCountry;

/*
 * Контроллер не тестировался
 * 
 */
class CityController extends Controller
{
	public function __construct()
	{
		 $this->middleware('auth');
	}
	public function search(Request $request) {
		
		$countries = MtlCity::where(
			'name_ru', 'like', "{$request->get('q')}%"
		)	
			->get(['id', 'name_ru as label']);
		
		return array('query' => $request->get('country'), 'suggestions' => $countries);
	}
	public function show($id) {
		
		$city = MtlCity::find($id);
	
		$provider_cities = MtlProviderCity::where('global_city_id', $id)->paginate();
		
		//dd($provider_cities);
		return view('city', ['city' => $city, 'provider_cities' => $provider_cities]);
	}
}
