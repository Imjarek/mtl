<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Models\MtlHotel;

use App\Models\MtlPMS;

class PMSController extends Controller
{
	public function __construct()
	{
		 $this->middleware('auth');
	}
	public function search(Request $request) {
		
		$pmsystems = MtlPMS::all();
		
		return view('pms', ['page' => 'Справочник PMS', 'pmsystems' => $pmsystems ]);
	}
	public function create (Request $request) {
		
		if (!$request->name) {
			
			\Session::flash('error_message', 'Не указано название PMS');
			return redirect('/pms');
		}
		
		$pms = new MtlPMS;
		
		$pms->name_ru = $request->name;
		
		$pms->save();
		
		\Session::flash('action_result_message', 'Новый PMS добавлен');
		
		return redirect('/pms');
	}
	public function show($id) {
		
		$pms = MtlPMS::find($id);
		
		// Найти отели с этим PMS
		$hotels = MtlHotel::where('pms_id', '=', $id)->get();
		
		return view('pms_info', ['page' => 'Свойства PMS', 'pms' => $pms, 'hotels' => $hotels ]);
	}
	
	public function edit($id, Request $request) {
		
		if (!$request->name) {
			
			\Session::flash('error_message', 'Не указано название PMS');
			return redirect('/pms');
		}
		
		$pms = MtlPMS::findOrFail($id);
		
		$pms->name_ru = $request->name;
		
		$pms->save();
		
		\Session::flash('action_result_message', 'PMS сохранена');
		return redirect('/pms');
	}
}