<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mtl\Classes\MatchLog;

class MatchLogController extends Controller
{
    public function __construct(MatchLog $log) {
		$this->log = $log;
	}
	public function show(Request $request) {
		
		if (!\Auth::user()->hasRole('admin'))
			return 'У Вас нет допуска к этой странице'; // TODO: вынести в middleware
		
		$entries = $this->log->search($request);
		
		return view('match_log', ['entries' => $entries]);
	}
}	
