<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\MtlLink;
use App\Models\MtlHotel;
use App\Models\MtlProviderHotel;

use Illuminate\Support\Facades\DB;

use App\Mtl\Repos\HotelRepo;
use App\Mtl\Repos\ProviderHotelRepo;

use App\Mtl\Classes\Log;
use App\Mtl\Classes\LogEntry;

class LinkController extends Controller
{	
	public function __construct()
	{
		$this->middleware('auth');
		
		$this->log = new Log();
	}
	function get () {
		
		$matches = DB::table('mtl_link')
		    ->select(
			'mtl_link.id',
			'mtl_hotel.id as global_id',
			'mtl_link.ref_id',
			'mtl_link_data.match_level',
			'mtl_provider_hotel.provider_hotel_id',
			'mtl_hotel.name_ru as global_hotel_name',
			'mtl_provider_hotel.name_ru as provider_hotel_name',
			'mtl_provider_hotel.provider_id as code',
			'mtl_provider.name as provider_name'
			)
		    ->join('mtl_link_data', 'mtl_link.data_id', '=', 'mtl_link_data.id')
		    ->join('mtl_hotel', 'mtl_link.global_id', '=', 'mtl_hotel.id')
		    ->join('mtl_provider_hotel', 'mtl_provider_hotel.id', '=', 'mtl_link.ref_id')
		    ->join('mtl_provider', 'mtl_link_data.provider_id', '=', 'mtl_provider.id')
		    ->paginate(25)
		    ;
		
		//dd($matches);
		return view('matches', ['page' => 'Совпадения', 'matches' => $matches]);
	}
	function show ($link_id) {
		$match = DB::table('mtl_link')
			->join('mtl_link_data', 'mtl_link.data_id', '=', 'mtl_link_data.id')
			->join('mtl_hotel', 'mtl_link.global_id', '=', 'mtl_hotel.id')
			->join('mtl_provider_hotel', 'mtl_provider_hotel.id', '=', 'mtl_link.ref_id')
			->join('mtl_provider', 'mtl_link_data.provider_id', '=', 'mtl_provider.id')
			->where('mtl_link.id', $link_id)
			->get([
			    'mtl_link.id as link_id',
			    'mtl_link.global_id as link_global_id',
			    'mtl_link.ref_id as link_ref_id',
			    'mtl_link_data.match_level',
			    'mtl_provider.name as provider_name',
			    'mtl_hotel.*',
			    'mtl_provider_hotel.*'])
			->first()
			;
		
		if (!$match) {
			\Session::flash('warning_message', 'Запрошенные данные не найдены');
			return redirect('/matches');
		}
		
		$repo = new HotelRepo;
		$hotel = $repo->getById($match->link_global_id);
		
		$providerHotelRepo = new ProviderHotelRepo;
		$providerHotel = $providerHotelRepo->getById($match->link_ref_id);
		
		//dd($match);
		
		//dd($hotel->provider_name);
		return view('match', ['page' => 'Совпадение', 'match' => $match, 'hotel' => $hotel, 'providerHotel' => $providerHotel]);
	}
	function link (Request $request) {
		
		// TODO: Нужно добавить проверочные условия для многопользовательской работы (например если ктото уже пересвязал отели)
		
		// если передан id связи то глоб. отель определяем по ней
		
		$link = MtlLink::find($request->linkId);
		
		$hotel = MtlHotel::find($link->global_id);
		
		$providerHotel = MtlProviderHotel::find($request->providerHotelId);
				
		$providerHotel->global_hotel_id = $link->global_id;
		
		$providerHotel->save();
		
		$link->delete();
		
		$message = "Отель поставщика <a href=\"/provider_hotel/{$providerHotel->id}\">{$providerHotel->name_ru}</a> привязан к глобальному отелю <a href=\"/hotel/{$hotel->id}\"> {$hotel->name_ru}</a>";
		
		\Session::flash('action_result_message', $message);
		
		// пишем в лог
		$params = array(
			'object_id' => $hotel->id,
			'subject_id' => $providerHotel->id,
			'action_type' => 'link',
			'object_type' => 'hotel',
			'comment' => $message // сформировать строку с описанием операций
		);
		$this->log->insert(new LogEntry($params));
		
		return array('message' => $message, 'result' => true);
	}
	function unlink (Request $request) {
		
		$ref_id = $request->ref_id;
		
		$providerHotel = MtlProviderHotel::find($ref_id);

		if ($providerHotel) {
			
			// указание на глоб. отель будет сохранено в логе
			$objectId = $providerHotel->global_hotel_id;
			
			$providerHotel->global_hotel_id = null;

			$providerHotel->save();
			
			$success_msg = "Отель поставщика <a href=\"/provider_hotel/{$providerHotel->id}\">$providerHotel->name_ru</a> отвязан от глобального отеля";
			
			\Session::flash('action_result_message', $success_msg);
			
			$params = array(
				'object_id' => $objectId,
				'subject_id' => $providerHotel->id,
				'action_type' => 'unlink',
				'object_type' => 'hotel',
				'comment' => $success_msg // сформировать строку с описанием операций
			);

			$this->log->insert(new LogEntry($params));
		
			return array('result' => true, 'message' => $success_msg);
		}
		
		$error_msg = 'Ошибка. Отель не отвязан';
		
		\Session::flash('error_message', $error_msg);
		
		return array('message' => $error_msg);
	}
}
