<?php

namespace App\Http\Controllers;

use App\Mtl\Utils\Robot;
use Illuminate\Http\Request;


class RobotController extends Controller
{	
	public function __construct(Robot $robot) {
		
		$this->robot = $robot;
	}
   
	public function searchSims(Request $request) {
		
		$id = $request->input('id');
		
		if (!isset($id))
			return array('message' => 'Неверный параметр ID');
		
		$result = $this->robot->searchSims($id);
		
		return $result;
	}
}
