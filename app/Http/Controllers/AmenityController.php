<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Models\MtlHotelAmenity;

class AmenityController extends Controller
{
    public function get() {
	    
	    $amenities = MtlHotelAmenity::paginate(25);
	    
	    return view('amenities', ['page'=> 'Услуги(удобства)', 'amenities' => $amenities]);
    }
}
