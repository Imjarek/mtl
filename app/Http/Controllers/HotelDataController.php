<?php

namespace App\Http\Controllers;

use App\Services\HotelData;

class HotelDataController extends Controller
{
    public function start(LoadHotelData $hotelData)
    {
		
        $hotelData->load();
    }
}