<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

use App\Mtl\Classes\Log;

use App\Models\LogEntry;

use Illuminate\Support\Facades\Auth;

class LogController extends Controller {
	
	protected $dates = ['created_at'];
	
	public function __construct() { // не получается внедрить сервис в контейнер, сдесь должен быть хинт LogInterface - проверить
		
		$this->middleware('auth');
		
		$this->log = new Log;
		
	}
	public function show(Request $request) {
		
		$users = User::get();
		
		// TODO: uid должен определяться в конструкторе, переделать

		$entries = $this->log->show($request);

		return view('log', ['page' => 'Журнал событий', 'entries' => $entries, 'users' => $users]);

	}
    
	public function insert($params, Request $request) {
		
		// TODO: uid должен определяться в конструкторе, убрать залепу
		//$this->log->uid = 2; //Auth::user()->id;
		
		$entry = new LogEntry($params);
		
		$this->log->insert($entry);
	}
}
