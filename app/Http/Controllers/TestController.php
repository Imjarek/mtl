<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\MtlHotel;

use App\Models\MtlCity;

use App\Http\Controllers\Controller;

use App\Mtl\Repos\TestRepo;

use App\Mtl\Classes\Log;
use App\Mtl\Classes\LogEntry;

use App\Models\MtlProviderHotel;

use Illuminate\Support\Facades\DB;

use App\Mtl\Classes\LoadHotelsReport_v3 as Report;

// use App\Mtl\Provider\HotelBook\HotelBookRequest;

use App\Mtl\Provider\HotelBook\HotelBookXMLRequest;

class TestController extends Controller
{
    /**
     * Show requested and found global hotels
     *
     * @param  int  $id
     * @return Response
     */
	
	public function __construct() {
		
		$this->report = new Report;
		
	}
    public function testReport(Request $request)
    {

		
		$this->report->fillIn();
		
		return view('report.import', ['report' => $this->report]);
    }
	public function show ($id) {
		
		$repo = new TestRepo;
		
		$hotel = $repo->getById($id);
		
		$hotelRepo = new HotelRepo;

		
		return view('hotel', ['hotel' => $hotel, 'providerHotels' => $providerHotels]);
	}
	public function testProviders() {
		
		$hotels = \App\Models\MtlHotel::
			select('*')
			->leftJoin('mtl_city as c', 'c.id', '=', 'mtl_hotel.city_id')
			->with(['providers' => function ($q){ $q->select('provider_id as provid');}])
			->paginate(20);
		
		return view('test', ['hotels' => $hotels]);
	}
	public function testMatch ($id) {
		
		$hotels = MtlProviderHotel::whereNull('global_hotel_id')
			->select('mtl_provider_hotel.id', 'name_ru')
			->paginate(300);
		
	return view('test', ['hotels' => $hotels]);
		
		return DB::table('mtl_link')
			->join('mtl_hotel', 'mtl_hotel.id', '=', 'mtl_link.global_id')
			->join('mtl_city', 'mtl_hotel.city_id', '=', 'mtl_city.id')
			->where('mtl_link.ref_id', '=', $id)
			->get(['mtl_link.id as link_id', 'mtl_hotel.*', 'mtl_city.name_ru as city_name']);
		
		$match = MtlProviderHotel::find($id)->matches()->get();
		
		dd($match);
		
	}
	public function testProviderApi() {
		
		$req = new HotelBookXMLRequest;
		
		//$req->xmlMethod = 'hotel_search';
		
		$req->params = $req->params();
		
		$req->params['request'] = '<?xml version="1.0" encoding="utf-8"?>
<HotelSearchRequest>
	<Request hotelId=131626>
</HotelSearchRequest>';
		
		$res = $req->send();
		
		return $res;
	}
}