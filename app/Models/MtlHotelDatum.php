<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlHotelDatum
 */
class MtlHotelDatum extends Model
{
    protected $table = 'mtl_hotel_data';

    public $timestamps = false;

    protected $fillable = [
        'description',
        'address',
        'photo'
    ];

    protected $guarded = [];

        
}