<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * Class MtlProviderCity
 */
class MtlProviderCity extends Model
{
    protected $table = 'mtl_provider_city';

    public $timestamps = true;

    protected $fillable = [
        'provider_city_id',
        'name_en',
        'name_ru',
        'provider_id',
        'provider_country_id',
        'provider_region_id',
        'global_city_id',
        'latitude',
        'longitude',
        'country_code',
        'data_id'
    ];

    protected $guarded = [];

    public function provider() {
	    
	    return $this->belongsTo('App\Models\MtlProvider', 'provider_id', 'id');
    }
}