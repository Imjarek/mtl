<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlCity
 */
class MtlCity extends Model
{
    protected $table = 'mtl_city';

    public $timestamps = false;

    protected $fillable = [
        'name_en',
        'name_ru',
        'latitude',
        'longitude',
        'country_code',
        'country_id'
    ];

    protected $guarded = [];
    
    function country () {
	return $this->belongsTo('App\Models\MtlCountry', 'country_id');
    }
}