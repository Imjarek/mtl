<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlProviderRateRel
 */
class MtlProviderRateRel extends Model
{
    protected $table = 'mtl_provider_rate_rel';

    public $timestamps = true;

    protected $fillable = [
        'provider_hotel_id',
        'rate_id',
        'inactive'
    ];

    protected $guarded = [];

        
}