<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlHotelRoom
 */
class MtlHotelRoom extends Model
{
    protected $table = 'mtl_hotel_room';

    public $timestamps = false;

    protected $fillable = [
        'room_code',
        'name_en',
        'name_ru',
        'description'
    ];

    protected $guarded = [];

        
}