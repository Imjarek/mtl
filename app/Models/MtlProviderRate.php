<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlProviderRate
 */
class MtlProviderRate extends Model
{
    protected $table = 'mtl_provider_rate';

    public $timestamps = false;

    protected $fillable = [
        'provider_rate_id',
        'provider_id',
        'description',
        'rate_code',
        'max_guests_num',
        'meal',
        'room_type_id',
        'available_rooms',
        'price',
        'b2b_recommended_price',
        'currency',
        'rate_group'
    ];

    protected $guarded = [];

        
}