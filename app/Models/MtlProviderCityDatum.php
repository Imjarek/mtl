<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlProviderCityDatum
 */
class MtlProviderCityDatum extends Model
{
    protected $table = 'mtl_provider_city_data';

    public $timestamps = false;

    protected $fillable = [
        'description'
    ];

    protected $guarded = [];

        
}