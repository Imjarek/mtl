<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlProviderHotelRoomRel
 */
class MtlProviderHotelRoomRel extends Model
{
    protected $table = 'mtl_provider_hotel_room_rel';

    public $timestamps = false;

    protected $fillable = [
        'provider_room_id',
        'provider_hotel_id'
    ];

    protected $guarded = [];

        
}