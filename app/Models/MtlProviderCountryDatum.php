<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlProviderCountryDatum
 */
class MtlProviderCountryDatum extends Model
{
    protected $table = 'mtl_provider_country_data';

    public $timestamps = false;

    protected $fillable = [
        'description'
    ];

    protected $guarded = [];

        
}