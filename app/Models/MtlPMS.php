<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlCountry
 */
class MtlPMS extends Model
{
    protected $table = 'mtl_pms';

    public $timestamps = false;

    protected $fillable = [
        'name_ru',
        'name_en',
    ];

    protected $guarded = [];

}