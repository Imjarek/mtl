<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlBuffer
 */
class MtlBuffer extends Model
{
    protected $table = 'mtl_buffer';

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'item_id',
        'item_type'
    ];

    protected $guarded = [];

        
}