<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Role 
 */

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
	protected $table = 'roles';
	
	protected $fillable = [
		'name',
		'display_name',
		'description'
	];
}