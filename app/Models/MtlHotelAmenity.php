<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlHotelAmenity
 */
class MtlHotelAmenity extends Model
{
    protected $table = 'mtl_hotel_amenity';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description'
    ];

    protected $guarded = [];

        
}