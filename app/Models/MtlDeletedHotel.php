<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlDeletedHotel
 */
class MtlDeletedHotel extends Model
{
    protected $table = 'mtl_deleted_hotel';

    public $timestamps = true;

    protected $fillable = [
        'hotel_id',
        'name_ru',
        'name_en',
        'code',
        'city_id',
        'region_id',
        'country_id',
        'latitude',
        'longitude',
        'address',
        'description',
        'phone',
        'photo',
        'formatted_address',
        'google_place_id',
        'data_id'
    ];

    protected $guarded = [];

        
}