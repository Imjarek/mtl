<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlLog
 */
class MtlLog extends Model
{
	protected $table = 'mtl_log';

	public $timestamps = true;

	protected $fillable = [
	    'uid',
	    'object_id',
	    'subject_id',
	    'action_ids',
	    'action_type',
		'object_type',
		'comment'
	];

	protected $guarded = [];
	
	public function setUpdatedAt($value) {
		// Do nothing;
	}
       
}