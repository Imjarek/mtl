<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlCountry
 */
class MtlCountry extends Model
{
    protected $table = 'mtl_country';

    public $timestamps = false;

    protected $fillable = [
        'name_ru',
        'name_en',
        'code'
    ];

    protected $guarded = [];

}