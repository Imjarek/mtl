<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlProviderHotel
 */
class MtlProviderHotel extends Model
{
    protected $table = 'mtl_provider_hotel';

    public $timestamps = true;

    protected $fillable = [
        'name_ru',
        'name_en',
        'provider_id',
        'provider_hotel_id',
        'global_hotel_id',
        'provider_city_id',
        'provider_region_id',
        'provider_country_id',
        'country_code',
        'raw_data',
        'data_id',
        'latitude',
        'longitude',
		'address',
		'formatted_address',
		'google_place_id',
		'description',
		'phone',
		'url',
		'email',
		'photo',
		'not_exists'
    ];

    protected $hidden = ['city', 'country'];


    public function city () {
	    return $this->belongsTo('App\Models\MtlProviderCity', 'provider_city_id', 'provider_city_id')
			->where('mtl_provider_city.provider_id', $this->provider_id)
		;
    }
	public function country () {
	    return $this->city->belongsTo('App\Models\MtlProviderCountry', 'provider_country_id', 'provider_country_id')
			->where('mtl_provider_country.provider_id', $this->provider_id)
		;
    }
    public function matches () {
	    
	    return $this->hasMany('App\Models\MtlLink', 'ref_id', 'id');
    }
	public function provider () {
	    return $this->belongsTo('App\Models\MtlProvider', 'provider_id', 'id');
    }
	protected $appends = array('city_name', 'country_name');

    public function getCityNameAttribute()
    {
        $this->city =  MtlProviderCity::where(['provider_id' => $this->provider_id, 'provider_city_id' => $this->provider_city_id])->first();  
		
		return $this->city ? $this->city->name_ru : null;
		
    }
	public function getCountryNameAttribute()
    {
        if (!$this->city)
			$this->getCityNameAttribute();
		
		$this->country = MtlProviderCountry::where([
			'provider_id' => $this->provider_id,
			'provider_country_id' => $this->city->provider_country_id]
		)->first();  
		
		return $this->country ? $this->country->name_ru : null;
    }
	
}