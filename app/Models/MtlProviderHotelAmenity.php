<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlProviderHotelAmenity
 */
class MtlProviderHotelAmenity extends Model
{
    protected $table = 'mtl_provider_hotel_amenity';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
	'provider_id',
	'provider_hotel_amenity_id',
	'global_amenity_id'
    ];

    protected $guarded = [];

        
}