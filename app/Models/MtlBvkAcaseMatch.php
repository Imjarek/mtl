<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlBvkAcaseMatch
 */
class MtlBvkAcaseMatch extends Model
{
    protected $table = 'mtl_bvk_acase_match';

    public $timestamps = false;

    protected $fillable = [
        'bvk_hotel_id',
        'acase_hotel_id'
    ];

    protected $guarded = [];

        
}