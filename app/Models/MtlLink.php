<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlLink
 */
class MtlLink extends Model
{
    protected $table = 'mtl_link';

    //protected $primaryKey = 'global_id';

	public $timestamps = false;

    protected $fillable = [
		'global_id',
		'link_type',
        'ref_id',
        'data_id'
    ];

    protected $guarded = [];
    
    public function data () {
	    return $this->hasOne('App\Models\MtlLinkDatum', 'id', 'data_id');
    }
}