<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlLinkDatum
 */
class MtlLinkDatum extends Model
{
    protected $table = 'mtl_link_data';

    public $timestamps = true;

    protected $fillable = [
        'provider_id',
        'comment',
        'match_type',
        'match_level',
        'user_id'
    ];

    protected $guarded = [];

        
}