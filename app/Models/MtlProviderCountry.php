<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlProviderCountry
 */
class MtlProviderCountry extends Model
{
    protected $table = 'mtl_provider_country';

    public $timestamps = true;

    protected $fillable = [
        'provider_country_id',
        'name_en',
        'name_ru',
        'country_code',
        'provider_id',
        'global_country_id',
        'data_id'
    ];

    protected $guarded = [];

	public function provider () {
		 
		return $this->belongsTo('App\Models\MtlProvider', 'provider_id', 'id');
	}
        
}