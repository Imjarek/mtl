<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlProvider
 */
class MtlProvider extends Model
{
    protected $table = 'mtl_provider';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'data_type'
    ];

    protected $guarded = [];
}