<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlHotelAmenityRel
 */
class MtlHotelAmenityRel extends Model
{
    protected $table = 'mtl_hotel_amenity_rel';

    public $timestamps = false;

    protected $fillable = [
        'hotel_id',
        'amenity_id'
    ];

    protected $guarded = [];

        
}