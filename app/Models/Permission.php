<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Permission 
 */

class Permission extends EntrustPermission
{
	protected $table = 'permissions';
	
	protected $fillable = [
		'name',
		'display_name',
		'description'
	];
}