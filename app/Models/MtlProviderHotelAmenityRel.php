<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlProviderHotelAmenityRel
 */
class MtlProviderHotelAmenityRel extends Model
{
    protected $table = 'mtl_provider_hotel_amenity_rel';

    public $timestamps = false;

    protected $fillable = [
        'provider_hotel_id',
        'provider_amenity_id'
    ];

    protected $guarded = [];

        
}