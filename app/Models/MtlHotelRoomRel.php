<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlHotelRoomRel
 */
class MtlHotelRoomRel extends Model
{
    protected $table = 'mtl_hotel_room_rel';

    public $timestamps = false;

    protected $fillable = [
        'room_id',
        'hotel_id'
    ];

    protected $guarded = [];

        
}