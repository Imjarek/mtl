<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MtlProviderHotelRoom
 */
class MtlProviderHotelRoom extends Model
{
	protected $table = 'mtl_provider_hotel_room';

	public $timestamps = false;

	protected $fillable = [
	    'provider_room_type_id',
	    'name_en',
	    'name_ru',
	    'description',
	    'size',
	    'global_room_id',
	    'provider_id'
	];

	protected $guarded = [];

}