<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class MtlHotel
 */
class MtlHotel extends Model
{
	//use Eloquence;
	
	use SoftDeletes;
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['updated_at', 'created_at', 'deleted_at'];
    
	protected $table = 'mtl_hotel';

	public $timestamps = true;

	protected $appends = ['checked' => false, 'city_name' => null, 'country_name' => null, 'country' => null];

	protected $fillable = [
		'name_ru',
		'name_en',
		'code',
		'city_id',
		'region_id',
		'country_id',
		'latitude',
		'longitude',
		'data_id',
		'address',
		'formatted_address',
		'google_place_id',
		'description',
		'email',
		'phone',
//		'url',
		'photo',
		'pms_id'
	];

	protected $guarded = [];

	protected $searchable = ['name_ru'];
	
	function city() {
		
		return $this->belongsTo('App\Models\MtlCity', 'city_id');
	}
	function country() {
		
		return $this->city->belongsTo('App\Models\MtlCountry', 'country_id');
	}
	// страну получаем вручную а все потому что во фреймворке не предусмотрели обратное отношение belongsToThrough
	function getCountryAttribute() {
		if ($this->city)
			return MtlCountry::where('id', '=', $this->city->country_id)->first();
		return null;
	}
	function pms() {
		
		return $this->belongsTo('App\Models\MtlPMS', 'pms_id');
	}
	function providerHotels () {
		
		return $this->hasMany('App\Models\MtlProviderHotel', 'global_hotel_id');
	}
	function providers () {
		
		return $this->hasMany('App\Models\MtlProviderHotel', 'global_hotel_id');
	}
	public function setChecked($checked) {
	    return $this->attributes['checked'] = $checked;
	}
	
	public function hasAttribute($attr)
	{
		return array_key_exists($attr, $this->attributes);
	}
}