<?php

namespace App\Listeners;

use App\Events\RobotCommandFinished;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mtl\Classes\RobotLog;

class RobotCommandFinishedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(RobotLog $log)
    {
		$this->log = $log;
    }

    /**
     * Handle the event.
     *
     * @param  RobotCommandFinished  $event
     * @return void
     */
    public function handle(RobotCommandFinished $event)
    {
        // запись в логе об окончании команды
		
		$this->log->finish($event->command->id);
    }
}
