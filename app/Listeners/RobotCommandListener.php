<?php

namespace App\Listeners;

use App\Events\RobotCommand;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mtl\Classes\RobotLog;

class RobotCommandListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(RobotLog $log)
    {
        $this->log = $log;
    }
    /**
     * Handle the event.
     *
     * @param  RobotEvent  $event
     * @return void
     */
    public function handle(RobotCommand $command)
    {
		// запись в логе о пуске команды
		
		$name = explode(':', $command->command->signature)[1];
		
		$space = strpos($name, ' ');
		
		if ($space > 0)
			$name = substr($name, 0, $space);
		
		$params = array(
			'name' => $name,
			'description' => $command->command->description,
			'options' => json_encode($command->command->options())
		);
		
		$command->command->id = $this->log->insert($params);
		
    }
}
