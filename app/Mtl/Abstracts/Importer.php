<?php

namespace App\Mtl\Abstracts;
/* 
 * Обработка данных островка
 */
use App\Mtl\Interfaces\ImporterInterface;

abstract class Importer implements ImporterInterface {
	
	protected static $source;
	static public function start() {}
	static function process() {}
}
