<?php

namespace App\Mtl\Abstracts;

use App\Mtl\Interfaces\SourceInterface;

abstract class Source implements SourceInterface {
	
	abstract public function getData();
	
}
