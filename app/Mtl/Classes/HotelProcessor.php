<?php

namespace App\Mtl\Classes;

use App\Mtl\Abstracts\Importer;

use App\Mtl\Repos\ProviderHotelRepo;
use App\Mtl\Repos\HotelRepo;

use App\Models\MtlLink;
use App\Models\MtlLinkDatum;

use App\Mtl\Classes\Common;

use Illuminate\Support\Facades\DB;

/*
 * @author Sergey Cherednichenko 2017
 * @email sergey.i.che@gmail.com
 * @owner IBC Corporate Travel ct.ibc.ru
 * @description Класс выполняет обработку отелей поставщиков имеющихся в системе, для чего сперва получает их id
 * При обработке отелей Броневика сопоставление пропускается
 * Учитываются уже имеющиеся связи отелей БВК и Академии, между ними устанавливается связь без сопоставления
 * при передаче параметра provider_id будут обработан только один поставщик (полезно при обновлении данных одного постащика)
 */
class HotelProcessor {
		
	private static $providerHotelRepo;
	private static $hotelRepo;
	private static $providerId;
	
	static public function start($providerId = null) {
		
		self::$providerId = $providerId;
		self::$providerHotelRepo = new ProviderHotelRepo;
		self::$hotelRepo = new HotelRepo;
		self::process();
		
	}
	static public function process() {

		if (!config('mtl.restrict_matching'))
			$ids = self::$providerHotelRepo->getIds(self::$providerId);
		else
			$ids = self::$providerHotelRepo->_getIds(self::$providerId); // только в определенном радиусе (тест)
		
		foreach ($ids as $id) {
			
			$providerHotel = self::$providerHotelRepo->_getById($id);
			
			echo "{$providerHotel['name_ru']}\n";
			
			// для броневика матчинг не делаем, просто создаем гл. отели
			// но только если уже не связан с глобальным отелей
			if ($providerHotel->provider_id == 3 && !$providerHotel->global_hotel_id) {
				
				$newHotel = self::$hotelRepo->create($providerHotel);
				$providerHotel->global_hotel_id = $newHotel->id;
				$providerHotel->save();
				continue;
			}
			// для академ-сервиса если есть связь то находим по ней global_id и сохраняем указываем ее
			// а матчинг в этом случае не делаем
			if ($providerHotel->provider_id == 2)
				if ($bvkMatch = static::bvkMatch($providerHotel['provider_hotel_id'])) {
					$providerHotel->global_hotel_id = $bvkMatch;
					$providerHotel->save();
					continue;
				}
			// если один больше 0.8 то связываем
			// если таких больше то создаем похожие
			
			// если ничего не найдено то 
			// создаем глобальный отель и новую связь
							
			$providerHotelData = $providerHotel->makeHidden(['city_name', 'country_name'])->toArray();
			
			$matches = self::$providerHotelRepo->match($providerHotelData);
			
			self::processMatches($providerHotel, $matches);
		}
	}
	static function processMatches($providerHotel, $matches) {
		
		if (!count($matches)) {
			
//			if ($providerHotel->latitude && $providerHotel->longitude) {
			if (!$providerHotel->global_hotel_id) {// создаем новый отель если еще нет жесткой связки
				$newHotel = self::$hotelRepo->create($providerHotel);
				$providerHotel->global_hotel_id = $newHotel->id;
				$providerHotel->save();
			}
			return;
		}
		// те что больше 0.8
		$bestMatches = array_filter($matches, function ($match) {
			
			return $match['similarity'] >= 0.8;
		});
		// если совпадение с рангом >= 0.8 только одно то жестко связываем
		if (count($bestMatches) == 1 && isset($bestMatches[0])) {
			self::$providerHotelRepo->hardLink($providerHotel, $bestMatches[0]['hotel']['id'], $bestMatches[0]);
		}
		// если их несколько то создаем для них похожие
		if (count($bestMatches) >= 2)
			self::addSimilars ($providerHotel, $bestMatches);
		 
		// обработать
		// те что в пределах 0.4 - 0.8
		$mediumMatches = array_filter($matches, function ($match) {
			
			return ($match['similarity'] >= 0.4 and $match['similarity'] < 0.8);
		});
		
		// для единичных совпадений связываем
		if (count($mediumMatches) == 1 && isset($mediumMatches[0])) {
			self::$providerHotelRepo->hardLink($providerHotel, $mediumMatches[0]['hotel']['id'], $mediumMatches[0]);
		}
		// если несколько то сохраняем как похожие
		if (count($mediumMatches) > 0)
			self::addSimilars ($providerHotel, $mediumMatches);
		
	}
	
	static function addSimilars($newProviderHotel, $matches) {
		
		// TODO: Отранжировать массив по степени совпадения
		echo "Линкуем совпадения\n";
		
		//var_dump($matches); 
		foreach ($matches as $match) {
			// cоздаем связь
			
			$link = MtlLink::where([
				'global_id' => $match['hotel']['id'],
				'ref_id' => $newProviderHotel->id
				])->first();
			
			if ($link)
				$data = MtlLinkDatum::find($link->data_id);
			else
				$data = new MtlLinkDatum;
			
			$data->provider_id = $match['provider_id'];
			$data->match_type = 0; // т.е. автоматическое сопоставление
			$data->match_level = $match['similarity'];
			$data->user_id = 1; //т.е. робот
			
			$data->save();
			
			MtlLink::updateOrCreate([
				'global_id' => $match['hotel']['id'],
				'ref_id' => $newProviderHotel->id
			],[
				'global_id' => $match['hotel']['id'],
				'ref_id' => $newProviderHotel->id,
				'data_id' => $data->id
			]);
		}
	}
	private static function sortBySimilarity($matches) {
		
		usort($matches, function ($a, $b) {
			//echo $a['val'];
			if ($a['similarity'] == $b['similarity'])
				return 0;
			return ($a['similarity'] > $b['similarity']) ? -1 : 1; // по убыванию, первый элемент наипохожий
		});
		
		return $matches;
	}
	static function bvkMatch($id) {
		
		$res = DB::select('select bvk_hotel_id as id 
					from mtl_bvk_acase_match 
					where acase_hotel_id = ?',
			[$id]);
		
		if (count($res) > 0) {
			$found = DB::select('select global_hotel_id
						from mtl_provider_hotel 
						where provider_id = 1 and provider_hotel_id = ?',
				[$res[0]->id]);
			
			if (count($found) > 0)
				return $found[0]->global_hotel_id;
		}
	}
}

