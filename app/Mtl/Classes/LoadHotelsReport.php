<?php

namespace App\Mtl\Classes;

use App\Mtl\Abstracts\Report;

use App\Models\MtlCity;

use App\Models\MtlHotel;

use Illuminate\Support\Facades\DB;

class LoadHotelsReport extends Report {
	
	public $cities;
	
	public $url;
	
	function __construct() {
		
		$this->url = config('app.url');
		$this->fillIn();
	}
	
	public function fillIn() {
		
		$this->cities = MtlCity::all();
		//$this->cities = MtlCity::paginate(1);
		
		$this->cities = $this->cities->each(function ($city, $key) {
			
			// для города нужно найти все линки и приджойнить инфу по отелям
			
			$hotels = DB::select('select 
						h.id as hotel_id,
						h.name_ru as hotel_name_ru,
						h.address as hotel_address,
						ph.id as pid,
						ph.provider_hotel_id as provider_hotel_id,
						ph.name_ru as provider_hotel_name_ru,
						ph.address as provider_hotel_address,
						p.name as provider,
						ld.match_level,
						l.id match_id
						from mtl_link l
						join mtl_link_data ld
						on l.data_id = ld.id
						join mtl_hotel h 
						on l.global_id = h.id
						join mtl_provider_hotel ph
						on ph.id = l.ref_id
						join mtl_provider p
						on p.id = ld.provider_id
						where 
						ph.provider_id = 2
						and h.city_id = ?',
			[$city->id]);
			
			$city->hotels = $hotels;
		});
		
	}
	
}
