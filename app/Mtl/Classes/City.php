<?php

namespace App\Mtl\Classes;

use App\Mtl\Abstracts\Entity;

use App\Models\MtlProviderCity;
use App\Models\MtlHotel;
use App\Models\MtlCity;
use App\Models\MtlProviderCountry;

class City extends Entity {
	
	public $name_ru;
	public $name_en;
	public $latitude;
	public $longitude;
	public $country_id;
	public $id;
	
	private static $distance = 3;
			
	public function __construct($city) {
		$this->name_ru = $city->name_ru;
		$this->name_en = isset($city->name_en) ? $city->name_en : null;
		$this->latitude = $city->latitude;
		$this->longitude = $city->longitude;
		
		$this->city = $city; // нам понадобятся от какого поставщика город и его код пост. (идентификатор поставщика)
	}
	public function add() {
		
		return MtlCity::create([
			'name_ru' => $this->name_ru,
			'latitude' => $this->latitude,
			'longitude' => $this->longitude,
			'country_id' => $this->getCountryId() // написать функцию определения айдишки страны
		]);
	}
	function getCountryId () {
		
		// определяем id страны, если была сопоставлен
		// по id у пост.
		if ($this->city->provider_country_id)
			$country = MtlProviderCountry::where([
				'provider_id' => $this->city->provider_id,
				'provider_country_id' => $this->city->provider_country_id
			])
			->first();
		// либо по коду, который тоже уникален
		elseif ($this->city->country_code)
			// 
			$country = MtlProviderCountry::where([
				'provider_id' => $this->city->provider_id,
				'country_code' => $this->city->country_code
			])
			->first();
		else {
			echo "нет данных для определения страны {$this->city->name_ru} ({$this->city->provider_city_id})";
			return null;
		}
		
		if (!$country)
			return null;
		
		return $country->global_country_id;
	}
	
	function match() {
		
		// имеет смысл только при наличии координат
		if (!$this->latitude || !$this->longitude)
			return $this->matchByName();
		
		$distance = self::$distance;
		
		$city = MtlCity::whereRaw("distance({$this->latitude}, {$this->longitude}, latitude, longitude) < {$distance}")
			->first();
		
		return $city;
	}
	
	function matchByName () {
		$city = MtlCity::where('name_ru', '=', $this->name_ru)
			->orWhere('name_en', '=', $this->name_en)
			->first();
		
		return $city;
	}
	function distance ($city) {
		
		return Common::distance( (float)$this->latitude, (float)$this->longitude, $city['latitude'], $city['longitude'], 'K');
	}
	
	
}