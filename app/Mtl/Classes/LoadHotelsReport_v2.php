<?php

namespace App\Mtl\Classes;

use App\Mtl\Abstracts\Report;

use App\Models\MtlCity;

use App\Models\MtlHotel;

use Illuminate\Support\Facades\DB;

class LoadHotelsReport_v2 extends Report {
	
	public $cities;
	
	public $url;
	
	function __construct() {
		
		$this->url = config('app.url');
		$this->fillIn();
	}
	
	public function fillIn() {
		
		$this->cities = MtlCity::all();
		
		$this->cities = $this->cities->each(function ($city, $key) {
			
			// для города нужно найти все линки и приджойнить инфу по отелям
			
			$hotels = DB::select('select h.id
						,h.name_ru
						,h.address
						,group_concat(" ", concat(pr.name, " (", ph.provider_hotel_id, ")")) providers
						from mtl_hotel h
						right join mtl_provider_hotel ph
						on ph.global_hotel_id = h.id
						join mtl_provider pr
						on pr.id = ph.provider_id
						where h.city_id = ?
						and ph.provider_id in (2, 3)
						group by h.id, h.name_ru, h.address
						order by h.name_ru asc
						limit 3
						',
			[$city->id]);
			
			$city->hotels = $hotels;
		});
	}
	
}
