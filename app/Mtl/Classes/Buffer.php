<?php

/*
 * Класс для работы с выделением отелей
 */
namespace App\Mtl\Classes;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Mtl\Interfaces\BufferInterface;

class Buffer implements BufferInterface {
	
	
	public function reset () {
		
		$uid = Auth::user()->id;
		
		DB::table('mtl_buffer')->where([
			     'uid' => $uid,
			     'item_type' => 'hotel' // параметризовать тип объекта
			 ])->delete();
	}

	public function add ($itemId, $state, $itemType = 'hotel') {
		
		$uid = Auth::user()->id;

		if (!isset($uid)) return;
		
		$cnt = DB::table('mtl_buffer')->where(['uid' => $uid, 'item_type' => 'hotel'])->count();
		
		if ($state == 1) {
			
			if ($cnt >= 2)
				return array('result' => false);
		
			DB::table('mtl_buffer')->insert(['uid' => $uid, 'item_id' => $itemId, 'item_type' => 'hotel']
			);
			
			return array('result' => true, 'cnt' => $cnt + 1);
		}
		else {
		     DB::table('mtl_buffer')->where([
			 'item_id' => $itemId,
			 'uid' => $uid,
			 'item_type' => 'hotel'
		     ])->delete();
		     
		     return array('result' => true, 'cnt' => $cnt - 1);
		}
		
	}
}
