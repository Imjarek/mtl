<?php

namespace App\Mtl\Classes;
/* 
 * Операции с логом сопоставлений
 */

//use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;


class MatchLog {
	
//	public function __construct() {
//		
//	}
	public static function insert($params) {
		
		DB::table('mtl_match_log')
			->insert($params);
	}
	
	public function search($request) {
		
		$entries = DB::select('select pr.name as provider, l.id as log_id, h.id as hotel_id, ph.provider_hotel_id, h.name_ru as hotel_name, ph.name_ru as provider_hotel_name, l.*
				from mtl_match_log l
				left join mtl_hotel h
				on h.id = l.hotel_id
				left join mtl_provider_hotel ph
				on ph.provider_hotel_id = l.provider_hotel_id and l.provider_id = ph.provider_id
				left join mtl_provider pr
				on pr.id = l.provider_id'
			);
//			->where('created_at', '<', $request->date_before)
//			->where('created_at', '>', $request->date_after)
		
		return $entries;
	}
}
