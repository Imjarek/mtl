<?php

namespace App\Mtl\Classes;

use App\Mtl\Repos\ProviderHotelRepo;

use App\Models\MtlProviderHotel;

use App\Mtl\Classes\RequestGeocode;

class Geocoding {
	
	public function _make($providerId = null) {
		
		//$repo = new ProviderHotelRepo;
		$ids = $repo->getIds($providerId);
		
		foreach ($ids as $id) {
			
			$providerHotel = $repo->getById($id);

			if (!$providerHotel->latitude || !$providerHotel->latitude) continue;
			
			// определить город и присобачить к строке 
			
			if (!$providerHotel->provider_city_id) continue;
			
			$city = $repo->getCity($providerHotel);
			
			// закоментировать чтобы запрашивать посторно (обновлять)
			if  ($providerHotel->google_place_id) continue;
			
			$request = new RequestGeocode($providerHotel, $city->name_ru);
			
			$geodata = $request->get();
			
			if ($geodata = $result['geodata']) {
				
				echo "$geodata->formatted_address\n";
				
				$providerHotel->formatted_address = $geodata->formatted_address;

				$providerHotel->google_place_id = $geodata->place_id;

				$providerHotel->save();

				//var_dump(@$geodata->formatted_address);
			}
			else
		echo "Не распознан: {$result['address']}\n";
		}
	}
	public function make($providerId = null) {
		
		MtlProviderHotel::chunk(1000, function ($hotels) use ($providerId) {
			
			if ($providerId)
				$hotels = $hotels->where('provider_id', '=', $providerId);
			
			foreach ($hotels as $providerHotel) {
				
				if (!$providerHotel->latitude || !$providerHotel->latitude) continue;
				
				// не запрашиваем если уже распознан или уже запрошен, но не распознан (экономим квоту Google API)
				if  ($providerHotel->google_place_id || $providerHotel->g_not_found)
					continue;

				$request = new RequestGeocode($providerHotel);

				$result = $request->get();

				if (isset($result['geodata'])) {
					
					$geodata = $result['geodata'];
					
					echo "{$result['address']} --> $geodata->formatted_address\n";

					$providerHotel->formatted_address = $geodata->formatted_address;
					$providerHotel->google_place_id = $geodata->place_id;
					$providerHotel->g_latitude  = $geodata->geometry->location->lat;
					$providerHotel->g_longitude  =$geodata->geometry->location->lng;
					
					$providerHotel->save();

					//var_dump(@$geodata->formatted_address);
				}
				else {
					$providerHotel->g_not_found = 1;
					$providerHotel->save();
					echo "Не распознан: {$result['address']}\n";
				}
					
			}
		});
	}
}
