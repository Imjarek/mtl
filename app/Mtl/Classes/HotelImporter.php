<?php

namespace App\Mtl\Classes;

use App\Mtl\Abstracts\Importer;

use App\Mtl\Repos\ProviderHotelRepo;
use App\Mtl\Repos\HotelRepo;

use App\Models\MtlProviderCity;

use App\Models\MtlLink;
use App\Models\MtlLinkDatum;

use App\Mtl\Classes\Common;

/*
 * @author Sergey Cherednichenko 2017
 * @email sergey.i.che@gmail.com
 * @owner IBC Corporate Travel ct.ibc.ru
 * @description Класс релизует методы для загрузки статических данных отелей поставщиков из их источников - обычно дампов ../bin/dumps
 * и является базовым для конкретных классов учитывающих при импорте специфику поставщиков (см. app/repos)
 */
class HotelImporter extends Importer {
		
	static public function start() {
	}
	static public function process() {
		
		$providerHotelRepo = new ProviderHotelRepo;
		$hotelRepo = new HotelRepo;
		
		foreach (self::$source->getData() as $item) {
			
			$providerHotelData = static::mapParams($item);
			
			echo "{$providerHotelData['name_ru']}\n";
			
			// фильтр локации
			if (config('mtl.restrict_import'))
				if (!self::closeTo($providerHotelData))
					continue;
			
			$newProviderHotel = $providerHotelRepo->create($providerHotelData);
		}
	}
	private static function closeTo ($hotel) {
		
		// search location (краснодар)
//		$lat = 45.0350;
//		$lon = 38.9743;
		// search location (питер) 59.939095, 30.315868

		$lat = 59.9390;
		$lon = 30.3158;
		
		if (Common::distance((float)$hotel['latitude'], (float)$hotel['longitude'], $lat, $lon, 'K') < 20)
			return true;
	}
	
}

