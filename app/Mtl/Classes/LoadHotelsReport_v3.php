<?php

namespace App\Mtl\Classes;

use App\Mtl\Abstracts\Report;

use App\Models\MtlCity;

use App\Models\MtlHotel;

use Illuminate\Support\Facades\DB;

class LoadHotelsReport_v3 extends Report {
	
	public $cities;
	
	public $cityId;
	
	public $url;
	
	function __construct() {
		
		$this->url = config('app.url');
	}
	
	public function fillIn() {
		
		$this->cities = DB::select('
			select c.id,
					c.name_ru as name,
					acase.id as acase_id,
					acase.name_ru as acase_name,
					bvk.id as bvk_id,
					bvk.name_ru as bvk_name
				from mtl_city c
				join mtl_provider_city acase
				on acase.global_city_id = c.id and acase.provider_id = 2
				join mtl_provider_city bvk
				on bvk.global_city_id = c.id and bvk.provider_id = 3
				where ? is null or c.id = ?', [$this->cityId, $this->cityId]
			);
		
		$this->cities = collect($this->cities);
		
		$this->cities = $this->cities->each(function ($city, $key) {
			
			// для города нужно найти все линки и приджойнить инфу по отелям
			
			$hotels = DB::select("
				select i.* from (
					select
						h.id,
						h.created_at as date,
						concat_ws(', ', h.name_ru, h.address) as name,
						(select group_concat(name_ru SEPARATOR ' | ') 
							from mtl_provider_hotel where global_hotel_id = h.id and provider_id = 2 
							group by global_hotel_id) as acase_name
						,(select group_concat(created_at SEPARATOR ' | ')
							from mtl_provider_hotel where global_hotel_id = h.id and provider_id = 2 
							group by global_hotel_id) as acase_date
						,(select group_concat(name_ru SEPARATOR ' | ')
							from mtl_provider_hotel where global_hotel_id = h.id and provider_id = 3 
							group by global_hotel_id) as bvk_name
						,(select group_concat(created_at SEPARATOR ' | ') 
							from mtl_provider_hotel where global_hotel_id = h.id and provider_id = 3
							group by global_hotel_id) as bvk_date
						,h.created_at
					from mtl_hotel h
					where h.city_id = ?
				) i
				where i.acase_name is not null
						",
			[$city->id]);
			
			$city->hotels = $hotels;
		});
	}
}
