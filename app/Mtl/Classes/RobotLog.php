<?php

namespace App\Mtl\Classes;
/* 
 * Операции с логом сопоставлений
 */

//use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

use Illuminate\Support\Facades\DB;


class RobotLog {
	
//	public function __construct() {
//		
//	}
	public $command;
	
	public function insert($params) {
		$entry = array();
		
		$command = DB::select('select * from mtl_tmp.robot_command where sys_name = ?', array($params['name']));
		
		// если команды нет в справочнике нужно добавить
		if (!$command || !is_array($command)) {
			$cmdId = DB::table('mtl_tmp.robot_command')
			->insertGetId(
				array('sys_name' => $params['name'], 'description' => $params['description'])
			);
		}
		else {
			$cmdId = $command[0]->id;
		}
		
		$entry['command_id'] = $cmdId;
		$entry['options'] = $params['options'];
		$entry['sent_at'] = new Carbon();
			
		$inserted = DB::table('mtl_tmp.robot_command_log')
			->insertGetId($entry);
		
		return $inserted;
	}
	public function search($request = null) {
		
		$entries = DB::connection('mtl_tmp')
			->select('select log.id, cmd.name,  cmd.sys_name, uid, sent_at, completed_at, options, comment
				from mtl_tmp.robot_command_log log
				join mtl_tmp.robot_command cmd
				on cmd.id = log.command_id'
			);
//			->where('created_at', '<', $request->date_before)
//			->where('created_at', '>', $request->date_after)
		
		return $entries;
	}
	public function finish($id) {
		
		$status = 1; // TODO: предусмотреть статус ошибки 2
		
		$finished = new Carbon();
		
		DB::update('update mtl_tmp.robot_command_log
			set completed_at = ?, status = ?
			where id = ?', array($finished, $status, $id));
	}
}
