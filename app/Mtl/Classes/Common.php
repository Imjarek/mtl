<?php

namespace App\Mtl\Classes;

use \ZipArchive;

use  \RecursiveIteratorIterator;

use \RecursiveDirectoryIterator;

class Common {
	
	/*
	 * Различные функции для работы с данными
	 */
	public static function mapProvider($providerCode) {
		
		$providerId = null;
		
		switch ($providerCode) {
			
			case 'ostrovok': $providerId = 1;
			break;
			case 'acase': $providerId = 2;
			break;
			case 'bvk': $providerId = 3;
			break;
			case 'amadeus': $providerId = 4;
			break;
			case 'bol': $providerId = 5;
			break;
			case 'hotelbook': $providerId = 6;
		}
		return $providerId;
	}
	public static function exactMatchTitle($title1, $title2, &$matched = []) {
		
		// Функция проверяет точное совпадение названий учитывая обозначение типа отеля в названии
		$title1 = mb_strtolower($title1);
		$title2 = mb_strtolower($title2);
		
		if ($title1 === $title2)
			return true;
		
		$title1 = trim(str_replace(self::getHotelTypes(), '', $title1), ' "'); // перед сравнением убираем тип отеля, пробелы и кавычи 
		$title2 = trim(str_replace(self::getHotelTypes(), '', $title2), ' "');
		
		if ($title1 === $title2) {
			
			$matched['exact_title'] = true;
			return true;
		}
	}
	
	public static function matchTitle ($title1, $title2, &$matched = []) {
		// функция возвращает кол-во одинаковых слов в названии за исключением типов
		
		$title1 = mb_strtolower($title1);
		$title2 = mb_strtolower($title2);
		
		$title1 = trim(str_replace(self::getHotelTypes(), '', $title1), ' "');
		$title2 = trim(str_replace(self::getHotelTypes(), '', $title2), ' "');
		
		$array1 = explode(' ', mb_strtolower($title1));
		$array2 = explode(' ', mb_strtolower($title2));
		
		$matches = array_intersect($array1, $array2);
		
		if ((count($matches)) > 0)
			$matched['title'] = true;
		
		return count($matches) > 0 ? 0.2 : 0;
	}
	
	public static function matchPhone($data, $hotel, &$matched = []) {
		
		if (!isset($data['phone']) || !isset($hotel['phone']))
			return 0;
		
		$phone1 = self::normalizePhone($data['phone']);
		
		$phone2 = self::normalizePhone($hotel['phone']);
		
		if ($phone1 === $phone2) {
			$matched['phone'] = true;
			return 0.1;
		}
		return 0;
	}
	// приведение телефона
	private static function normalizePhone($num) {
		
		$num = str_replace('+7', '', $num);
		$num = preg_replace('/[^0-9\-]/', '', $num);
		return $num;
	}
	public static function matchUrl($data, $hotel) {
		
		if (!isset($data['url']) || !isset($hotel['url']))
			return 0;
		return $data['url'] === $hotel['url'] ? 0 : 0.1;
	}
	
	public static function matchEmail($data, $hotel, &$matched = []) {
		
		if (!isset($data['email']) || !isset($hotel['email']))
			return 0;
		
		return $data['email'] === $hotel['email'] ? 0.1 : 0;
	}
	/*
	 * словарик слов не участвующие в сопоставлении названий
	 * @returns array
	 * 
	 */
	private static function getHotelTypes() {
		return array(
			'гостевой дом', 'конгресс отель', 'парк отель', 
				'мини отель', 'мини-отель', 'конгресс-отель', 'парк-отель', 
				'резорт', 'санаторий', 'гостиница',
					'отель', 'хостел', 'апартаменты',
		);
	}
	public static function matchAddress ($address1, $address2, &$matched = []) {
		// функция возвращает кол-во совпавших чисел в строках
	
		preg_match_all('!\d+!', $address1, $array1);
		preg_match_all('!\d+!', $address2, $array2);
		
		$matches = array_intersect($array1[0], $array2[0]);
		
		if (count($matches) > 0) {
			$matched['address'] = true; 
			return 0.2;
		}
		return 0;
	}
	
	/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::                                                                         :*/
	/*::  This routine calculates the distance between two points (given the     :*/
	/*::  latitude/longitude of those points). It is being used to calculate     :*/
	/*::  the distance between two locations using GeoDataSource(TM) Products    :*/
	/*::                                                                         :*/
	/*::  Definitions:                                                           :*/
	/*::    South latitudes are negative, east longitudes are positive           :*/
	/*::                                                                         :*/
	/*::  Passed to function:                                                    :*/
	/*::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :*/
	/*::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :*/
	/*::    unit = the unit you desire for results                               :*/
	/*::           where: 'M' is statute miles (default)                         :*/
	/*::                  'K' is kilometers                                      :*/
	/*::                  'N' is nautical miles                                  :*/
	/*::  Worldwide cities and other features databases with latitude longitude  :*/
	/*::  are available at http://www.geodatasource.com                          :*/
	/*::                                                                         :*/
	/*::  For enquiries, please contact sales@geodatasource.com                  :*/
	/*::                                                                         :*/
	/*::  Official Web site: http://www.geodatasource.com                        :*/
	/*::                                                                         :*/
	/*::         GeoDataSource.com (C) All Rights Reserved 2015		   		     :*/
	/*::                                                                         :*/
	/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	public static function distance($lat1, $lon1, $lat2, $lon2, $unit) {

	  $theta = $lon1 - $lon2;
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	  $dist = acos($dist);
	  $dist = rad2deg($dist);
	  $miles = $dist * 60 * 1.1515;
	  $unit = strtoupper($unit);

	  if ($unit == "K") {
		return ($miles * 1.609344);
	  } else if ($unit == "N") {
		  return ($miles * 0.8684);
		} else {
			return $miles;
		  }
	}
	
	public static function propsDiff($item1, $item2) {
		
		$arr = array();
		
		if (!method_exists($item1, 'getAttributes')) // для обычных объектов (не моделей)
			return self::_propsDiff($item1, $item2);
		
		foreach($item1->getAttributes() as $key => $val) {
			
			if (isset($item2[$key]) && $val !== $item2[$key])
				$arr[] = $key;
		}
		
		return self::normalizeAttributes($arr, $item1, $item2);
	}
	public static function _propsDiff($item1, $item2) {
		
		$arr = array();
		foreach($item2->getAttributes() as $key => $val) {
			
			if (property_exists($item1, $key))
				if ($val !== $item1->$key)
					$arr[] = $key;
		}
		
		return self::normalizeAttributes($arr, $item1, $item2);
	}
	public static function __propsDiff($item1, $item2) {
		
		$arr = array();
		foreach($item1 as $key => $val) {
			
			if ($val !== $item2[$key])
				$arr[] = $key;
		}
		
		return self::normalizeAttributes($arr, $item1, $item2);
	}
	private static function normalizeAttributes($arr, &$item1, &$item2) {
		
		$idx1 = array_search('latitude', $arr);
		$idx2 = array_search('latitude', $arr);
			
		if ($idx1 || $idx2) {
			array_push($arr, 'coords');
		}
		// для полей из отношений
//		if (isset($item1->city) && $item1->city->name_ru)
//			if ($item2->city && $item2->city->name_ru)
//				if ($item1->city->name_ru !== $item2->city->name_ru)
//					array_push($arr, 'city_name');
//				
//		if (isset($item1->country) && $item1->country->name_ru)
//			if ($item2->country && $item2->country->name_ru)
//				if ($item1->country->name_ru !== $item2->country->name_ru)
//					array_push($arr, 'country_name');	
		return $arr;
	}
	public static function serverError ($message = null) {
		
		
	}
	public static function Zip($source, $destination)
{
	/*
	 * Создание архива из папки
	 */
    if (!extension_loaded('zip') || !file_exists($source)) {
        return false;
    }

    $zip = new ZipArchive();
    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
        return false;
    }

    $source = str_replace('\\', '/', realpath($source));

    if (is_dir($source) === true)
    {
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

        foreach ($files as $file)
        {
            $file = str_replace('\\', '/', $file);

            // Ignore "." and ".." folders
            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                continue;

            $file = realpath($file);

            if (is_dir($file) === true)
            {
                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
            }
            else if (is_file($file) === true)
            {
                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
            }
        }
    }
    else if (is_file($source) === true)
    {
        $zip->addFromString(basename($source), file_get_contents($source));
    }

    return $zip->close();
}
}

