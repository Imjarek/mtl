<?php

namespace App\Mtl\Classes;

use App\Mtl\Abstracts\Entity;

use App\Models\MtlProviderCountry;
use App\Models\MtlCountry;

class ProviderCountry extends Entity {
	
	public function __construct() {
	}
	static public function create ($country, $provider_id) {
		
		$country['provider_id'] = $provider_id;
		
		// создаем или обновляем страну
		$record = MtlProviderCountry::updateOrCreate([
				'provider_country_id' => $country['id'],
				'provider_id' => $provider_id
			], $country);
		
		// записываем в глобальный справочник
		if (!$record['global_country_id']) {
			
			// Здесь нужно получить ID глобальной страны
			
			// Есть ли уже такая глобальная страна?
			// проверку лучше сделать по коду страны
			if ($found = MtlCountry::where('name_ru', '=', $country['name_ru'])
				->first()) {
				
				$found = $found->toArray();
				$record['global_country_id'] = $found['id'];
				$record->save();
			}
			else {
				$newGlobalCountry = MtlCountry::create($country);
			
				// создаем связь
				$record['global_country_id'] = $newGlobalCountry['id'];

				$record->save();
			}
			
		}
	}
}