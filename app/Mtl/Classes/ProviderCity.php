<?php

namespace App\Mtl\Classes;

use App\Mtl\Abstracts\Entity;

use App\Models\MtlProviderCity;
use App\Models\MtlCity;

use App\Models\MtlProviderCountry;

class ProviderCity extends Entity {
	
	public $provider_city_id;
	public $name_ru;
	public $name_en;
	public $latitude;
	public $longitude;
	public $provider_country_id;
	public $provider_id;
			
	public function __construct($city, $provider_id) {
		
		// dd($city);
		$this->provider_city_id = $city['provider_city_id'];
		$this->name_ru = $city['name_ru'];
		$this->name_en = $city['name_en'];
		$this->latitude = isset($city['latitude']) ? $city['latitude'] : null;
		$this->longitude = isset($city['longitude']) ? $city['longitude'] : null;
		$this->country_code = $city['country_code'];
		$this->provider_country_id = isset($city['provider_country_id']) ? $city['provider_country_id'] : null;
		// TODO: нужно предусмотреть ситуацию когда нет ни кода страны ни id страны
		$this->provider_id = $provider_id;
		
	}
	private function setCountryId() {
		
		if (!$this->country_code)
			return null;
		
		$country = MtlProviderCountry::where([
			'country_code' => $this->country_code,
			'provider_id' => $this->provider_id
		])->first();
		
		if ($country)
			$this->provider_country_id = $country->provider_country_id;
	}
	public function add() {
		
		if (!$this->provider_country_id)
			$this->setCountryId();
		
		$city = array (
			'name_ru' => $this->name_ru,
			'name_en' => $this->name_en,
			'latitude' => $this->latitude,
			'longitude' => $this->longitude,
			'country_code' => $this->country_code,
			'provider_country_id' => $this->provider_country_id,
			'provider_id' => $this->provider_id
		);
		// создаем или обновляем город
		try {
			$record = MtlProviderCity::updateOrCreate([
				'provider_city_id' => $this->provider_city_id,
				'provider_id' => $this->provider_id
			], $city);
		} catch ( \Exception $e) {
			echo "не могу создать город {$this->name_ru} ({$this->provider_city_id}) [{$this->provider_country_id}]\n";
			var_dump($e);
			return;
		}
		
		// записываем в глобальный справочник
		if (!$record['global_city_id']) {
			
			//$city['country_id'] = self::getCountryId($city['provider_country_id'], $provider_id);
			// ищем такой глобальный город
			// если нет добавляем
			// если есть проставляем id
			$newCity = new City($this);

			$match = null;
			
			// if ($this->provider_id !== 3) // при первичной загрузке пропускаем сопоставление для городов БВК
				
			$match = $newCity->match();
			
			if ($match)
				$record['global_city_id'] = $match['id'];
			
			else
				$record['global_city_id'] = $newCity->add()->id;
			
			// создаем связь
			$record->save();
		}
	}
}