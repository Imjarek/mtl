<?php

namespace App\Mtl\Classes;
/* 
 * Вовращает форм. и адрес google_place_id
 */

class RequestGeocode {
	
	public $address;
	public $city;
	
	function __construct($hotel) {
		$this->address = $hotel->address;
		$this->latitude = isset($hotel->latitude) ? $hotel->latitude : null;
		$this->longitude = isset($hotel->longitude) ? $hotel->longitude : null;
		//$this->city = $hotel->city->name_ru;
	}
	function get() {
		
		//$this->address = $this->city ? $this->city.', ' . $this->address : $this->address;
		
		//echo $this->address;
		
		return self::request();
		
	}
	public function request() {
		
		$this->address = urlencode($this->address);
		
		$bounds = '';
		
		// область поиска немного не прямоугольная)
		if ($this->longitude && $this->longitude)
			$bounds = ($this->latitude - 0.2) .',' . ($this->longitude - 0.2) . 
				'|' . ($this->latitude + 0.2) . ',' . ($this->longitude + 0.2);
		
		$url = "https://maps.googleapis.com/maps/api/geocode/json?bounds=$bounds&language=ru&address=" . $this->address . "&key=AIzaSyBMLm-fqO3_-sT-m0oFbY-a7CvBpyopqVI";
		
		$ch = curl_init($url);
				
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		//curl_setopt($ch, CURLOPT_HEADER, TRUE);
		//curl_setopt($ch, CURLOPT_VERBOSE, TRUE); 
		curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, TRUE);

		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array('Accept-Language' => 'ru-ru,ru;q=0.8'));

		//curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		
		$result = json_decode(curl_exec($ch));
		
		curl_close($ch);
		
		if ($result->results)
			return $result->results[0];
		
	}
}