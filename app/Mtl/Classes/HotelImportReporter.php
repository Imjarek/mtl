<?php

namespace App\Mtl\Classes;

use App\Mtl\Interfaces\ReporterInterface;

use App\Mail\LoadHotelsReportMail;

use Illuminate\Support\Facades\Mail;

use App\Mtl\Classes\LoadHotelsReport_v2;


class HotelImportReporter implements ReporterInterface {
	
	static $toAddress;
	private $report;
	
	static function send() {
		
		Mail::to(self::$toAddress)
		    ->send(new LoadHotelsReportMail(self::generate()));
	}
	static function generate() {
		echo "Cоздаем отчет\n";
		
		$report = new LoadHotelsReport_v2;
		
		return new $report;
		
	}
}

