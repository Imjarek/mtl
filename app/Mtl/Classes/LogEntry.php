<?php

/* 
 * Реализует запись в журнале
 */
namespace App\Mtl\Classes;

class LogEntry {
	
	public $type;
	public $comment;
	public $objectId;
	public $subjectId;
	public $data;
	
	public function __construct($params) {
		
		$this->actionType = $params['action_type']; // обязательный
		$this->objectType = $params['object_type']; // обязательный
		
		$this->comment = isset($params['comment']) ? $params['comment'] : null;
		$this->objectId = isset($params['object_id']) ? $params['object_id'] : null;
		$this->subjectId = isset($params['subject_id']) ? $params['subject_id'] : null;
		$this->actionIds = isset($params['data']) ? $params['data'] : null;
	}
}
