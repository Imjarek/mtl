<?php

namespace App\Mtl\Classes;
/* 
 * Вовращает форм. и адрес google_place_id
 */

class RequestGeocode {
	
	public $address;
	public $city;
	
	function __construct($hotel, $city = null) {
		$this->language = 'ru';
		$this->address = $hotel->address;
		$this->latitude = $hotel->latitude;
		$this->longitude = $hotel->longitude;
		$this->city = $city;
		
		if ($city)
			$this->address = $this->city->name_ru . ', ' . $this->address;
		
//		if ($hotel->provider_id === 4 || $hotel->provider_id = 5)
//			$this->address = $this->city->name_en . ', ' . $this->address;
		//$this->city = $hotel->city->name_ru;
	}
	function get() {
		
		//$this->address = $this->city ? $this->city.', ' . $this->address : $this->address;
		
		//echo $this->address;
		
		return self::request();
		
	}
	public function request() {
		
		$language = 'ru';
		$this->address = urlencode($this->address);
		
		$bounds = '';
		
		// область поиска немного не прямоугольная)
		if ($this->longitude && $this->longitude)
			$bounds = ($this->latitude - 0.2) .',' . ($this->longitude - 0.2) . 
				'|' . ($this->latitude + 0.2) . ',' . ($this->longitude + 0.2);
		
		$url = "https://maps.googleapis.com/maps/api/geocode/json?bounds=$bounds&language=$this->language&address=".$this->address."&key=AIzaSyD_hxb4B9cvFJHZ_ifFtSzVZ3M_ceosuAQ";
		
		//echo "$url\n";
		
		$ch = curl_init($url);
				
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		//curl_setopt($ch, CURLOPT_HEADER, TRUE);
		//curl_setopt($ch, CURLOPT_VERBOSE, TRUE); 
		curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, TRUE);

		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array('Accept-Language' => 'ru-ru,ru;q=0.8'));

		//curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		
		$result = json_decode(curl_exec($ch));
		
		curl_close($ch);
		
		if ($result && isset($result->error_message))
			return array('address' => $result->error_message ); // увидим если например квоту превысили
		if ($result && $result->results)
			return array('geodata' => $result->results[0], 'address' => $this->address);
		return array('address' => $this->address);
	}
}