<?php

/* 
 * Реализует операции с журналом
 */

namespace App\Mtl\Classes;

use App\Models\MtlLog;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use App\Mtl\Classes\LogEntry;

use App\Mtl\Interfaces\LogInterface;


class Log implements LogInterface {
	
	public $uid;
	
	public function __construct() {
		
		//$this->uid = 2;
    
	}
	public function show(Request $request) {
		
		$request->flash();
		
		// конечно надо денормализовтаь по полю action_type т.е. сделать справочник операций
		// в случае если предвидиться увеличение кол-ва возможных действий в системе требующих логирование
		$entries = DB::table('mtl_log as l')
			->leftJoin('mtl_hotel as h', 'h.id', '=', 'l.object_id')
			->leftJoin('users as u', 'l.uid', '=', 'u.id')
			->select(
					'l.id',
					//'h.name_ru as object_name',
					DB::raw("(case when l.object_type = 'hotel' then concat(' <a href=\"/hotel/', h.id, '\">', '№ ', h.id, ' ', h.name_ru, '</a>') 
							else h.name_ru end) 
							as object_name"),
					'l.object_type',
					DB::raw("case when l.action_type = 'union' 	then 'Объединение' 
							when l.action_type = 'union_undo' then 'Отмена объединения'
							when l.action_type = 'link' then 'Привязка'
							when l.action_type = 'unlink' then 'Отвязка'
							when l.action_type = 'create' then 'Создание'
							when l.action_type = 'delete' then 'Удаление'
							when l.action_type = 'edit' then 'Редактирование'
							else l.action_type end as action_type"),
					'u.name as user_name',
					'u.email as user_email',
					'l.comment',
					'l.created_at'
			);
		
		if ($request->date_after)
			$entries->whereDate('l.created_at', '<=', $request->date_after);
		
		if ($request->date_before)
			$entries->whereDate('l.created_at', '>=', $request->date_before);
		
		if ($request->log_id)
			$entries->where('id', $request->log_id);
		
		if ($request->user_id)
			$entries->where('uid' , $request->user_id);
		
		if ($request->object_type)
			$entries->where('object_type' , $request->object_type);
		
		if ($request->action_type)
			$entries->where('action_type' , $request->action_type);
		
		// если требуется сортировка
		if ($request->sorting && $request->sorting_param)
			$entries->orderBy($request->sorting_param, $request->sorting);
		return $entries->paginate(25);
	}
	
	public function insert(LogEntry $entry) {
		
		$this->uid = Auth::user()->id;
		
		MtlLog::create([
			'uid' => $this->uid,
			'action_type' => $entry->actionType,
			'object_type' => $entry->objectType,
			'object_id' => $entry->objectId,
			'subject_id' => $entry->subjectId,
			'comment' => $entry->comment,
			'data' => $entry->data
		]);
	}
	public function find($id) {
		
		return MtlLog::find($id);
	}
	
	public function getUnlinked($objectId) {
		
		return MtlLog::where('action_type', 'unlink')
			->where('object_id', $objectId)
			->where('object_type', 'hotel')
			->pluck('subject_id');
	}
//	public function getUnions($id) {
//		
//		return
//			DB::select("
//				select mtl_log.id, mtl_hotel.id as hotel_id, mtl_hotel.name_ru
//				from mtl_log
//					join mtl_hotel
//					on mtl_hotel.id = mtl_log.subject_id
//					where object_id = {$id} and action_type = 'union'
//					and subject_id not in (
//						select t.subject_id
//						from mtl_log t
//						left join mtl_log u
//						on t.object_id = u.object_id 
//							and t.subject_id = u.subject_id and t.id < u.id
//						where t.action_type = 'union'
//							and u.action_type = 'union_undo'
//				);
//			");
//	}
}