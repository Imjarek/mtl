<?php

namespace App\Mtl\Classes;

use App\Mtl\Abstracts\Report;

use App\Models\MtlCity;

use App\Models\MtlHotel;

use Illuminate\Support\Facades\DB;

use \stdClass;

class LoadHotelsReport_v4 extends Report {
	
	public $cities;
	public $cityId;
	public $countryId;
	public $managerId;
	public $url;
	
	function __construct() {
		
		$this->url = config('app.url');
	}
	
	public function fillIn() {
		
		$emptyManager = new stdClass;
		$emptyManager->id = null;
		$emptyManager->fio = 'нет';
			
		$this->managers = DB::select('
			select id, fio
				from mtl_manager
				where ? is null or id = ?
				-- limit 5
				', [$this->managerId, $this->managerId]
		);
	
		$this->managers = collect($this->managers);
		
		if (!$this->managerId) {
			$this->managers->push($emptyManager);
		}
		
		if ($this->managerId === 'nobody')
			$this->managers = collect(array($emptyManager)); // параметр для запроса без менеджера where mtl_manager = null
		
		$this->managers = $this->managers->each(function ($manager, $key) {
			
			$hotels = DB::select("
				SELECT 
					mn.fio,
					mco.name_ru as 'country',
					mc.name_ru as 'city',
					h.name_ru as 'name',
					h.id as 'hotel_id',
					group_concat(if(mh.provider_id=2, mh.name_ru, null)) as 'acase_name',
					group_concat(if(mh.provider_id=2, concat_ws(', ', mh.address, mh.url, mh.email, mh.phone, concat('ID ', mh.provider_hotel_id)), null)) as 'acase_info',
					group_concat(if(mh.provider_id=3, mh.name_ru, null)) as 'bvk_name',
					group_concat(if(mh.provider_id=3, concat_ws(', ', mh.address, mh.url, mh.email, mh.phone, concat('ID ', mh.provider_hotel_id)), null)) as 'bvk_info',
					group_concat(if(mh.provider_id=1, mh.name_ru, null)) as 'ostrovok_name',
					group_concat(if(mh.provider_id=1, concat_ws(', ', mh.address, mh.url, mh.email, mh.phone, concat('ID ', mh.provider_hotel_id)), null)) as 'ostrovok_info',
					group_concat(if(mh.provider_id=5, mh.name_ru, null)) as 'bol_name',
					group_concat(if(mh.provider_id=5, concat_ws(', ', mh.address, mh.url, mh.email, mh.phone, concat('ID ', mh.provider_hotel_id)), null)) as 'bol_info',
					group_concat(if(mh.provider_id=6, mh.name_ru, null)) as 'hb_name',
					group_concat(if(mh.provider_id=6, concat_ws(', ', mh.address, mh.url, mh.email, mh.phone, concat('ID ', mh.provider_hotel_id)), null)) as 'hb_info'
				FROM `mtl_hotel` h
					LEFT JOIN `mtl_city` mc
						ON mc.id = h.city_id
					LEFT JOIN `mtl_country` mco
						ON mco.id = mc.country_id 
					LEFT JOIN `mtl_provider_hotel` mh
						ON mh.global_hotel_id = h.id
					LEFT JOIN `mtl_manager` as mn
						ON mn.id = mc.manager_id
				WHERE 1=1
					and (? is not null or mc.manager_id is null)
					and (? is null or mc.manager_id = ?)
					and (? is null or mc.id = ?)
					and (? is null or mco.id = ?) 
					and h.deleted_at is NULL
					GROUP BY h.id
					ORDER BY mc.name_ru, h.name_ru
						",
			[$manager->id, $manager->id, $manager->id, $this->cityId, $this->cityId, $this->countryId, $this->countryId]);
			
			$manager->hotels = $hotels;
			
		});
		
	}
}
