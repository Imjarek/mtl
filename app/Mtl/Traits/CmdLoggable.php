<?php

namespace App\Mtl\Traits;

use App\Events\RobotCommand;
use App\Events\RobotCommandFinished;

trait CmdLoggable {
	
	private function start() {
		event(new RobotCommand($this));
	}
	
	private function finish() {
		event(new RobotCommandFinished($this));
	}
}
