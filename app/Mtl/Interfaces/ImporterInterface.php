<?php

namespace App\Mtl\Interfaces;

interface ImporterInterface {
	static function start();
	static function process();
};

