<?php
/*
 * Определеят операции с журналом
 */
namespace App\Mtl\Interfaces;

use Illuminate\Http\Request;

use App\Mtl\Classes\LogEntry;

interface LogInterface {
	
	public function show(Request $request);
	public function insert(LogEntry $entry);
}

