<?php

namespace App\Mtl\Interfaces;

interface SourceInterface {
	
	public function getData();
}
