<?php

namespace App\Mtl\Interfaces;

interface ReporterInterface {
	
	static function generate();
}