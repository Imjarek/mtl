<?php

namespace App\Mtl\Repos;

use App\Models\MtlHotel;
use App\Models\MtlProviderHotel;
use App\Models\MtlProviderCity;
use App\Models\MtlProviderCountry;

use App\Mtl\Classes\Common;

use App\Mtl\Repos\HotelRepo;

use App\Mtl\Classes\Log;

use App\Mtl\Classes\MatchLog;

use Illuminate\Support\Facades\DB;


class ProviderHotelRepo {
	
	const DISTANCE = 0.150;
	const DISTANCE2 = 1;
	
	public static function getByProviderHotelId ($hotelProviderId, $providerId) {
		
		return MtlProviderHotel::where('provider_id', $providerId)
			->where('provider_hotel_id', $$hotelProviderId)
			->first();
	}
	public function search ($request) {
		
		$coords = $request->get('coords');
		
		$radius = $request->get('radius');
		
		$hotelName = $request->get('hotel_name');
		
		$city = $request->get('city');
		
		$country = $request->get('country');
		
		$providerCode = Common::mapProvider($request->get('provider_code'));
		
		$hotelId = $request->get('hotel_id');
		
		$providerHotelId = $request->get('provider_hotel_id');
		
		$matches = $request->get('matches');
		
		$dateBefore = $request->get('date_before');
		
		$dateAfter = $request->get('date_after');
		
		$this->hotels = MtlProviderHotel //
			::whereNull('global_hotel_id')
			//::where('global_hotel_id', '=', null)
			->with('matches')
			->leftJoin('mtl_provider_city as c', function ($join){
				$join->on('c.provider_city_id', '=', 'mtl_provider_hotel.provider_city_id');
				$join->on('c.provider_id', '=', 'mtl_provider_hotel.provider_id');
			})
			->leftJoin('mtl_provider_country as cn', function ($join) {
				$join->on('cn.provider_country_id', '=', 'c.provider_country_id');
				$join->on('cn.provider_id', '=', 'mtl_provider_hotel.provider_id');
			})
			->leftJoin('mtl_provider as pr', 'mtl_provider_hotel.provider_id', '=', 'pr.id')
			//->leftJoin('mtl_link as l', 'mtl_provider_hotel.id', '=', 'l.ref_id')
			->select(
				'mtl_provider_hotel.id', // если убрать id то отношение matches не грузится - почему?
				'mtl_provider_hotel.name_ru as hotel_name',
				'mtl_provider_hotel.id as hotel_id',
				'mtl_provider_hotel.provider_hotel_id as provider_hotel_id',
				'mtl_provider_hotel.created_at',
				'mtl_provider_hotel.latitude',
				'mtl_provider_hotel.longitude',
				'mtl_provider_hotel.g_latitude',
				'mtl_provider_hotel.g_longitude',
				'c.name_ru as hotel_city_name',
				'cn.name_ru as hotel_country_name',
				'pr.name as provider_name'
			);
		
		if ($hotelName)
			$this->hotels->where('mtl_provider_hotel.name_ru', 'LIKE', "%$hotelName%");
		
		if ($city)
			$this->hotels->where('c.name_ru', 'LIKE', "%$city%");
			
		if ($country)
			$this->hotels->where('cn.name_ru', 'LIKE', "%$country%");
			
		if ($providerCode)
			$this->hotels->where('mtl_provider_hotel.provider_id', '=', $providerCode);
		
		if ($hotelId)
			$this->hotels->where('mtl_provider_hotel.id', '=', $hotelId);
		
		if ($providerHotelId)
			$this->hotels->where('mtl_provider_hotel.provider_hotel_id', '=', $providerHotelId);
		
		if ($dateBefore)
			$this->hotels->where('mtl_provider_hotel.created_at', '>=', $dateBefore);
//		
		// протестировать на меньше либо равно
		if ($dateAfter)
			$this->hotels->where('mtl_provider_hotel.created_at', '<=', $dateAfter);
		
		if ($matches)
			if ($matches === 'yes')
				$this->hotels->has('matches');
			else 
				$this->hotels->doesntHave('matches');
		
		// если требуется сортировка
		if (!$request->sorting_param) {
			old('sorting_param', 'hotel_name');
			old('sorting', 'ASC');
			$this->hotels->orderBy('mtl_provider_hotel.name_ru', 'ASC')->orderBy('c.name_ru', 'ASC');
		}
		elseif ($request->sorting_param == 'city_name')
			$this->hotels->orderBy('c.name_ru', $request->sorting);
		else
				$this->hotels->orderBy($request->sorting_param, $request->sorting);
		
		if ($coords && $radius)
			return $this->searchByCoords($coords, $radius)->paginate(25);
		
		return $this->hotels->paginate(25);
		
	}
	public function searchByName($name) {
		
		return MtlProviderHotel::where(
			'name_ru', 'like', "%{$name}%"
		)->distinct();
		    
	}
	private function searchByCoords($coords, $radius) {
		
		$coords = explode (',', $coords);
		
		$lat = $coords[0];
		$lng = $coords[1];
		
		$radius = $radius / 1000;
		
		return $this->hotels->whereRaw(
			"mtl.distance(mtl_provider_hotel.latitude, mtl_provider_hotel.longitude, {$lat}, {$lng}) < {$radius}"
		);
		
	}
	/*
	 * @param $providerCode
	 * @returns $providerId
	 * 
	 */
	public function total () {
		
		return MtlProviderHotel::count();
		
	}
	public function totalUnprocessed () {
		
		return MtlProviderHotel::whereNull('global_hotel_id')
			->count();
		
	}
	public function getById($id) {
		// не используем модель из-за непонятной ситуации с джойном по ДВУМ ключам и корректным пробросом связи отель->город->страна
		$hotel = DB::table('mtl_provider_hotel as hotel')
			->leftJoin('mtl_provider_city as city', function ($j) {
				$j->on('city.provider_city_id', '=', 'hotel.provider_city_id');
				$j->on('city.provider_id', '=', 'hotel.provider_id');
			})
			->leftJoin('mtl_provider_country as country', function ($j) {
				$j->on('country.provider_country_id', '=', 'city.provider_country_id');
				$j->on('country.provider_id', '=', 'city.provider_id');
			})->join('mtl_provider as provider', 'hotel.provider_id', '=', 'provider.id')
			->where('hotel.id', '=', $id)
			->select(
				'hotel.id as hotel_id',
				'hotel.provider_city_id',
				'hotel.name_ru as hotel_name',
				'hotel.provider_hotel_id',
				'hotel.provider_id',
				'city.name_ru as city_name',
				'country.name_ru as country_name',
				'hotel.address',
				'hotel.description',
				'hotel.raw_data',
				'hotel.latitude',
				'hotel.longitude',
				'hotel.g_latitude',
				'hotel.g_longitude',
				'hotel.phone',
				'hotel.photo',
				'hotel.url',
				'hotel.email',
				'hotel.not_exists',
				'hotel.global_hotel_id',
				'hotel.created_at',
				'provider.name as provider_name',
				'provider.data_type as provider_data_type'
			)->first();
		
		return $hotel;
//		
		//return MtlProviderHotel::find($id); // так конечно было бы лучше (видимо будет проще в поздней версии eloquent)
	}
	public function _getById($id) {
		$hotel = MtlProviderHotel::find($id);
		
		//$hotel->city_name = MtlProviderCity::where(['provider_id' => $hotel->provider_id, 'provider_city_id' => $hotel->provider_city_id]);
		//dd($hotel->country_name);
		return $hotel;
		
	}
	public function getIds($providerId = null) {
		
		if ($providerId)
			return MtlProviderHotel::where('provider_id', '=', $providerId)->pluck('id');
		
		return MtlProviderHotel::all()->pluck('id');
	}
	public function _getIds($providerId = null) {
		
		// 55.030199, 82.920430
		// 59.939095, 30.315868
		$lat = 59.939095; 
		$lon = 30.315868;
		
		if ($providerId)
			return MtlProviderHotel::where('provider_id', '=', $providerId)
				->whereRaw("distance(latitude, longitude, {$lat}, {$lon}) < 20")
				->take(20)
				->pluck('id');
		
		return MtlProviderHotel::whereRaw("distance(latitude, longitude, {$lat}, {$lon}) < 20")
			->pluck('id');
	}
	public static function create($item) {
		// создаем или обновляем по ключу
		
		$item['not_exists'] = null; // если вдруг был такой флаг снимаем
		
		$providerHotel =  MtlProviderHotel::updateOrCreate([
			'provider_hotel_id' => $item['provider_hotel_id'],
			'provider_id' => $item['provider_id']
		], $item);
		
		return $providerHotel;
	}
	public function match($data, $forced = null) {
		
		if (!isset($data['latitude']) || !isset($data['longitude']))
			return;
		
		$distance = self::DISTANCE;
		
		$matches = array();
		
		if (!$forced) {
			$globalCityId = HotelRepo::getGlobalCityId($data);
			// ограничиваем поиск отелями в данном городе
			$hotels = MtlHotel::where('city_id', $globalCityId);
		}
		else {
			// примерный фильтр по координатам
			$distance = 0.180;
			$hotels = MtlHotel::whereNotNull('id'); // another simple way please 
			// to prepare query builer instance for subsequent "where" clause
		}
		
		// находим лежащие в данном радиусе
		if (isset($data['g_latitude']) && isset($data['g_longitude']))
			$hotels->whereRaw("distance(latitude, longitude, {$data['g_latitude']}, {$data['g_longitude']}) < {$distance}");
		else
			$hotels->whereRaw("distance(latitude, longitude, {$data['latitude']}, {$data['longitude']}) < {$distance}");
		
		$hotels = $hotels->get()->toArray();
		
		foreach ($hotels as $hotel) {
			
			$matched = array();
			
			// сначала проверяем случай строгого совпадения имен и близкого расположения
			if (Common::exactMatchTitle($data['name_ru'], $hotel['name_ru'], $matched))
				
				return array(
					array(
						'hotel' => $hotel,
						'similarity' => 1,
						'provider_id' => $data['provider_id'],
						'matched' => array('exact_title' => true)
					)
				);
			
			$similarity = 0.3; 
			
			// здесь храняться параметры которые значения которых совпали
			
			if (isset($data['google_place_id']) && isset($hotel['google_place_id'])) {
				if ($data['google_place_id'] === $hotel['google_place_id']) {
					$similarity += 0.5;
					$matched['google_place_id'] = true;
				}
			}
			else
				$similarity += Common::matchAddress($data['address'], $hotel['address'], $matched);
			
			$similarity += Common::matchTitle($data['name_ru'], $hotel['name_ru'], $matched);
			
			$similarity += Common::matchEmail($data, $hotel, $matched);
			
			$similarity += Common::matchUrl($data, $hotel, $matched);
			
			$similarity += Common::matchPhone($data, $hotel, $matched);
			
			if ($similarity > 1)
					$similarity = 1;
				
			$matches[] = array(
				'hotel' => $hotel,
				'similarity' => $similarity,
				'provider_id' => $data['provider_id'],
				'matched' => $matched
			);
		}
		
		return $matches;
	}
	
	public function _getByGlobalHotelId($id) {
		return MtlProviderHotel::with('provider')->where('global_hotel_id', $id)
			->get();
	}
	
	public function getByGlobalHotelId($id) {
		// не используем модель из-за непонятной ситуации с джойном по ДВУМ ключам и корректным пробросом связи отель->город->страна
		$hotels = DB::table('mtl_provider_hotel as hotel')
			->leftJoin('mtl_provider_city as city', function ($j) {
				$j->on('city.provider_city_id', '=', 'hotel.provider_city_id');
				$j->on('city.provider_id', '=', 'hotel.provider_id');
			})
			->leftJoin('mtl_provider_country as country', function ($j) {
				$j->on('country.provider_country_id', '=', 'city.provider_country_id');
				$j->on('country.provider_id', '=', 'city.provider_id');
			})->join('mtl_provider as provider', 'hotel.provider_id', '=', 'provider.id')
			->where('hotel.global_hotel_id', '=', $id)
			->select(
				'hotel.id as hotel_id',
				'hotel.provider_city_id',
				'hotel.name_ru as hotel_name',
				'hotel.provider_hotel_id',
				'hotel.provider_id',
				'city.id as city_id',
				'city.name_ru as city_name',
				'country.id as country_id',
				'country.name_ru as country_name',
				'hotel.address',
				'hotel.description',
				'hotel.raw_data',
				'hotel.latitude',
				'hotel.longitude',
				'hotel.g_latitude',
				'hotel.g_longitude',
				'hotel.phone',
				'hotel.photo',
				'hotel.url',
				'hotel.email',
				'hotel.not_exists',
				'hotel.global_hotel_id',
				'hotel.created_at',
				'provider.name as provider_name',
				'provider.data_type as provider_data_type'
			)->get();
		
		return $hotels;
	}
	
	/*
	 * Перенос отелей поставщика от одного родительского гл. отеля c id2 к другому (id1)
	 */
	public function relink($id1, $id2) {
		
		MtlProviderHotel::where('global_hotel_id', $id2)
		    ->update(['global_hotel_id' => $id1]);
		
	}
	private function getDistance($data, $hotel) {
		return Common::distance(
			    (float)$hotel['latitude'], (float)$hotel['longitude'], (float)$data['latitude'], (float)$data['longitude'], 'K'
			);
	}
	public function getCity ($hotel) {
		
		return MtlProviderCity::where([
			'provider_city_id' => $hotel->provider_city_id,
			'provider_id' => $hotel->provider_id
		])->first();
		
	} 
	static function hardLink($providerHotel, $id, $matched = null) {
		$providerHotel->global_hotel_id = $id;
		$providerHotel->save(['global_hotel_id']);
		
		if ($matched) {
			$params = array(
				'hotel_id' => $id,
				'provider_hotel_id' => $providerHotel->provider_hotel_id,
				'provider_id' => $matched['provider_id'],
				'google_place_id' => @$matched['matched']['google_place_id'],
				'title' => @$matched['matched']['title'],
				'exact_title' => @$matched['matched']['exact_title'],
				'phone' => @$matched['matched']['phone'],
				'url' => @$matched['matched']['url'],
				'email' => @$matched['matched']['email'],
				'range' => $matched['similarity'],
				'created_at' => date('Y-m-d H:m:s'),
			);
			
			MatchLog::insert($params);
		}
	}
}
