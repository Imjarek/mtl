<?php

/*
|--------------------------------------------------------------------------
| Операции с глобальными номерами
|--------------------------------------------------------------------------
|
| Выборка, сопоставление
|
*/

namespace App\Mtl\Repos;

use App\Models\MtlProviderHotel;
use App\Models\MtlHotel;
use App\Models\MtlHotelRoom;
use App\Models\MtlProviderHotelRoom;


class HotelRoomRepo {
	
//	public function getByHotelId ($hotelId) {
//		
//		return MtlProviderHotel::find($hotelId);
//	}
	
	public function match(MtlProviderHotelRoom $providerHotelRoom) {
	
		// сопоставление номеров по имени твин-дабл
		// пока просто создаем гл. номер
		
		$hotelRoom = new MtlHotelRoom;
				
		$hotelRoom->name_ru = $providerHotelRoom->name_ru;
		$hotelRoom->description = $providerHotelRoom->description  ? $providerHotelRoom->description : null;

		$hotelRoom->save();
		
		$providerHotelRoom->global_room_id = $hotelRoom->id;
		
		$providerHotelRoom->save();
		
	}
	public function create($roomData) {
		
		return MtlProviderHotelRoom::updateOrCreate([
		    'provider_id' => $roomData['provider_id'],
		    'provider_room_type_id' => $roomData['provider_room_type_id']
		    ], $roomData);
		
	}
	public function search ($request) {
		
			}
	public function getById($id) {
		
		
	}	
}
