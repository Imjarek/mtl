<?php

/*
|--------------------------------------------------------------------------
| Операции с глобальными отелями
|--------------------------------------------------------------------------
|
| Поиск, фильтрация, найти по id и т.д
|
*/

namespace App\Mtl\Repos;

use App\Models\MtlProviderHotel;
use App\Models\MtlHotel;

use App\Models\MtlCity;
use App\Models\MtlCountry;

use App\Models\MtlProviderCity;
use App\Models\MtlProviderCountry;

use App\Mtl\Classes\Log;

use Illuminate\Support\Facades\DB;

class HotelRepo {
	
//	public function getByHotelId ($hotelId) {
//		
//		return MtlProviderHotel::find($hotelId);
//	}
	
	public function create($item) {
		
		$hotel = new MtlHotel;		
		
		$hotel->name_ru = $item['name_ru'];
		$hotel->latitude = $item['g_latitude'] ? $item['g_latitude'] : ($item['latitude'] ?: null);
		$hotel->longitude = $item['g_longitude'] ? $item['g_longitude'] : ($item['longitude'] ?: null);
		
		$hotel->address = $item['address'] ?: null;
		$hotel->description = $item['description'] ?: null;
		$hotel->phone = $item['phone'] ?: null;
		$hotel->photo = $item['photo'] ?: null;
		
		$hotel->google_place_id = $item['google_place_id'] ?: null;
		$hotel->formatted_address = $item['formatted_address'] ?: null;
		
		$hotel->city_id = self::getGlobalCityId($item);
		$hotel->country_id = self::getGlobalCityId($item);
		
		$hotel->save();
		
		return $hotel;
	}
	public function search ($request) {
			
		$hotelId = $request->get('hotel_id');
		
		$coords = $request->get('coords');
		
		$radius = $request->get('radius');
		
		$dateBefore = $request->get('date_before');
		
		$dateAfter = $request->get('date_after');
		
		$hotelName = $request->get('hotel_name');
		
		$city = $request->get('city');
		
		$country = $request->get('country');
		
		$this->hotels = MtlHotel::
			where('mtl_hotel.name_ru', 'LIKE', "%$hotelName%")
			->leftJoin('mtl_city as c', 'c.id', '=', 'mtl_hotel.city_id')
			->leftJoin('mtl_country as cn', 'cn.id', '=', 'c.country_id')
			->select(
				'mtl_hotel.name_ru as hotel_name',
				'mtl_hotel.id as hotel_id',
				'mtl_hotel.created_at',
				'mtl_hotel.description',
				'mtl_hotel.address',
				'mtl_hotel.phone',
				'mtl_hotel.photo',
				'mtl_hotel.email',
				'mtl_hotel.url',
				'mtl_hotel.latitude',
				'mtl_hotel.longitude',
				'c.name_ru as city_name',
				'c.id as city_id',
				'cn.name_ru as country_name',
				'cn.id as country_id'
			);
		
		if ($hotelId)
			$this->hotels->where('mtl_hotel.id', '=', $hotelId);
		
		if ($city)
			$this->hotels->where('c.name_ru', 'LIKE', "%$city%");
			
		if ($country)
			$this->hotels->where('cn.name_ru', 'LIKE', "%$country%");
			
		if ($dateBefore)
			$this->hotels->where('created_at', '>=', $dateBefore);
//		
		// протестировать на меньше либо равно
		if ($dateAfter)
			$this->hotels->where('created_at', '<=', $dateAfter);
		
		// если требуется сортировка
		
		if (!$request->sorting_param) {
			old('sorting_param', 'hotel_name');
			old('sorting', 'ASC');
			$this->hotels->orderBy('mtl_hotel.name_ru', 'ASC')->orderBy('c.name_ru', 'ASC');
		}
		elseif ($request->sorting_param === 'hotel_city')
			$this->hotels->orderBy('c.name_ru', $request->sorting);
		else
			$this->hotels->orderBy($request->sorting_param, $request->sorting);
		
		if ($coords && $radius)
			return $this->searchByCoords($coords, $radius);
		
		return $this->hotels;
		
	}
	public function searchByName($name) {
		
		return MtlHotel::where(
			'name_ru', 'like', "%{$name}%"
		);
	}
	public function getById($id) {
		
		return  MtlHotel::find($id);
	}
	public static function getGlobalCityId($item) {
		$city = MtlProviderCity::where('provider_city_id', $item['provider_city_id'])
					->where('provider_id', $item['provider_id'])
					->first();
		
		return @$city->global_city_id ? $city->global_city_id : null;
	}
	public static function getGlobalCountryId() {
		
		$country = MtlProviderCountry::where('provider_country_id', $item['provider_country_id'])
					->where('provider_id', $item['provider_id'])
					->first();
		
		return @$country->global_country_id ? $city->global_country_id : null;
	}
	public function total () {
		
		return MtlHotel::count();
	}
	private function searchByCoords($coords, $radius) {
		
		$coords = explode (',', $coords);
		
		$lat = $coords[0];
		$lng = $coords[1];
		
		$radius = $radius / 1000;
		
		return $this->hotels->whereRaw(
			"mtl.distance(mtl_hotel.latitude, mtl_hotel.longitude, {$lat}, {$lng}) < {$radius}"
		);
	}

	public function getUnions($id) {
		
		return DB::table('mtl_union as u')
			->join('mtl_hotel as h', 'u.dst_id', '=', 'h.id')
			->where([
				'u.src_id' => $id,
				'item_type' => 'hotel'
			])->select('u.id', 'h.id as hotel_id', 'h.name_ru')
			->get();
	}
	public function edit($request) {
		
		$name = $request->get('name');
        $value = $request->get('value');
		$id = $request->get('hotelId');
		
		$hotel =  MtlHotel::find($id);
		
		if(!$hotel)
			return array('message' => 'Отель не найден');
		
		$data = $request->get('data');
	
		if (!$data)
			return array('message' => 'Нет изменений');
		
		$warnings = '';
		
		foreach ($data as $field => $value) {

			$edit = $this->editField($hotel, $field, $value);
			if (!isset($edit['result']))
				$warnings .= $edit['message'] . "\n";
		}
		
		return array('result' => true, 'message' => "Отель &laquo{$hotel->name_ru}&raquo сохранен", 'warnings' => $warnings);
	}
	public function editField($hotel, $name, $value) {
		
		if(!$hotel)
			return array('message' => 'Отель не найден');
		
		if ($name == 'city') {
			$change = $this->changeCity($hotel, $value);
			
			if (!$change)
				return array('message' => 'Указанный город не найден');
			else
				return array('result' => true, 'message' => 'Поле [ город ] изменено');
		}
		
		elseif ($name == 'country') {
			$change = $this->changeCountry($hotel, $value);
			
			if (!$change)
				return array('message' => 'Указанная страна не найдена');
			else 
				return array('result' => true, 'message' => 'Поле [ country ] изменено');
		}
		else {
			// валидация координат
			if ($name == 'latitude' || $name == 'longitude') 
				{	
					$value = (float)$value;
					
					if (!$value || !is_float($value) || $value > 180 || $value < -180)
						return array('message' => 'Укажите правильное значение');
				}
			
			if (!$hotel->hasAttribute($name))
				return array('message' => "Нет поля $name");
			
			if ($name == 'pms_id' && $value == 0)
				$value = null;
			
			$hotel->$name = $value;

			$hotel->save();
		}
		
		return array('result' => true, 'message' => "Изменено поле [ $name ] отеля");
	}
	public function changeCity ($hotel, $value) {
		
		$city = MtlCity::where('name_ru', $value)->first();
		
		if (!$city)
			return;
			
		$hotel->city()->associate($city)->save();
		
		return true;
	}
//	не имеет смысла если страна определяется через город
//	public function changeCountry ($hotel, $value) {
//		
//		$country = MtlCountry::where('name_ru', $value)->first();
//		
//		if (!$country)
//			return;
//		
//		$hotel->country()->associate($country)->save();
//		
//		return true;
//	}
	public function changePMS ($hotel, $value) {
		
		$pms = MtlPMS::where('name_ru', $value)->first();
		
		if (!$pms)
			return;
		
		$hotel->pms()->associate($pms)->save();
		
		return true;
	}
	
	public function delete($id) {
		
		$hotel = MtlHotel::find($id);
		
		if(!$hotel)
			return array('message' => 'Отель не найден (возможно уже был удален)');
		
		$hotelName = $hotel->name_ru;
		$hotel->delete();
		// отвязать
		
		MtlProviderHotel::where('global_hotel_id', '=', $id)
			->update(['global_hotel_id' => null]);
		
		return array('result' => true, 'message' => "Отель &laquo$hotelName&raquo был удален");
	}
}
