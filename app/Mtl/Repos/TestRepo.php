<?php

namespace App\Mtl\Repos;

use App\Models\MtlProviderHotel;
use App\Models\MtlHotel;

class TestRepo {
	
//	public function getByHotelId ($hotelId) {
//		
//		return MtlProviderHotel::find($hotelId);
//	}
	
	public function create($item) {
		
		$hotel = new MtlHotel;
		
		$hotel->name_ru = $item['name_ru'];
		$hotel->latitude = $item['latitude'];
		$hotel->longitude = $item['longitude'];
		$hotel->city_id = $item['global_city_id'];
		$hotel->country_id = null;
		
		$hotel->save();
		
		return $hotel;
	}
	public function search ($request) {
		
		$hotel_name = $request->get('hotel_name') ;
		$city_name = $request->get('city_name');
		
		$hotels = MtlHotel::with('city')->where('name_ru', 'LIKE', "%$hotel_name%")->paginate(25);
		
		return $hotels;
		
	}
	public function getById($id) {
		
		return  MtlHotel::with('city')->find($id);
	}	
}
