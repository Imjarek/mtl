<?php

namespace App\Mtl\Provider\Amadeus;

use App\Mtl\Abstracts\Source;

class BolCitySource extends Source {
	
	public function getData() {
		
		$file = file_get_contents( base_path().'/../../bin/dumps/bronevik/bvk_cities.json');

		echo "Обработка json с городами Амадеуса\n";

		$data = json_decode($file, JSON_OBJECT_AS_ARRAY);

		foreach ($data as $city) {

			// пророссийский фильтр
//			if ($city['country_code'] !== 'RU')
//				continue;

			yield $city;
		}
	}
}