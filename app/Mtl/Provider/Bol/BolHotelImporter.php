<?php

namespace App\Mtl\Provider\Bol;

use App\Mtl\Classes\HotelImporter;


class BolHotelImporter extends HotelImporter {
	
	const PROVIDER_ID = 5;
	
	static public function start() {
		
		echo "Загрузка данных отелей [BedsOnline]\n";
		
		self::$source = new BolHotelSource;
		
		self::process();
	}
	
	static function mapParams($hotel) {

		$item = array(
			'provider_hotel_id' => $hotel['code'],
			'name_ru' => $hotel['name']['content'],
			'latitude' => @$hotel['coordinates'] ? $hotel['coordinates']['latitude'] : null,
			'longitude' => @$hotel['coordinates'] ? $hotel['coordinates']['longitude'] : null,
			'country_code' => $hotel['countryCode'],
			'description' => @$hotel['description'] ? $hotel['description']['content'] : null,
			'provider_id' =>  self::PROVIDER_ID,
			'address' => $hotel['address']['content'],
			'email' => @$hotel['email'] ? $hotel['email'] : null,
			'url' => @$hotel['web'] ? $hotel['web'] : null,
			'phone' => @$hotel['phones'] ? $hotel['phones'][0]['phoneNumber'] : null
		);
		
		//var_dump($item);
		return $item;
	}
	
//	'provider_city_id' => $item['region_id'],
//			'provider_country_id' => self::getProviderCountryId($item['country_code']), // преобразовать 'country_code' в 'provider_id'
//			'provider_hotel_id' => $item['id'],
//			'name_ru' => $item['name'],
//			'name_en' => null,
//			'latitude' => $item['latitude'],
//			'longitude' => $item['longitude'],
//			'provider_id' =>  self::PROVIDER_ID,
//			'raw_data' => $item['raw_data'],
//			'address' => $data->address,
//			'description' => $data->description,
//			'phone' => $data->phone,
//			'photo' => ($data->images && $data->images[0]) ? $data->images[0]->url : null,
	
}
