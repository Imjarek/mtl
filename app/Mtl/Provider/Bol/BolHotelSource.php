<?php

namespace App\Mtl\Provider\Bol;
use App\Mtl\Abstracts\Source;


class BolHotelSource extends Source {
	
	private static $path = '../../bin/dumps/bol/hotels/';
	
	public static $countries = array('AZ', 'AM', 'BY', 'BG', 'GE', 'KZ', 'LV', 'LT', 'RU', 'TJ', 'UZ', 'UA', 'AB', 'KG', 'TM', 'MD');
	
	public function getData() {
		
		$filelist = scandir( base_path(self::$path) );
		
		foreach($filelist as $filename) {
			
			$file = file_get_contents( base_path(self::$path) . $filename);
			
			if (!$file && empty($file)) continue;

			echo "Обработка дампа [ $filename ]\n";
				
			$data = json_decode($file, JSON_OBJECT_AS_ARRAY);
			
			if (count($data['hotels']) > 0)
				foreach ($data['hotels'] as $hotel) {

					// ограничение по странам (СССР)
					if (config('mtl.restrict_import_by_country')) {
						if (!self::countryToImport($hotel['countryCode']))
							continue;
					}
					
					yield $hotel;
				}
		}
	}
	private static function countryToImport($cc) {
		
		if (array_search($cc, self::$countries) > 0)
			return true;
	}
}
