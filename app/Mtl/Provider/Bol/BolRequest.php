<?php

namespace App\Mtl\Provider\Bol;

class BolRequest {
	
	public $hotelId;
	public $checkIn;
	public $checkOut;
	public $adults;
	public $result;
	
	public function __construct() {
		
	}
	public function load () {
		
		$ch = curl_init();
		
		//$errors = '/tmp/bol-errors';
		//curl_setopt($ch, CURLOPT_USERPWD, "1064:1ada79d8-c100-4d35-ad51-cf59bf38590e");
		
		$params = self::getParams();
		
		$params = http_build_query($params);
		
		curl_setopt($ch, CURLOPT_URL, 'http://test.apibol.ru/api/booking/hotels');

		curl_setopt($ch, CURLOPT_POST, true);

		//curl_setopt($ch, CURLOPT_VERBOSE, TRUE);  

		//curl_setopt($ch, CURLOPT_STDERR, $errors);

		//curl_setopt($ch, CURLOPT_HEADER, true);

		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		
		$params = $this->getParams();
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));                                                                  

		$header = $this->getHeader(strlen(json_encode($params)));

		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$res = curl_exec($ch);

		curl_close($ch);
		
		return $res;
		
	}
	
	private function getParams() {
		
		return array(
			'language' => 'RUS',
			'stay' => array('checkIn' => $this->checkIn, 'checkOut' => $this->checkOut),
			'occupancies' => array(
				'occupancy' => array(
					'rooms' => $this->rooms,
					'adults' => $this->adults,
					'children' => $this->children
				)
			),
			'hotels' => array('hotel' => array($this->hotelId))
		);
	}
	 private function getHeader($len) {
		 
		// NET|TEST11|RU
		$apiKey = 'MTT-oKn5wuV4gJttCFJh8K9Dr-KJp7Uc';
		$secret = 'j3Mk7BA9WpzKK7E9CjgXYD3XGd1kqYMA';
		
		// NET|TEST66|WW
//		$apiKey = '2Azu32aBlJVZ74m9yoiD6lYslTDTZr5O';
//		$secret = 'r9w1EaxC8XxDSwO7VVe43PcZ2gwOXgGz';
		$signature = hash("sha256", $apiKey.$secret.time());

		$header = array(
			'Api-Key: ' . $apiKey,
			'X-Signature: ' . $signature,
			'X-Time: ' . time(),
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . $len
		);

		return $header;
	 }
}

