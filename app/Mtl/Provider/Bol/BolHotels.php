<?php

namespace App\Mtl\Provider\Bol;

class BolHotels {
	
	const LIMIT = 200; // сколько кусков загружаем
	const CHUNK = 1000; // кол-во гостиниц в куске
	private $from;
	private $to = 0;
	
	private $fields = 'name, city, address, phone, description, phones, email, coordinates, web, countryCode';
	
	public function __construct() {
		$this->path = base_path('../../bin/dumps/bol/hotels/');
		
		// bol_data.json
	}
	public function get() {
		
		$this->cleanUp();
		
		for ($i = 1; $i < self::LIMIT + 1; $i++) {
			
			$this->from = $this->to + 1;
			$this->to = $this->from + self::CHUNK - 1;
			
			$response = self::request();
			
			//var_dump($response);
			$fileName = $this->getFileName();
			file_put_contents($this->path . $fileName, $response);

			if (strlen($response) > 0)
				echo "получено и сохранено " . round(strlen($response) / 1024, 2) . " КБ - $fileName\n";
			else 
				echo "данные не получены\n";
		}
	}
	private function getFileName() {
		$time = date('d-m-Y');
		
		return "bol_hotel_data_{$this->from}_{$this->to}_$time.json";
	}
	private function request () {
		
		$ch = curl_init();
		
		//$errors = '/tmp/bol-errors';
		//curl_setopt($ch, CURLOPT_USERPWD, "1064:1ada79d8-c100-4d35-ad51-cf59bf38590e");
		
		$params = self::getParams();
		
		$params = http_build_query($params);
		
		curl_setopt($ch, CURLOPT_URL, 'http://test.apibol.ru/api/content/hotels?' . $params);

		//curl_setopt($ch, CURLOPT_POST, true);

		//curl_setopt($ch, CURLOPT_VERBOSE, TRUE);  

		//curl_setopt($ch, CURLOPT_STDERR, $errors);

		//curl_setopt($ch, CURLOPT_HEADER, true);

		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));                                                                  

		$header = $this->getHeader(strlen(json_encode($params)));

		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$res = curl_exec($ch);

		curl_close($ch);
		
		return $res;
		
	}
	
	private function getParams() {
		
		$params = array(
			'fields' => $this->fields, // или all
			'language' => 'RUS',
			//'countryCode' => 'RU',
			'to' => $this->to,
			'from' => $this->from
		);
		
		return $params;
	}
	 private function getHeader($len) {
		 
		// NET|TEST11|RU
		$apiKey = 'MTT-oKn5wuV4gJttCFJh8K9Dr-KJp7Uc';
		$secret = 'j3Mk7BA9WpzKK7E9CjgXYD3XGd1kqYMA';
		
		// NET|TEST66|WW
//		$apiKey = '2Azu32aBlJVZ74m9yoiD6lYslTDTZr5O';
//		$secret = 'r9w1EaxC8XxDSwO7VVe43PcZ2gwOXgGz';
		$signature = hash("sha256", $apiKey.$secret.time());

		$header = array(
			'Api-Key: ' . $apiKey,
			'X-Signature: ' . $signature,
			'X-Time: ' . time(),
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . $len
		);

		return $header;
	 }
	 
	private function cleanUp() {
		 
		$files = glob($this->path. '*', GLOB_MARK | GLOB_NOSORT); // get all file names
		
		foreach($files as $file){ // iterate files
		  if (is_file($file))
			unlink($file); // delete file
		}
	}
}

