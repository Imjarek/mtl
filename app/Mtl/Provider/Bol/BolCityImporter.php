<?php

namespace App\Mtl\Provider\Amadeus;

use App\Mtl\Classes\CityImporter;

use App\Mtl\Classes\ProviderCity;

use App\Mtl\Provider\Bol\BolCitySource;

/* @description
 * По сути мы просто мОкаем геоданные Bedsonline потому что отель содержит лишь строку но не ID города
 * и мы определяем город на лету по координатам (хотя можно и сравнивать названия)
 * т.е. это просто копия стран и городов броневика
 */
class BolCityImporter extends CityImporter {
	static private $provider_id = 5;
	
	static public function start() {
		
		echo "Загрузка городов [BedsOnline]\n";
		
		// загружаем города броневика
		// поскольку городов у BOL нет
		// в дальнейшем необходимо обратиться к документации/api Аmadeus и пересмотреть этот момент
		
		self::process();
	}
	
	static public function process () {
		
		// выполнять этот сырой запрос!
		
		/*
		insert into mtl_provider_city (
			provider_city_id, name_en, name_ru, provider_id, provider_country_id, global_city_id, latitude, longitude, country_code
		)
		select provider_city_id, name_en, name_ru, 5, provider_country_id, global_city_id, latitude, longitude, country_code
		from mtl_provider_city 
		where provider_id = 3;


		-- select * from mtl_provider_country where provider_id = 3;

		insert into mtl_provider_country (
			provider_country_id, name_en, name_ru, country_code, provider_id, global_country_id
		)
		select provider_country_id, name_en, name_ru, country_code, 5, global_country_id
		from mtl_provider_country
		where provider_id = 3;
		 * 
		 */
	}
}

