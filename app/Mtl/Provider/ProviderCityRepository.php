<?php

namespace App\Mtl\Provider;

use App\Mtl\Model\MtlProviderCity;

use App\Mtl\Abstracts\Repository;

class ProviderHotelRepository extends Repository {
	
	/**
     * Specify Model class name
     *
     * @return mixed
     */
	
    function getModel()
    {
        return 'App\Models\MtlProviderCity';
    }
}