<?php

namespace App\Mtl\Provider\Ostrovok;

use App\Models\MtlProviderHotelAmenity;

use App\Models\MtlHotelAmenity;

use App\Models\MtlProviderHotel;

class OstrovokAmenityLoader {
	
	const PROVIDER_ID = 1;
	
	public static function load() {
		
		
		$hotels = MtlProviderHotel::where('provider_id', self::PROVIDER_ID)->get();
		
		foreach ($hotels as $hotel) {
			
			$data = json_decode($hotel['raw_data']);
			
			
			foreach($data->amenities as $amenityGroup) {
				
				foreach ($amenityGroup->amenities as $amenity) {
					
					echo "{$amenity}\n";
					
					// TODO: вынести в репозиторий удобств AmenityRepo::save($props, $providerId)
					
					$props = array('provider_id' => self::PROVIDER_ID, 'name' => $amenity);
					
					$providerHotelAmenity = MtlProviderHotelAmenity::firstOrCreate(['name' => $amenity], $props);
					
					// простая проверка существования удобства по имени
					$amenity = MtlHotelAmenity::firstOrCreate(['name' => $providerHotelAmenity->name], $props);
					
					$providerHotelAmenity->global_amenity_id = $amenity->id;
					
					$providerHotelAmenity->save();
					
				}
			}
		
		}
		
	}
}

