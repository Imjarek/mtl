<?php

namespace App\Mtl\Provider\Ostrovok;

use App\Mtl\Classes\CitySource;

// Класс аналогичен OstrovokCounrySource
// разница только в проверке типа Объекта
// (дублирование)
// 
class OstrovokCitySource extends CitySource {
	
	public static $countries = array('AZ', 'AM', 'BY', 'GE', 'KZ', 'LV', 'LT', 'RU', 'TJ', 'UZ', 'UA', 'AB', 'KG', 'TM', 'MD');
	
	public function getData() {
		
		$filelist = scandir( base_path().'/../../bin/dumps/ostrovok/geodata');
		
		foreach($filelist as $filename) {
			
			$file = file_get_contents( base_path().'/../../bin/dumps/ostrovok/geodata/'.$filename);
			
			
			if (!$file && empty($file)) continue;

			echo "Обработка дампа [ $filename ]\n";
				
			$data = json_decode($file, JSON_OBJECT_AS_ARRAY);

			foreach ($data['result'] as $region) {

				//print_r ($region);
				
				// только СНГ
				if (config('mtl.restrict_import_by_country')) {
					if (array_search($region['country_code'], self::$countries) < 1)
						continue;
				}
				
				if ($region['type'] === 'City') 
					
					yield $region;
				
			}
			
		}
		
	}
}