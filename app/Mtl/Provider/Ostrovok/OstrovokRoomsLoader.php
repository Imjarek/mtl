<?php

namespace App\Mtl\Provider\Ostrovok;

use App\Models\MtlProviderHotelAmenity;

use App\Models\MtlHotelAmenity;

use App\Models\MtlProviderHotel;

use App\Models\MtlProviderHotelRoom;

use App\Models\MtlProviderRate;

use App\Mtl\Repos\HotelRoomRepo;

class OstrovokRoomsLoader {
	
	const PROVIDER_ID = 1;
	
	public static function load() {
		
		$hotelRoomRepo = new HotelRoomRepo;
		$hotels = MtlProviderHotel::where('provider_id', self::PROVIDER_ID)->get();
		
		$url = 'https://partner.ostrovok.ru/api/b2b/v2/hotel/rates';
		
		$user = "1075:467f44c1-8fdd-4819-81d5-3188c064b251";
		
		
		
		// '{"ids":["test_hotel"],"checkin":"2017-03-01","checkout":"2017-03-03","adults":1,"lang":"en","format":"json"}';
		
				
		//var_dump($str);
		foreach ($hotels as $hotel) {
			
			echo "$hotel->provider_hotel_id\n";
			
			$request = self::createRequest($hotel);
			
			$ch = curl_init();  
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
			curl_setopt($ch, CURLOPT_USERPWD, $user);
			//curl_setopt($ch, CURLOPT_HEADER, 0);
			
			$result = curl_exec($ch); 
			curl_close($ch); 
			
			echo $result;
			sleep(1);
			$result = json_decode($result);
			
			//var_dump($result->result->hotels);
			
			if (!count($result->result->hotels))
				continue;
			
			$hotel = $result->result->hotels[0];
			
			foreach($hotel->rates as $rate) {
				
				//var_dump($rate);
				
				echo "{$rate->room_name}\n";
				// нужно проверить есть ли у поставщика такая комната
				
				// добавить обработку cancellation info // bed places //bed_types // paymenttypes и прочее
				$roomData = array(
					'provider_id' => self::PROVIDER_ID,
					'name_ru' => $rate->room_name,
					'provider_room_type_id' => $rate->room_type_id,
					'size' => $rate->room_size
				);
				
				$newRoom = $hotelRoomRepo->create($roomData);
				
				// заюзать репозитарий
				//$rateRepo->create($item);
				
				$newRate = MtlProviderRate::updateOrCreate([
					'provider_id' => self::PROVIDER_ID,
					'provider_rate_id' => $rate->room_type_id, // могут ли быть разные тарифы для одного номер
				   ], [
					'meal'=>$rate->meal,
					'currency' => $rate->rate_currency,
					'b2b_recommended_price' => $rate->b2b_recommended_price,
					'price' => $rate->rate_price,
					'provider_rate_id' => $rate->room_type_id,
					'available_rooms' => $rate->available_rooms,
					'provider_room_id'=>$newRoom->id// у островка нет id
				]);
				
				// ассоциировать обработанный тариф с отелем поставщика
				
			}
			
			sleep (0.2);
		}
	}
	
	private static function createRequest($hotel) {
		
		return json_encode(array(
				"ids" => array($hotel->provider_hotel_id),
				"checkin" => "2017-03-01",
				"checkout" => "2017-03-03",
				"adults" => 1 // "lang":"en","format":"json"
			));
	}
}

