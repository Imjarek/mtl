<?php

namespace App\Mtl\Provider\Ostrovok;

use App\Models\MtlProviderHotelAmenity;

use App\Models\MtlHotelAmenity;

use App\Models\MtlProviderHotel;

use App\Models\MtlProviderHotelRoom;

use App\Mtl\Repos\HotelRoomRepo;

class OstrovokRoomsLoader {
	
	const PROVIDER_ID = 1;
	
	public static function load() {
		
		$hotelRoomRepo = new HotelRoomRepo;
		$hotels = MtlProviderHotel::where('provider_id', self::PROVIDER_ID)->get();
		
		foreach ($hotels as $hotel) {
			
			$data = json_decode($hotel['raw_data']);
			
			foreach($data->room_groups as $group) {
				
				if (isset($group->size)) {
					
					echo "$group->name\n";
					
					$providerHotelRoom = MtlProviderHotelRoom::updateOrCreate([
					    'provider_id' => self::PROVIDER_ID,
					    'provider_room_id' => $group->room_group_id
					],[
					    'name_ru' => $group->name,
					    'room_code' => $group->room_group_id
					]);
					
					if (!$providerHotelRoom->global_room_id)
						$hotelRoomRepo->match($providerHotelRoom);
				}
				
				
			}
		
		}
		
	}
}

