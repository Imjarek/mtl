<?php

namespace App\Mtl\Provider\Ostrovok;

use App\Mtl\Provider\Ostrovok\OstrovokCountrySource;

use App\Mtl\Classes\CountryImporter;

use App\Mtl\Classes\ProviderCountry;

class OstrovokCountryImporter extends CountryImporter {
	
	static private $provider_id = 1;
	static public function start() {
		
		echo "Загрузка городов [Островок]\n";
		
		self::$source = new OstrovokCountrySource;
		
		self::process();
	}
	static public function process () {
		
		foreach (self::$source->getData() as $country) {
			
			echo "{$country['name_ru']}\n";

			ProviderCountry::create($country, self::$provider_id);
			// сохранения страны в репозиторий если еще не существует
		}
	}
}

