<?php

namespace App\Mtl\Provider\Ostrovok;

use App\Mtl\Classes\CityImporter;

use App\Mtl\Classes\ProviderCity;

class OstrovokCityImporter extends CityImporter {
	static private $provider_id = 1;
	
	static public function start() {
		
		echo "Загрузка городов [Островок]\n";
		
		self::$source = new OstrovokCitySource;
		
		self::process();
	}
	
	static public function process () {
		
		foreach (self::$source->getData() as $item) {
			
			echo "{$item['name_en']}\n";
			
			$providerCity = new ProviderCity(self::mapParams($item), self::$provider_id);
			
			$providerCity->add();
		}
	}
	static private function mapParams($item) {
		
		return array (
			'name_ru' => $item['name_ru'],
			'name_en' => $item['name_en'],
			'provider_city_id' => @$item['id'],
			'provider_country_id' => null, /// родительский регион - это область а не страна @$item['parent'],
			'country_code' => $item['country_code'],
			'latitude' => @$item['center']['latitude'],
			'longitude' => @$item['center']['longitude'],
		);
	}
}

