<?php

namespace App\Mtl\Provider\Ostrovok;

use App\Mtl\Classes\HotelImporter;

use Illuminate\Support\Facades\DB;

class OstrovokHotelImporter extends HotelImporter {
	
	const PROVIDER_ID = 1;
	
	static public function start() {
		
		echo "Загрузка данных отелей [Островок]\n";
		
		self::$source = new OstrovokHotelSource;
		
		self::process();
	}
	// TODO: вязаться к интерфейсу репозитория
	
	static function mapParams($item) {
		
		$data = json_decode($item['raw_data']);
		
		return array (
			'provider_city_id' => $item['region_id'],
			'provider_country_id' => self::getProviderCountryId($item['country_code']), // преобразовать 'country_code' в 'provider_id'
			'provider_hotel_id' => $item['id'],
			'name_ru' => $item['name'],
			'name_en' => null,
			'latitude' => $item['latitude'],
			'longitude' => $item['longitude'],
			'provider_id' =>  self::PROVIDER_ID,
			'raw_data' => $item['raw_data'],
			'address' => $data->address,
			'description' => $data->description,
			'phone' => $data->phone,
			'photo' => ($data->images && $data->images[0]) ? $data->images[0]->url : null,
		);
	}
	static function getProviderCountryId ($code) {
	
		$entry = DB::table('mtl_provider_country')
			->where(['country_code' => $code, 'provider_id' => self::PROVIDER_ID])
			->select('provider_country_id')
			->first();
		
		return @$entry->provider_country_id ? $entry->provider_country_id : null;
	}
}