<?php

namespace App\Mtl\Provider\Ostrovok;

class OstrovokHotels {
	
	public function get() {
		
		echo "Начинаем скачивание архива\n";
		$url = 'https://partner.ostrovok.ru/api/affiliate/v2/dump';
		
		$filename = base_path('/../../bin/dumps/ostrovok/full_v2_dump.json.gz');
		
		$cmd = "curl --user '1075:467f44c1-8fdd-4819-81d5-3188c064b251' --data '{\"format\":\"json\",\"type\":\"full_v2.1\",\"lang\":\"en\"}' \"https://partner.ostrovok.ru/api/affiliate/v2/dump\" --output " . $filename;
		
		shell_exec ($cmd);
		
		echo "Архив получен\n";
		
		echo "Начинаем распаковку\n";
		
		$gz = gzopen($filename, 'rb');
		
		if (!$gz) {
			throw new \UnexpectedValueException(
				'Could not open gzip file'
			);
		}
		
		$dst = base_path('/../../bin/dumps/ostrovok/full_v2_dump.json');
		touch($dst);
		
		$dest = fopen($dst, 'wb');
		if (!$dest) {
			gzclose($gz);
			throw new \UnexpectedValueException(
				'Could not open destination file'
			);
		}

		while (!gzeof($gz)) {
			fwrite($dest, gzread($gz, 4096));
		}

		gzclose($gz);
		fclose($dest);

		echo "Распаковка завершена";
	} 
}

