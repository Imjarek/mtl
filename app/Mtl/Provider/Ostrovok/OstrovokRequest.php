<?php

namespace App\Mtl\Provider\Ostrovok;

use App\Models\MtlProviderHotelAmenity;

use App\Models\MtlHotelAmenity;

use App\Models\MtlProviderHotel;

use App\Models\MtlProviderHotelRoom;

use App\Models\MtlProviderRate;

use App\Mtl\Repos\HotelRoomRepo;

class OstrovokRequest {
	
	public $hotelId;
	public $checkIn;
	public $checkOut;
	public $adults;
	public $result;
	
	public function load() {
		
		if (!isset($this->hotelId))
			return 'Не заданы обязательные параметры';

		//	B2B API USER
//		$url = 'https://partner.ostrovok.ru/api/b2b/v2/hotel/rates';
//		$user = "1075:467f44c1-8fdd-4819-81d5-3188c064b251";
		
		// AFFILIATE USER
		$url = 'https://partner.ostrovok.ru/api/affiliate/v2/hotel/rates';
		$user = "1078:f6f00486-293b-4525-b7d2-a94260bc1319";
		// sandbox 1077:9f05fe5d-1f9c-46b1-b82c-3a614b48b891
		
		// '{"ids":["test_hotel"],"checkin":"2017-03-01","checkout":"2017-03-03","adults":1,"lang":"en","format":"json"}';
		
		//var_dump($str);
		
			//echo "$hotel->provider_hotel_id\n";
			
			$request = self::createRequest();
			
			$ch = curl_init();  
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
			curl_setopt($ch, CURLOPT_USERPWD, $user);
			//curl_setopt($ch, CURLOPT_HEADER, 0);
			
			$result = curl_exec($ch); 
			
			curl_close($ch); 
			
			return $result;


			// продолжить отсюда
			// 			sleep(1);
			$result = json_decode($result);
			
			//var_dump($result->result->hotels);
			
			if (!count($result->result->hotels))
				return;
			$hotel = $result->result->hotels[0];

			foreach($hotel->rates as $rate) {
				
				// далее идет запись в таблицу это должно быть в верхнеуровневом классе
				
				echo "{$rate->room_name}\n";
				// нужно проверить есть ли у поставщика такая комната
				
				// добавить обработку cancellation info // bed places //bed_types // paymenttypes и прочее
				$roomData = array(
					'provider_id' => self::PROVIDER_ID,
					'name_ru' => $rate->room_name,
					'provider_room_type_id' => $rate->room_type_id,
					'size' => $rate->room_size
				);
				
				$newRoom = $hotelRoomRepo->create($roomData);
				
				$newRate = MtlProviderRate::updateOrCreate([
					'provider_id' => self::PROVIDER_ID,
					'provider_rate_id' => $rate->room_type_id, // могут ли быть разные тарифы для одного номер
				   ], [
					'meal'=>$rate->meal,
					'currency' => $rate->rate_currency,
					'b2b_recommended_price' => $rate->b2b_recommended_price,
					'price' => $rate->rate_price,
					'provider_rate_id' => $rate->room_type_id,
					'available_rooms' => $rate->available_rooms,
					'provider_room_id'=>$newRoom->id// у островка нет id
				]);
				
				// ассоциировать обработанный тариф с отелем поставщика
				
			}
	}
	
	private function createRequest() {
		
		return json_encode(array(
				"ids" => array($this->hotelId),
				"checkin" => $this->checkIn,
				"checkout" => $this->checkOut,
				"adults" => $this->adults
				// "lang":"en","format":"json"
			));
	}
}

