<?php

namespace App\Mtl\Provider\Ostrovok;

use App\Mtl\Classes\CountrySource;

class OstrovokCountrySource extends CountrySource {
	
	public function getData() {
		
		$filelist = scandir( base_path().'/../../bin/dumps/ostrovok/geodata');
	
		foreach($filelist as $filename) {

			$file = file_get_contents( base_path().'/../../bin/dumps/ostrovok/geodata/'.$filename);
			if (!$file && empty($file)) continue;

			echo "Обработка дампа [ $filename ]\n";
				
			echo substr($file, 0, 100). "\n";
			$data = json_decode($file, JSON_OBJECT_AS_ARRAY);

			foreach ($data['result'] as $region) {

				//print_r ($region);

				if ($region['type'] === 'Country')
					yield $region;
			}
			
		}
		
	}
}
