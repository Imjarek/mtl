<?php

namespace App\Mtl\Provider\Ostrovok;
use App\Mtl\Abstracts\Source;

class OstrovokHotelSource extends Source {
	
	private $file;
	public function __construct() {
		
		$this->file = base_path().'/../../bin/dumps/ostrovok/full_v2_dump.json';
	}
	public function getData () {
		
		$handle = fopen($this->file, "r") or die("not found");
	
		if ($handle) {
			while (!feof($handle)) {
				$str = fgets($handle);
				$str = $this->trimStr($str);
				
				$arr = json_decode($str, JSON_OBJECT_AS_ARRAY);
				
				// пророссийский фильтр и фильтр расположения возле точки
				if ($arr['country'] !== 'Россия')
					continue;
				
				$arr['raw_data'] = $str;
				yield $arr;
			}

		fclose($handle);
		}
	
	}
	private function trimStr($str) {
		
		if (substr($str, 0, 1) === '[' or substr($str, 0, 1) === ',')
					$str = substr($str, 1);

				if (substr($str, strlen($str)) === ']')
					$str = substr($str, 0, strlen($str) - 1);
		
		return $str;
	}
	
}
