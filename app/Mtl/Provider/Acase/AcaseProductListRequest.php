<?php

namespace App\Mtl\Provider\Acase;

class AcaseRequest {
	
	public $buyer = "ibcentr";
	public	$user = "xml";
	public 	$pass = "349638";
	public $method = "HotelProductRequest";
	public $request;
	
	public function send() {
		
		$ch = curl_init("http://www.acase.ru/xml/form.jsp");
		
		$proxy = '89.17.39.2:3129';
		
		if ($proxy) {
			curl_setopt($ch, CURLOPT_PROXY, $proxy);
			//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, TRUE);
		}
		
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		//curl_setopt($ch, CURLOPT_HEADER, TRUE);
		//curl_setopt($ch, CURLOPT_VERBOSE, TRUE); 

		// готовим xml
		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array('Content-Type: application/x-www-form-urlencoded; charset=utf-8', 
			      'Content-Length: '.strlen($this->request)
		));
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->request);
		
		$result = curl_exec($ch);
		
		$error = curl_error($ch);
		
		curl_close($ch);
		
		return $result;
	}
	public function get(){
		
	}
	// во избежаннии путаницы задать соответствием этих методов метода acase gateway
	
	public function make ($fnc) {
		
		$xml = call_user_func(array($this, 'hotelsListRequest'));
		
		$this->request = "RequestName=$this->method&XML=" . $xml;
	}
	public function hotelsListRequest() {
		return <<<xml
<?xml version="1.0" encoding="UTF-8"?><HotelListRequest BuyerId="$this->buyer" UserId="$this->user" Password="$this->pass" Language="ru"/>
xml;
	}
	public function roomsRequest() {
		
		return <<<xml
<?xml version="1.0" encoding="UTF-8"?><HotelProductRequest BuyerId="$this->buyer" UserId="$this->user" Password="$this->pass" Language="ru" Currency="2" WhereToPay="3" Hotel="800300" ArrivalDate="$this->check_in_date" DepartureDate="$this->check_out_date" Id="" AccommodationId="" NumberOfGuests="2" NumberOfExtraBedsAdult=""/>
xml;
	
	}
}

