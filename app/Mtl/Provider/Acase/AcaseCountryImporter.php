<?php

namespace App\Mtl\Provider\Acase;

use App\Mtl\Classes\CountryImporter;

use App\Mtl\Classes\ProviderCountry;

class AcaseCountryImporter extends CountryImporter {
	
	static private $provider_id = 2;
	
	static public function start() {
		
		echo "Загрузка городов [Академсервис]\n";
		
		self::$source = new AcaseCountrySource;
		
		self::process();
	}
	static public function process () {
		
		foreach (self::$source->getData() as $country) {
			
			echo "{$country['name_ru']}\n";
			
			ProviderCountry::create($country, self::$provider_id);
			// сохранения страны в репозиторий если еще не существует
		}
	}
}