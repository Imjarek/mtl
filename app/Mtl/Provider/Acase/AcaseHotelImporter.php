<?php

namespace App\Mtl\Provider\Acase;

use App\Mtl\Classes\HotelImporter;


class AcaseHotelImporter extends HotelImporter {
	
	const PROVIDER_ID = 2;
	
	static public function start() {
		
		echo "Загрузка данных отелей [Академсервис]\n";
		
		self::$source = new AcaseHotelSource;
		
		self::process();
	}
	
	static function mapParams($item) {
		
		$data = simplexml_load_string("<?xml version='1.0'?>".$item['raw_data']);
		
		$item['provider_id'] = self::PROVIDER_ID;
		$item['address'] = $data['Address'];
		$item['description'] = $data['Description'];
		$item['photo'] = $data['Url'];
		$item['phone'] = $data['Phone'];
		
		return $item;
	}
	
}
