<?php

namespace App\Mtl\Provider\Acase;

use App\Models\MtlProviderHotelAmenity;

use App\Models\MtlHotelAmenity;

use App\Models\MtlProviderHotel;

class AcaseAmenityLoader {
	
	const PROVIDER_ID = 2;
	
	public static function load() {
		
		
		$xml = simplexml_load_file(base_path().'/../../bin/dumps/acase/HotelAmenityListRequestRU.xml');
	
		//dd($xml);
		// нужно обрабатывать и англ. и русский xml
		foreach($xml->HotelAmenity as $amenity) {
			
					echo "{$amenity['Name']}\n";
//					
//					// TODO: вынести в репозиторий удобств AmenityRepo::save($props, $providerId)
//					
					$props = array(
					    'provider_id' => self::PROVIDER_ID,
					    'name' => $amenity['Name'],
					    'provider_hotel_amenity_id' => $amenity['Code']);
//					
					$providerHotelAmenity = MtlProviderHotelAmenity::firstOrCreate(['name' => $amenity['Name']], $props);
//					
//					// простая проверка существования удобства по имени
					$amenity = MtlHotelAmenity::firstOrCreate(['name' => $providerHotelAmenity->name], $props);
//					
					$providerHotelAmenity->global_amenity_id = $amenity->id;
//					
					$providerHotelAmenity->save();
//					
		
		}
		
	}
}

