<?php

namespace App\Mtl\Provider\Acase;

use App\Models\MtlProviderHotelAmenity;

use App\Models\MtlHotelAmenity;

use App\Models\MtlProviderHotel;

class AcaseAmenityImporter {
	
	const PROVIDER_ID = 2;
	
	public static function load() {
		
		
		$hotels = MtlProviderHotel::where('provider_id', self::PROVIDER_ID)->get();
		
		foreach ($hotels as $hotel) {
			
			$data = simplexml_load_string("<?xml version='1.0'?>".$hotel['raw_data']);
			
			
			foreach($data->Amenities as $item) {
				
				
				foreach ($item as $amenity) {
					
					echo "{$amenity}\n";
					
					dd($amenity);
					// TODO: вынести в репозиторий удобств AmenityRepo::save($props, $providerId)
					
					$props = array('provider_id' => self::PROVIDER_ID, 'name' => $amenity);
					
					$providerHotelAmenity = MtlProviderHotelAmenity::firstOrCreate(['name' => $amenity], $props);
					
					// простая проверка существования удобства по имени
					$amenity = MtlHotelAmenity::firstOrCreate(['name' => $providerHotelAmenity->name], $props);
					
					$providerHotelAmenity->global_amenity_id = $amenity->id;
					
					$providerHotelAmenity->save();
					
				}
			}
		
		}
		
	}
}

