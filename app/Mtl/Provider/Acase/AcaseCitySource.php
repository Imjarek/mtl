<?php

namespace App\Mtl\Provider\Acase;

use App\Mtl\Classes\CitySource;

class AcaseCitySource extends CitySource {
	
	public function getData() {
		
		$xml = simplexml_load_file(base_path().'/../../bin/dumps/acase/CityListRequestRU.xml');
	
		// нужно обрабатывать и англ. и русский xml
		foreach($xml->Country as $country) {

			foreach($country->City as $city) {
				
				$item['name_ru'] = (string)$city['Name'];
				$item['provider_city_id'] = (string)$city['Code'];
				$item['provider_country_id'] = (string)$country['Code'];
				$item['country_code'] = null; // нужно получить код страны
				$item['latitude'] = (float)$city->Position['Latitude'];
				$item['longitude'] = (float)$city->Position['Longitude'];
				
				yield $item;
			}
		}
	}
}