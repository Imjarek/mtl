<?php

namespace App\Mtl\Provider\Acase;
use App\Mtl\Classes\HotelSource;

class AcaseHotelSource extends HotelSource {
	
	private $file;
	public function __construct() {
		
		$this->file = base_path('/../../bin/dumps/acase/HotelListRequestRU.xml');
	}
	public function getData () {
		
		$xml = simplexml_load_file($this->file);
		
		foreach($xml->Country as $country) {
			foreach($country->City as $city) {

				foreach($city->Hotel as $hotel) {
					
					//var_dump($hotel);
					$item = array(
						'provider_hotel_id' => $hotel['Code'],
						'name_ru' => $hotel['Name'],
						'latitude' => $hotel->Position['Latitude'],
						'longitude' => $hotel->Position['Longitude'],
						'provider_city_id' => $city['Code'],
						'provider_country_id' => $country['Code'],
						'raw_data' => $hotel->asXML()
					);
					
					yield($item);
				}
			}
		}
	
	}
}
