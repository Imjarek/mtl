<?php

namespace App\Mtl\Provider\Acase;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Schema;

class ActualizeAcaseHotels {
	
	const PROVIDER_ID = 2;
	
	static public function start() {
		
		echo "Актуализация отелей Академии\n";
		
		Schema::dropIfExists('temp_ids');
			
		Schema::create('temp_ids', function ($table) {
				$table->string('id');
		});

		$source = new AcaseHotelSource;
		
		foreach ($source->getData() as $data ) {
			
			$id = $data['provider_hotel_id'];
			//echo ("$id ");
			
			DB::insert("insert into temp_ids (id) values (?)", [$id]);
		}
		
		if (DB::table('temp_ids')->count() > 0)
			
			DB::update("update mtl_provider_hotel
				set not_exists = 1
				where provider_id = ?
					and provider_hotel_id not in (select id from temp_ids)", [self::PROVIDER_ID]);
		
		// прибираемся
		Schema::drop('temp_ids');
		
		echo "Выполнена актуализация отелей Академии\n";
	}
}