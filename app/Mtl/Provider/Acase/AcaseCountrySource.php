<?php

namespace App\Mtl\Provider\Acase;

use App\Mtl\Classes\CountrySource;

class AcaseCountrySource extends CountrySource {
	
	public function getData() {
		
		$xml = simplexml_load_file(base_path().'/../../bin/dumps/acase/CountryListRequestRU.xml');

		foreach($xml->Country as $country) {
			
			// Преобразовываем в массив
			//$item['latitude'] 
			$item['name_ru'] = (string)$country['Name'];
			$item['id'] = (string)$country['Code'];
			
			yield $item;
		}
	}
}
