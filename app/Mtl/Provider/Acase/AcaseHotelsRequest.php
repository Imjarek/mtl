<?php

namespace App\Mtl\Provider\Acase;

/*
 * Для обращений к Академии здесь используется простой запрос с передачей параметров строкой
 * 
 * Для многих других запросов (PriceRequest) используйте класс AcaseHotel
 */
class AcaseHotelsRequest {
	
//	public $buyer = "ibcentr"; // подходят для указания внутри xml
//	public	$user = "xml";
//	public 	$pass = "349638";
	
//	public $buyer = "bti2";
//	public	$user = "rodionova";
//	public 	$pass = "1";
	
// 'BTI2';
//'XML';
//'RTH34GGF';
//	
	
	public $buyer = "BTI2"; // подходят для получения данных без xml
	public $user = "XML1";
	public $pass = "RTH34GGF";
	public $method = "HotelProductRequest";
	public $request;
	
	public function load() {
		
		$url = "http://www.acase.ru/xml/form.jsp";
		
		///$url = $url . '?' . $this->requestString();

		$ch = curl_init($url);
		
		$proxy = '89.17.39.2:3129';
		
		if (isset($proxy)) {
			curl_setopt($ch, CURLOPT_PROXY, $proxy);
			//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, TRUE);
		}
		
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		
		curl_setopt($ch, CURLOPT_POST, count($this->params()));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->params());
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		//curl_setopt($ch, CURLOPT_HEADER, TRUE);
		//curl_setopt($ch, CURLOPT_VERBOSE, TRUE); 

		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array(
				'Content-Type: application/x-www-form-urlencoded', 
			    'Content-Length: '.strlen(json_encode($this->params()))
			)
		);
		
		var_dump($this->params());
		
		$result = curl_exec($ch);
		
		$error = curl_error($ch);
		
		curl_close($ch);
		
		return $result;
	}
	public function requestString() {
		return http_build_query($this->params());
	}
	public function params() {
		
		return
			array(
				'CompanyId' => $this->buyer,
				'UserId' => $this->user,
				'Password' => $this->pass,
				'CityCode' => 123
			);
	}
}

