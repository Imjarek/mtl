<?php

namespace App\Mtl\Provider\Acase;

use App\Mtl\Classes\CityImporter;

use App\Mtl\Classes\ProviderCity;

class AcaseCityImporter extends CityImporter {
	
	private static $provider_id = 2;
	
	static public function start() {
		
		echo "Загрузка городов [АкадемСервис]\n";
		
		self::$source = new AcaseCitySource;
		
		self::process();
	}
	
	static public function process () {
		
		foreach (self::$source->getData() as $item) {
			
			echo "{$item['name_ru']}\n";
			
			$city = $item;
			$city['name_en'] = null; // дописать источник чтобы получать английские имена (они в отдельном xml)
			$providerCity = new ProviderCity($city, self::$provider_id);
			
			$providerCity->add();
		}
	}
}

