<?php

namespace App\Mtl\Provider\Bronevik;


use App\Mtl\Classes\HotelImporter;
use App\Mtl\Classes\ProviderHotel;

class BronevikHotelImporter extends HotelImporter {
	
	const PROVIDER_ID = 3;
	
	public function __construct() {

	}
	static public function start() {
		
		echo "Загрузка данных отелей [Броневик]\n";
		
		self::$source = new BronevikHotelSource;
		
		self::process();
	}
	static function mapParams($item) {
		
		return array (
			
			'name_ru' => $item['name'],
			'latitude' => $item['latitude'],
			'longitude' => $item['longitude'],
			'provider_hotel_id' => $item['id'], //
			'provider_city_id' => $item['city'],
			'provider_country_id' => $item['country'],
			'address' => $item['address'],
			'phone' => $item['phone'],
			'photo' => $item['photo'],
			'description' => $item['description'],
			'provider_id' =>  self::PROVIDER_ID,
			'raw_data' => json_encode($item, JSON_PRETTY_PRINT)
		);
	}
	
}
