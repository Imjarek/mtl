<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use \App\Mtl\Provider\Bronevik\BronevikHotelImporter;

class LoadBronevikHotels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mtl:loadBroneviklHotels';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Загрузка данных отелей Броневика';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
   
		BronevikHotelImporter::start();
	}
		
}