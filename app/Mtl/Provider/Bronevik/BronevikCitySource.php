<?php

namespace App\Mtl\Provider\Bronevik;

use App\Mtl\Abstracts\Source;

class BronevikCitySource extends Source {
	
	public function getData() {
		
		$file = file_get_contents( base_path().'/../../bin/dumps/bronevik/bvk_cities.json');

		echo "Обработка json  с городами Броневика\n";

		$data = json_decode($file, JSON_OBJECT_AS_ARRAY);

		foreach ($data as $city) {

			// пророссийский фильтр
//			if ($city['country_code'] !== 'RU')
//				continue;
			$city['provider_country_id'] = $city['country_id'];
			
			yield $city;
		}
	}
}