<?php

namespace App\Mtl\Provider\Bronevik;

use App\Mtl\Classes\CountrySource;

class BronevikCountrySource extends CountrySource {
	
	public function getData() {
		
			$file = file_get_contents( base_path().'/../../bin/dumps/bronevik/bvk_countries.json');

			echo "Обработка json со странами Броневика \n";
				
			$data = json_decode($file, JSON_OBJECT_AS_ARRAY);

			foreach ($data as $country) {

				yield $country;
			}
			
	}
}
