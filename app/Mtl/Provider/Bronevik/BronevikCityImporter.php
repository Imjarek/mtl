<?php

namespace App\Mtl\Provider\Bronevik;

use App\Mtl\Classes\CityImporter;

use App\Mtl\Classes\ProviderCity;

class BronevikCityImporter extends CityImporter {
	static private $provider_id = 3;
	
	static public function start() {
		
		echo "Загрузка городов [Броневик]\n";
		
		self::$source = new BronevikCitySource;
		
		self::process();
	}
	
	static public function process () {
		
		foreach (self::$source->getData() as $city) {
			
			echo "{$city['name_en']}\n";
			
			$city['provider_city_id'] = $city['id'];
			$city['provider_country_id'] = $city['country_id'];
			
			$providerCity = new ProviderCity($city, self::$provider_id);
			
			$providerCity->add();
		}
	}
}

