<?php

namespace App\Mtl\Provider\Bronevik;

use App\Models\MtlProviderHotel;

class BronevikRequest {
	
	public $hotelId;
	public $checkIn;
	public $checkOut;
	public $adults;
	public $result;
	
	public function load() {
		
		if (!isset($this->hotelId))
			return 'Не заданы обязательные параметры';

		$wsdl = 'https://xml.bronevik.com/soap/v2.0/ws_2.0.0.wsdl';

		//  9058, 4333, 2739, 3718, 2737
		$client = new \SoapClient($wsdl, [
			// 'location' => 'https://dev-xml.bronevik.com/soap/v2.0/ws_2.0.0.php',
			'location' => 'https://dev-xml.bronevik.com/soap/v2.0/ws_2.0.0.php',
			'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
		]);

		$result = $client->SearchHotelOffers([
			'credentials' => [
				'clientKey' => '123456',
				'login' => 'test',
				'password' => 'test',
			],
			'language' => 'ru',
			'currency' => 'RUB',
			'hotelId' => $this->hotelId,
			'arrivalDate' => $this->checkIn,
			'departureDate' => $this->checkOut,
		]);
			
		return json_encode($result);

	}
	private function createRequest() {
		
		return json_encode(array(
				"ids" => array($this->hotelId),
				"checkin" => $this->checkIn,
				"checkout" => $this->checkOut,
				"adults" => $this->adults
				// "lang":"en","format":"json"
			));
	}
}

