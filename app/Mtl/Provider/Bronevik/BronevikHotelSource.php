<?php

namespace App\Mtl\Provider\Bronevik;
use App\Mtl\Classes\HotelSource;

class BronevikHotelSource extends HotelSource {
	
	private $file;
	public function __construct() {
		
		$this->file = base_path('../../bin/dumps/bronevik/bvk_hotels.json');
	}
	public function getData () {
		
		$json = file_get_contents($this->file);
		
		$data = json_decode($json, true);
		
		foreach($data as $item) {
			
			yield $item;
		}
	}
}
