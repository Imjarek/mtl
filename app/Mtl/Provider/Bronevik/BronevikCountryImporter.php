<?php

namespace App\Mtl\Provider\Bronevik;

use App\Mtl\Classes\CountryImporter;

use App\Mtl\Classes\ProviderCountry;

class BronevikCountryImporter extends CountryImporter {
	
	static private $provider_id = 3;
	
	static public function start() {
		
		echo "Загрузка стран [Броневик]\n";
		
		self::$source = new BronevikCountrySource;
		
		self::process();
	}
	
	static public function process () {
		
		foreach (self::$source->getData() as $country) {
			
			echo "{$country['name_en']}\n";
			
			//print_r ($country);
			
			ProviderCountry::create($country, self::$provider_id);
			// сохранения страны в репозиторий если еще не существует
		}
	}
}

