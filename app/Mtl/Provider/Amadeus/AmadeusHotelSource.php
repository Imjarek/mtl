<?php

namespace App\Mtl\Provider\Amadeus;
use App\Mtl\Classes\HotelSource;

use \PDO;

class AmadeusHotelSource extends HotelSource {
	
	private $file;
	public function __construct() {
		
	}
	public function getData () {
		
		$dsn = "mysql:host=localhost; dbname=supplier; charset=UTF8";
		
		$user = 'exys';
		$pass = '6996';
		
		$opt = array(
		    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
		    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
		);
		$pdo = new PDO($dsn, $user, $pass, $opt);
		
		// запрос возвращает отели по USSR в соответствии с геоданными БВК
		$stm = $pdo->query("select c.name, cn.name, cn.iso3_alpha2 as country_code, h.*, d.*
								from amadeus_hotel h
								join amadeus_hotel_data d
									on h.id =  d.id
								left join bvk_cities.citys as c
									on h.bvk_city_id = c.id
								left join bvk_cities.countrys as cn
									on cn.id = c.country
								where iso3_alpha2 in ('AZ', 'AM', 'BY', 'BG', 'GE', 'KZ', 'LV',
									'LT', 'RU', 'TJ', 'UZ', 'UA', 'AB', 'KG', 'TM', 'MD')
								-- limit 100; -- ОГРАНИЧЕНИЕ УБРАТЬ!!!
		");
		
		while($item = $stm->fetch()) {
			
			yield $item;
		}
	}
}
