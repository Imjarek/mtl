<?php

namespace App\Mtl\Provider\Amadeus;

use App\Mtl\Classes\CountryImporter;

use App\Mtl\Classes\ProviderCountry;

use App\Mtl\Provider\Bronevik\BronevikCountrySource;

class AmadeusCountryImporter extends CountryImporter {
	
	static private $provider_id = 4;
	
	static public function start() {
		
		echo "Загрузка стран [Амадеус]\n";
		
		// загружаем страны броневика
		// поскольку своих стран у амадеуса нет
		// в дальнейшем необходимо обратиться к документации/api Аmadeus и пересмотреть этот момент
		
		self::$source = new BronevikCountrySource;
		
		self::process();
	}
	
	static public function process () {
		
		foreach (self::$source->getData() as $country) {
			
			echo "{$country['name_en']}\n";
			
			//print_r ($country);
			
			ProviderCountry::create($country, self::$provider_id);
			// сохранения страны в репозиторий если еще не существует
		}
	}
}

