<?php

namespace App\Mtl\Provider\Amadeus;


use App\Mtl\Classes\HotelImporter;
use App\Mtl\Classes\ProviderHotel;

class AmadeusHotelImporter extends HotelImporter {
	
	const PROVIDER_ID = 4;
	
	public function __construct() {

	}
	static public function start() {
		
		echo "Загрузка данных отелей [Амадеус]\n";
		
		self::$source = new AmadeusHotelSource;
		
		self::process();
	}
	static function mapParams($item) {
		
		$item['name_ru'] = $item['name'];
		
		$data = self::parseAmadeusXml($item['data']);
		
		return array (
			
			'name_ru' => self::normChars($item['name']),
			'latitude' => $item['latitude'],
			'longitude' => $item['longitude'],
			'provider_hotel_id' => $item['code'], //
			'provider_city_id' => $item['bvk_city_id'],
			'address' => self::normChars($item['address']),
			'phone' => $data['phone'],
			'description' => $data['description'],
			'photo' => $data['photo'],
			'provider_id' =>  self::PROVIDER_ID,
			'raw_data' => $item['data'],
		);
	}
	
	static function normChars ($str) {
		return ucwords(strtolower($str));
	}
	/*
	 * Собирает данные из "жутковатого" xml Amadeus
	 */
	static private function parseAmadeusXml($xml) {
		$result = array();
		
		$data = simplexml_load_string($xml);
		
		// вот такие вот веселые иксемельки приходят от дяди Амадея) - Без жесткого тихого подаления ошибок не обойтись
		
		// если не прокатят индексы придется выискивать инфу по атрибуту infocode
		// пример $xml->xpath('name[@id="minwage"]/full_image')[0];
		// описание
		$descriptions = @$data->HotelDescriptiveContents->HotelDescriptiveContent
			->HotelInfo->Descriptions
			->MultimediaDescriptions->MultimediaDescription;
		
		$str = '';
		$i = 0;
		while($item = $descriptions[$i]) {
			$description = @$item->TextItems->TextItem->Description;
			if (isset($description)) {
				if ($str === '')
					$str = self::collapseTextObject($description);
				else 
					$str .= '. ' . self::collapseTextObject($description);
			}
			$i++;
		}
			
		$result['description'] = $str;
		
		// фото
		
		$result['photo'] = (string)@$data->HotelDescriptiveContents->HotelDescriptiveContent
			->HotelInfo->Descriptions
			->MultimediaDescriptions->MultimediaDescription[2]->ImageItems->ImageItem->ImageFormat[0]->URL;
		
		$result['phone'] = (string)@$data->HotelDescriptiveContents->HotelDescriptiveContent
			->ContactInfos->ContactInfo->Phones->Phone[0]['PhoneNumber'];
		
		//dd($result);
		return $result;
	}
	/*
	 * Конвертер тескт из объекта (для Академ-данных)
	 */
	private static function collapseTextObject($obj, $limit = 120) {
		
		$txt = '';
		for ($i = 0 ; $i < $limit; $i++) {
			$txt .= $obj->$i.' ';
		}
		return $txt;
	}
	
}
