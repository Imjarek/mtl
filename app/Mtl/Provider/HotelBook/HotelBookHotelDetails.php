<?php

namespace App\Mtl\Provider\HotelBook;

use App\Models\MtlProviderHotel;

class HotelBookHotelDetails {
	
	// детализация отелей
	// обходим список ID отелей и сохраняем xml с данными каждого отеля
	public $request;
	
	private $ids;
	const PROVIDER_ID = 6;
	
	public function __construct() {
		$this->ids = MtlProviderHotel::where([
			'provider_id' => self::PROVIDER_ID
		])->pluck('provider_hotel_id');
	}
	function get() {

		foreach ($this->ids as $hotelId) {
			
			// можно и не создавать для каждого запроса объект, но тогда нужно будет обновлять время и checksum
			// учитывая скорость ответа можно не заморачиваться
			
			$this->request = new HotelBookRequest;
		
			$this->request->method = 'hotel_detail';

			$this->request->params = $this->request->params();
		
			$this->request->params['hotel_id'] = $hotelId;
			$this->request->params['language'] = 'ru';
			
			$path = base_path('../../bin/dumps/hotelbook/hotels/details');

			$this->request->filename = $path . '/HotelDetails_' . $hotelId;
			$this->request->response = $this->request->send();

			$this->request->save();
			
			unset($this->request); // см. коммент выше
		}
			
	}
}
