<?php

namespace App\Mtl\Provider\HotelBook;

class HotelBookRequest {
	
	//protected $url = 'https://www.hotelbook.pro/xml'; // живой шлюз
	protected $url = 'http://test1.hotelbook.pro/xml'; // тестовый шлюз
	
	protected $user = "raketa";
	protected $pass = "fhtrav1";
	public $method = "hotel_search";
	
	public $hotelId;
	public $cityId;
	public $checkIn;
	public $checkOut;
	public $adults;
	public $children;
	public $rooms;
	public $result;
	
	//public $xmlMethod = "HotelListRequest";
	public $request;
	
	public function save() {
		
		echo "сохранено : " . (strlen($this->response)) . " байт\n";
		file_put_contents($this->filename, $this->response);
	}
	public function load() {
		
		$ch = curl_init();
		
		$url = $this->url.'/'. $this->method . '?'. $this->requestString();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('request' => $this->requestXML()));
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		//curl_setopt($ch, CURLOPT_HEADER, TRUE);
		//curl_setopt($ch, CURLOPT_VERBOSE, TRUE); 

		// готовим xml
		
//		curl_setopt($ch, CURLOPT_HTTPHEADER, 
//			array(
//				'Content-Type: application/xml; charset=utf-8', 
//			    'Content-Length: '.strlen($this->requestString())
//			)
//		);
		
		$result = curl_exec($ch);
		
		$error = curl_error($ch);
		
		curl_close($ch);
		
		return $result;
	}
	public function requestString() {
		return http_build_query($this->params());
	}
	public function params() {
		
		$time = time();
		
		return
			array(
				'login' => $this->user,
				'password' => $this->pass,
				'time' => $time,
				'checksum' => md5(md5($this->pass).$time),
			);
	}
	public function requestXML() {
		return <<<xml
			<?xml version="1.0" encoding="UTF-8"?>
<HotelSearchRequest>
	<Request
       cityId="{$this->cityId}"
		hotelId="{$this->hotelId}"
		checkIn="{$this->checkIn}"
		duration="{$this->duration}"/>
	<Rooms>
		<Room roomNumber="{$this->rooms}" adults="{$this->adults}">
		</Room>
	</Rooms>
</HotelSearchRequest>
xml;
	}
}
