<?php

namespace App\Mtl\Provider\HotelBook;

use App\Mtl\Classes\CountryImporter;

use App\Mtl\Classes\ProviderCountry;

class HotelBookCountryImporter extends CountryImporter {
	
	static private $provider_id = 6;
	
	static public function start() {
		
		echo "Загрузка cтран [Хотелбук]\n";
		
		self::$source = new HotelBookCountrySource;
		
		self::process();
	}
	static public function process () {
		
		foreach (self::$source->getData() as $country) {
			
			echo "{$country['name_ru']}\n";
			
			ProviderCountry::create($country, self::$provider_id);
			// сохранения страны в репозиторий если еще не существует
		}
	}
}