<?php

namespace App\Mtl\Provider\HotelBook;

class HotelBookGeodataRequest extends HotelBookRequest {
	
	public $method = "cities";
	//public $xmlMethod = "HotelListRequest";
	public $request;
	
	public function get() {
		$this->params = $this->params();
		$this->response = $this->send();
	}
	public function params() {
		
		$time = time();
		
		return
			array(
				'login' => $this->user,
				'password' => $this->pass,
				'time' => $time,
				'checksum' => md5(md5($this->pass).$time)
			);
	}
}

