<?php

namespace App\Mtl\Provider\HotelBook;

use App\Mtl\Classes\CountrySource;

class HotelBookCountrySource extends CountrySource {
	
	public function getData() {
		
		$xml = simplexml_load_file(base_path().'/../../bin/dumps/hotelbook/CountriesResponse.xml');

		foreach($xml->Countries->Country as $country) {
			
			$item['name_ru'] = (string)$country;
			$item['id'] = (string)$country['id'];
			
			yield $item;
		}
	}
}
