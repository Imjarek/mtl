<?php

namespace App\Mtl\Provider\HotelBook;

use App\Models\MtlProviderHotel;

class LoadHotelBookHotelData {
	
	private $dir;
	
	const PROVIDER_ID = 6;
	
	public function __construct() {
		
		$this->dir = base_path('../../bin/dumps/hotelbook/hotels/details');
	}
	public function import() {
		
		$filelist = scandir($this->dir);
		
		foreach($filelist as $filename) {
			
			$file = file_get_contents( $this->dir . '/' . $filename);

			if (!$file && empty($file)) continue;
				
			$data = simplexml_load_string($file);
			
			if (!isset($data->HotelDetail))
				continue;
			
			$hotel = $data->HotelDetail;
			
			$hotel->raw_data = $hotel->asXML();
			
			echo "{$hotel['name']}\n";
			// пишем полученные данные в отель
			$this->update($hotel);
		}
	}
	
	private function update($data) {
		
		$hotel = MtlProviderHotel::where([
			'provider_id' => self::PROVIDER_ID,
			'provider_hotel_id' => (integer)$data['id']
		])->first();
		
		if ($hotel) {
				
			$hotel->address = $data->Address;
			$hotel->description = isset($data->Description) ? $data->Description : null;
			
			$hotel->phone = isset($data->Phone) ? $data->Phone : null;
			$hotel->email = isset($data->Email) ? $data->Email : null;
			$hotel->url = isset($data->WWW) ? $data->WWW : null;
			// ниче не забыли?
			
//			if (isset($hotel->distances))
//				$hotel->description = $hotel->description ? $hotel->description . '. ' . $hotel->distances : $hotel->distances;
			
			$hotel->raw_data = $data->raw_data;
			$hotel->save();
		}
			
	}
}
