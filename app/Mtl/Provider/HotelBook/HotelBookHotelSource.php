<?php

namespace App\Mtl\Provider\HotelBook;
use App\Mtl\Classes\HotelSource;

use Illuminate\Support\Facades\DB;


class HotelBookHotelSource extends HotelSource {
	
	private $dir;
	
	private $cids;
	
	public static $countries = array(3, 7, 13, 16, 39, 59, 67, 75, 78, 118, 139, 147, 150, 151, 245, 91);
	
	public function __construct() {
		
		$this->dir = base_path('../../bin/dumps/hotelbook/hotels');
		
		if (config('mtl.restrict_import_by_country'))
			$this->cids = $this->getCityIDs();
	}
	public function getData() {
		
		$filelist = scandir($this->dir);
		
		foreach($filelist as $filename) {
			
			$file = file_get_contents( $this->dir . '/' . $filename);

			if (!$file && empty($file)) continue;

			echo "Обработка дампа [ $filename ]\n";
				
			$xml = simplexml_load_string($file);
			
			if (count((integer)$xml->HotelList['totalHotels']) > 0)
				foreach ($xml->HotelList->Hotel as $hotel) {
					
					// ограничение по странам (СССР)
					if (config('mtl.restrict_import_by_country')) {
						if (!$this->cityToImport($hotel['city'], $this->cids))
							continue;
					}
					
					// пока загружаем только отели с координатами
					if (!$hotel['hotelLatitude']
						|| !$hotel['hotelLongitude']
						|| $hotel['hotelLatitude'] == 0
						|| $hotel['hotelLatitude'] == 0)
						continue;
					
					$hotel['raw_data'] = $hotel->asXML();
					yield $hotel;
				}
		}
	}
	
	private static function cityToImport($cc, $cids) {
		
		if (array_search($cc, $cids) > 0)
			return true;
	}
	private function getCityIds() {
		
		$ids = array();
		// получаем айди городов интересующих нас стран, по странам отфильтровать нет возможности
		$res = DB::select('select pc.provider_city_id as id -- , pc.name_ru, pcn.provider_country_id, pcn.name_ru
			from mtl_provider_city pc
			join  mtl_provider_country pcn
			on pc.provider_country_id = pcn.provider_country_id and pcn.provider_id = 6
			where pc.provider_id = 6
			and pcn.provider_country_id in (3, 7, 13, 16, 39, 59, 67, 75, 78, 118, 139, 147, 150, 151, 245, 91)
			;');
		
		foreach ($res as $row) {
			$ids[] = $row->id;
		}
		
		return $ids;
		
	}
}
