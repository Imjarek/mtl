<?php

namespace App\Mtl\Provider\HotelBook;

use App\Models\MtlProviderCountry;


class HotelBookHotels extends HotelBookRequest {
	
//	public function __construct (AcaseRequest $request) {
//		
//		$this->request = $request;
//	}
	const PROVIDER_ID = 6;
	protected $path;
	protected $filename = 'HotelList';
	
	public function get() {
		
		$cids = MtlProviderCountry::where(['provider_id' => self::PROVIDER_ID])->pluck('provider_country_id');
		
		foreach ($cids as $country_id) {
		
			$path = base_path('../../bin/dumps/hotelbook/hotels/');

			$this->request = new HotelBookRequest;

			$this->request->method = 'hotel_list';

			$this->request->params = $this->request->params();

			//$this->request->params['city_id'] = $city_id;
			$this->request->params['country_id'] = $country_id;

			$this->request->filename = $path . '/HotelList_' . $country_id;

			//var_dump ($this->request->params);	

			$this->request->response = $this->request->send();

			$this->request->save();
		}
	} 
}

