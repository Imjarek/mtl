<?php

namespace App\Mtl\Provider\HotelBook;

use App\Mtl\Classes\HotelImporter;


class HotelBookHotelImporter extends HotelImporter {
	
	const PROVIDER_ID = 6;
	
	static public function start() {
		
		echo "Загрузка данных отелей [Хотелбук]\n";
		
		self::$source = new HotelBookHotelSource;
		
		self::process();
	}
	
	static function mapParams($item) {
		
		return array(
			'provider_hotel_id' => $item['id'],
			'name_en' => $item['name_en'],
			'name_ru' => $item['name_ru'],
			'provider_id' => self::PROVIDER_ID,
			'provider_city_id' => $item['city'],
			'latitude' => round((float)$item['hotelLatitude'], 5), // слишком точно нам тоже не надо, 8 знаков хватит
			'longitude' => round((float)$item['hotelLongitude'], 5),
			'raw_data' => $item['raw_data']
		);
	}
	
}
