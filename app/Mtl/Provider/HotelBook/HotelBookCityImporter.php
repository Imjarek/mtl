<?php

namespace App\Mtl\Provider\HotelBook;

use App\Mtl\Classes\CityImporter;

use App\Mtl\Classes\ProviderCity;

class HotelBookCityImporter extends CityImporter {
	
	private static $provider_id = 6;
	
	static public function start() {
		
		echo "Загрузка городов [Хотелбук]\n";
		
		self::$source = new HotelBookCitySource;
		
		self::process();
	}
	
	static public function process () {
		
		foreach (self::$source->getData() as $city) {
			
			echo "{$city['name_ru']}\n";
			
			
			$providerCity = new ProviderCity($city, self::$provider_id);
			
			$providerCity->add();
		}
	}
}

