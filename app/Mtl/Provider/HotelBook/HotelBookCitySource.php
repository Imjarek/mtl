<?php

namespace App\Mtl\Provider\HotelBook;

use App\Mtl\Classes\CitySource;

class HotelBookCitySource extends CitySource {
	
	public function getData() {
		
		$xml = simplexml_load_file(base_path().'/../../bin/dumps/hotelbook/CitiesResponse.xml');
	
		// нужно обрабатывать и англ. и русский xml
		foreach($xml->Cities->City as $city) {
			
			$item['provider_city_id'] = (string)$city['id'];
			$item['name_ru'] = (string)$city['name_ru'];
			$item['name_en'] = (string)$city['name_en'];
			$item['provider_country_id'] = (string)$city['country'];
			$item['latitude'] = null;
			$item['longitude'] = null;
			$item['country_code'] = null; // ничего этого у хотелбука к сожалению пока нет(((
			
			if ($pos = mb_strpos($item['name_ru'], ' ('))
				$item['name_ru'] = mb_substr($item['name_ru'], 0, $pos);
			
			yield $item;
		}
	}
}
