<?php

namespace App\Mtl\Utils;

use Illuminate\Support\Facades\DB;

use App\Models\MtlProviderHotel;

//use App\Mtl\Utils\RequestGeocodeByCoords as Geocode;

/*
 * @author Sergey Cherednichenko 2017
 * @email sergey.i.che@gmail.com
 * класс инкапсулирует процесс определения фиктивного provider_hotel_id
 * по названии и координатам с использованием кэша запросов google_place_id
 */
class NormalizeCities {
	
	public static $PROVIDER_ID;
	
	public static function start() {
		
		//$hotels = DB::connection('mtl_tmp')->table('mtl_tmp.google_coords_address')->where('provider_id', self::$PROVIDER_ID)->get();
		
		$hotels = MtlProviderHotel::where('provider_id', self::$PROVIDER_ID)->get();
		
		foreach($hotels as $hotel) {
			
			if (!$hotel->provider_city_id) {
				if ($hotel->latitude && $hotel->longitude)
					self::defineProviderCity($hotel);
			}
		}
	}
	
	private static function defineProviderCity($hotel) {
		// ищем город у это поставщика с расстонием меньше 5 км.

		$city = DB::table('mtl_provider_city')->where('provider_id', self::$PROVIDER_ID)
			->whereRaw("distance(latitude, longitude, {$hotel->latitude}, {$hotel->longitude}) < 10")
			->first();
			
		if ($city) {
		// и записываем его ID в данные отеля
			$hotel->provider_city_id = $city->provider_city_id;
			$hotel->save();
			echo "$city->name_ru\n";
		}
		else
			echo "не найден для отеля № {$hotel->id}\n"; 
		
//		$geocode = new Geocode($hotel);
//		$result = $geocode->get();
			
	}
}
