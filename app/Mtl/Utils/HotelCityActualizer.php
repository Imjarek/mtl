<?php

namespace App\Mtl\Utils;

use Illuminate\Support\Facades\DB;

use App\Models\MtlProviderHotel;

use App\Models\MtlHotel;

use App\Models\MtlProviderCity;

/*
 * Находим отели без указания города и пытаемся исправить
 */
class HotelCityActualizer {
	
	public $PROVIDER_ID;
	
	public function __construct($providerId) {
		$this->PROVIDER_ID = $providerId;
	}
	public function start() {
		
		//$hotels = DB::connection('mtl_tmp')->table('mtl_tmp.google_coords_address')->where('provider_id', self::$PROVIDER_ID)->get();
		
		$hotels = MtlProviderHotel::where('provider_id', $this->PROVIDER_ID)
			->get(['id', 'name_ru', 'provider_hotel_id', 'global_hotel_id', 'provider_city_id']);
		
		foreach($hotels as $providerHotel) {
			
			if ($this->updateCity($providerHotel))
				echo "$providerHotel->name_ru\n";
		}
	}
	private function updateCity ($providerHotel) {
	
		if (!$providerHotel->global_hotel_id)
			return;
		
		$hotel = MtlHotel::find($providerHotel->global_hotel_id);
		
		if (!$hotel || isset($hotel->city_id))
			return;
		
		$hotel->city_id = $this->defineCity($providerHotel->provider_city_id);
		$hotel->save();
		
	}
	private function defineCity($providerHotelId) {
		
		$city = MtlProviderCity::where([
			'provider_id' => $this->PROVIDER_ID,
			'provider_city_id' => $providerHotelId
		])->first();
		
		if ($city)
			return $city->global_city_id;
	}
}
