<?php

namespace App\Mtl\Utils;

use App\Mtl\Repos\ProviderHotelRepo;

use App\Models\MtlLink;
use App\Models\MtlLinkDatum;

/*
 * @author Sergey Cherednichenko 2017
 * @email sergey.i.che@gmail.com
 * @owner IBC Corporate Travel ct.ibc.ru
 * @description Класс содержит вспомогательные операции для работы с отелями)
 */
class Robot {
	
	public function __construct(ProviderHotelRepo $providerHotelRepo) {
		
		$this->providerHotelRepo = $providerHotelRepo;
	}
	/*
	* @description Класс выполняет процедуру поиска похожих отелей, иначе говоря вызывает для отелей пост. 
	* со всеми глобальными отелями в некотором радиусе)
	*/
    function searchSims ($id) {
	
		$hotel = $this->providerHotelRepo->_getById($id);
		
		$data = $hotel->toArray();
		
		$matches = $this->providerHotelRepo->match($hotel, true);
		
		$matches = array_filter($matches, function ($match) {
			
			return ($match['similarity'] >= 0.4);
		});

		$count = count($matches);
		
		$count = $count ? $count : 0;
		
		if ($count > 0) 
			$this->addSims($hotel, $matches);
		
		return array('result' => (bool)$count, 'count' => $count, 'message' => "Поиск произведен. Найдено похожих: {$count}");
	}
	static function addSims($newProviderHotel, $matches) {
		
		// нужно перезаписать линки если уже имеются и
		// и перезаписать данные

		foreach ($matches as $match) {
			
			$link = MtlLink::where([
				'global_id' => $match['hotel']['id'],
				'ref_id' => $newProviderHotel->id
			])->first();
			
			// если уже есть
			if ($link) {
				
				$data = $link->data;
			
				$data->provider_id = $match['provider_id'];
				$data->match_type = 2; // т.е. ручное нахождение похожих
				$data->match_level = $match['similarity'];
				$data->user_id = 1; //т.е. робот

				$data->save();
			}
			else {
			
				$data = new MtlLinkDatum;
				$data->provider_id = $match['provider_id'];
				$data->match_type = 2; // т.е. ручное нахождение похожих
				$data->match_level = $match['similarity'];
				$data->user_id = 1; //т.е. робот

				$data->save();

				$link = new MtlLink;

				$link->global_id = $match['hotel']['id'];
				$link->ref_id = $newProviderHotel->id;
				$link->data_id = $data->id;
				$link->save();
			}
		}
	}
}
