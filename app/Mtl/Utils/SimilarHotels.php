<?php

namespace App\Mtl\Utils;

use App\Mtl\Utils\Robot;

use App\Models\MtlProviderHotel;

/*
 * @author Sergey Cherednichenko 2017
 * @email sergey.i.che@gmail.com
 * @owner IBC Corporate Travel ct.ibc.ru
 * @description Класс выполняет процедуру поиска похожих отелей, иначе говоря вызывает для отелей пост. со всеми глобальными отелями в некотором радиусе)
 */
class SimilarHotels {
		
	private $repo;
	private $robot;
	
	public function __construct(Robot $robot) {
		$this->robot = $robot;
	}
	public function search($providerId = null) {
		
		$hotels = MtlProviderHotel::whereNull('global_hotel_id');
		
		if ($providerId)
			
			$hotels->where('provider_id', $providerId);
	
		$hotels->pluck('id');
		
		foreach ($hotels->pluck('id') as $id) {
			
			$res = $this->robot->searchSims($id);
			
			echo "Для #{$id} найдено: {$res['count']}\n";
		}
	}
}

