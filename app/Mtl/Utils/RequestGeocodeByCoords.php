<?php

namespace App\Mtl\Utils;
/* 
 * Вовращает форм. и адрес google_place_id
 */

class RequestGeocodeByCoords {
	
	public $address;
	public $city;
	
	function __construct($hotel) {
		
		$this->latitude = $hotel->latitude;
		$this->longitude = $hotel->longitude;
	}
	public function get() {
		
		
		$url = "https://maps.googleapis.com/maps/api/geocode/json?latlng={$this->latitude},{$this->longitude}&sensor=true";
		
		$ch = curl_init($url);
				
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		//curl_setopt($ch, CURLOPT_HEADER, TRUE);
		//curl_setopt($ch, CURLOPT_VERBOSE, TRUE); 
		curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, TRUE);

		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array('Accept-Language' => 'ru-ru,ru;q=0.8'));

		//curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		
		$result = json_decode(curl_exec($ch));
		
		curl_close($ch);
		
		if ($result && $result->results)
			return array('geodata' => $result->results[0]);
	}
}