<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mtl\Classes\HotelProcessor;

use App\Mtl\Traits\CmdLoggable;

class MatchHotels extends Command
{
	use CmdLoggable;
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	public $signature = 'mtl:matchHotels {--P|provider= : ID Поставщика, отели которого будут сопоставлены (необязательно)}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	public $description = 'Сопоставление отелей';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(HotelProcessor $hotelProcessor)
	{
		parent::__construct();
		    $this->hotelProcessor = $hotelProcessor;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
	    $this->info('Запуск процедуры сопоставления отелей');
		
		$this->start();
		
		$providerId = $this->option('provider');
		
		if ($providerId) {
			$this->processProviderHotel($providerId);
	    }
		else {
			$this->info('Будут последовательно обработаны все поставщики');
			
			$providers = array(3, 2, 1, 4, 5, 6); // список ID поставщиков для последовательной обработки (организвать передачу из параметра)
			
			foreach($providers as $providerId) {
				$this->processProviderHotel($providerId);
			}
		}
		
		$this->finish();
		
	}
	private function processProviderHotel($providerId) {
		$this->info('Запуск обработки поставщика с ID: ' . $providerId);
		$this->hotelProcessor->start($providerId);
	}
}
