<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mtl\Provider\Ostrovok\OstrovokHotels;

use App\Mtl\Traits\CmdLoggable;

class GetOstrovokHotels extends Command
{
	use CmdLoggable;
	
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	public $signature = 'mtl:getOstrovokHotels';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	public $description = 'Получение JSON с гостиницами Островка';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
       public function __construct(OstrovokHotels $ostrovokHotels)
	{
	       $this->ostrovokHotels = $ostrovokHotels;
		   parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{	
		$this->start();
		$this->ostrovokHotels->get();
		$this->finish();
	}

}
