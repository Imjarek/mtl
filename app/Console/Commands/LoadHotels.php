<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\HotelData;

class LoadHotels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mtl:loadHotels';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Загрузка данных отелей';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(HotelData $hotelData)
    {
        parent::__construct();
		$this->hotelData = $hotelData;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		
		$this->info('Запуск импорта отелей');
		
		$this->hotelData->import();
		
	}
		
}
