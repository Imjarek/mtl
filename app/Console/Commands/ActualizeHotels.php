<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mtl\Provider\Ostrovok\ActualizeOstrovokHotels;
use App\Mtl\Provider\Acase\ActualizeAcaseHotels;
use App\Mtl\Provider\Bronevik\ActualizeBvkHotels;

use App\Mtl\Traits\CmdLoggable;

class ActualizeHotels extends Command

{
	use CmdLoggable;
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'mtl:actualizeHotels {--P|provider= : ID Поставщика, отели которого будут сопоставлены (необязательно)}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Флаг not_exists для отелей которых больше нет в данных поставщиков';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(
		ActualizeAcaseHotels $actualizeAcaseHotels,
		ActualizeOstrovokHotels $actualizeOstrovokHotels,
		ActualizeBvkHotels $actualizeBvkHotels
	 )
	{
		$this->actualizeAcaseHotels = $actualizeAcaseHotels;
		$this->actualizeOstrovokHotels = $actualizeOstrovokHotels;
		$this->actualizeBvkHotels = $actualizeBvkHotels;

		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{	
		
		$this->start();
		
		$providerId = $this->option('provider');
		
		if (!isset($providerId)) {
			// актуализируем по всем
			$this->actualizeBvkHotels->start();
			$this->actualizeAcaseHotels->start();
			$this->actualizeOstrovokHotels->start();
			//$this->actualizeAmadeusHotels->start();
			//$this->actualizeHotelbookHotels->start();
		}
		switch ($providerId) {
			case 1: $this->actualizeOstrovokHotels->start();
			break;
			case 2: $this->actualizeAcaseHotels->start();
			break;
			case 3: $this->actualizeBvkHotels->start();
			break;
			default: $this->info('Укажите правильный ID поставщика');
		}
		
		$this->finish();

	}

}
