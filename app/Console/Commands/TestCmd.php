<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mtl\Traits\CmdLoggable;

class TestCmd extends Command
	
{	
	use CmdLoggable;
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	public $signature = 'mtl:TestCmd';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	public $description = 'Отладочный скрипт';

	/*lasl
	 * Create a new command instance.
	 *
	 * @return void
	 */
       public function __construct()
	{
	    parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->start();
		$this->info('testing log');
		$this->finish();
	}
}
