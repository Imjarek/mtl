<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mtl\Provider\Bol\BolHotels;
use App\Mtl\Traits\CmdLoggable;

class GetBolHotels extends Command
{
	use CmdLoggable;
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	public $signature = 'mtl:getBolHotels';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	public $description = 'Получение JSON с гостиницами BedsOnline';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
       public function __construct(BolHotels $bolHotels)
	{
	       $this->bolHotels = $bolHotels;
		   parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->start();
		$this->bolHotels->get();
		$this->finish();
	}
}
