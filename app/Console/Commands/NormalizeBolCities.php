<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\DataUtil;

use App\Models\MtlProviderHotel;

use App\Mtl\Classes\RequestGeocode; // DRY!

use App\Mtl\Utils\NormalizeCities;

class NormalizeBolCities extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'mtl:normalizeBolCities';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Скрипт проставляет provider_city_id';

	/*lasl
	 * Create a new command instance.
	 *
	 * @return void
	 */
       public function __construct()
	{
	    parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		NormalizeCities::$PROVIDER_ID = 5;
		NormalizeCities::start();
	}
}
