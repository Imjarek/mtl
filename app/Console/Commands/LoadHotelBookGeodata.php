<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mtl\Provider\HotelBook\HotelBookCountryImporter;

use App\Mtl\Provider\HotelBook\HotelBookCityImporter;

class LoadHotelBookGeodata extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mtl:loadHotelBookGeodata';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Загрузка данных стран и городов Хотелбук из дампов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Запуск импорта геоданных Хотелбук');
		HotelBookCountryImporter::start();
		
		$this->info('Запуск импорта городов');
		HotelBookCityImporter::start();
    }
}
