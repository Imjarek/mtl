<?php

namespace App\Console\Commands\Utils;

use Illuminate\Console\Command;

use App\Mtl\Utils\HotelCityActualizer;

/*
 * Реализует заполнение city_id у отелей указанного поставщика
 * Обрабатывает только те у которых он вообще не был указан, возможно надо предусмотреть и обновление
 */
class ActualizeGlobalCity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mtl:actualizeGlobalCity {--P|provider= : ID Поставщика, для которого выполнить актуализацю (обязательно)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Заполнение глобального города у отелей выбранного поставщика.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {	
		$providerId = (int)$this->option('provider');
		
		if (!$providerId)  {
			$this->info('Укажите ID поставщика --provider');
			return;
		}
		
		$process = new HotelCityActualizer($providerId);
		$process->start();
	}
		
}
