<?php

namespace App\Console\Commands\Utils;

use Illuminate\Console\Command;
use App\Mtl\Utils\SimilarHotels;

class SearchSims extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'mtl:searchSimilarHotels {--P|provider= : ID Поставщика, для необработанных отелей которого будет произведен поиск похожих (необязательно)}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Принудительный поиск похожих отелей';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(SimilarHotels $similarHotels)
	{
		parent::__construct();
		    $this->similarHotels = $similarHotels;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
	    $this->info('Запуск поиска похожих отелей');
		
		$providerId = $this->option('provider');
		
		if ($providerId) {
			$this->similarHotels->search($providerId);
	    }
		else {
			$this->info('Будет найдены похожие отели для всех поставщиков');
			
			$providers = array(3, 2, 1, 4, 5, 6); // список ID поставщиков для последовательной обработки (организвать передачу из параметра)
			
			foreach($providers as $providerId) {
				$this->similarHotels->search($providerId);
			}
		}
	}
}
