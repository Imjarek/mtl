<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mtl\Provider\Acase\AcaseHotelImporter;

use App\Mtl\Traits\CmdLoggable;

class LoadAcaseHotels extends Command
{	
	use CmdLoggable;
	
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $signature = 'mtl:loadAcaseHotels';

    /**
     * The console command description.
     *
     * @var string
     */
    public $description = 'Загрузка данных отелей Академ-сервиса';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$this->start();
		AcaseHotelImporter::start();
		$this->finish();
	}
}
