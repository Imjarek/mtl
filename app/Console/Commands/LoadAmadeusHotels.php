<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use \App\Mtl\Provider\Amadeus\AmadeusHotelImporter;

use App\Mtl\Traits\CmdLoggable;

class LoadAmadeusHotels extends Command
{
	use CmdLoggable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mtl:loadAmadeusHotels';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Загрузка данных отелей Амадеус (импорт из БД suppliers)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$this->start();
		
		AmadeusHotelImporter::start();
		
		$this->finish();
	}
		
}
