<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\GeoData;

class LoadGeodata extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mtl:loadGeodata';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Загрузка данных стран и городов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(GeoData $geodata)
    {
        parent::__construct();
		$this->geodata = $geodata;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Запуск импорта стран');
		$this->geodata->importCountries();
		
		$this->info('Запуск импорта городов');
		$this->geodata->importCities();
    }
}
