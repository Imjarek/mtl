<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mtl\Provider\HotelBook\LoadHotelBookHotelData;

class LoadHotelBookHotelDetails extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'mtl:loadHotelBookHotelDetails';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Добавление статических данных в таблицу с отелями HotelBook';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
       public function __construct(LoadHotelBookHotelData $data)
	{
	       $this->data = $data;
		   parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		
		$this->info("Импорт статических данных отелей Хотелбук");
		$this->data->import();
	}

}
