<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mtl\Provider\HotelBook\HotelBookGeodataRequest;

class GetHotelBookGeodata extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'mtl:getHotelBookGeodata';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Получение и сохранение XML с городами и странаме Хотелбука';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
       public function __construct(HotelBookGeodataRequest $geodata)
	{
	       $this->geodata = $geodata;
		   parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{	
		// страны
		$this->geodata->method = 'countries';
		$this->geodata->filename = base_path('../../bin/dumps/hotelbook/CountriesResponse.xml');
		$this->geodata->get();
		$this->geodata->save();
		
		// города
		$this->geodata->method = 'cities';
		$this->geodata->filename = base_path('../../bin/dumps/hotelbook/CitiesResponse.xml');
		$this->geodata->get();
		$this->geodata->save();
		
	}

}
