<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mtl\Provider\HotelBook\HotelBookCountryImporter;

use App\Mtl\Provider\HotelBook\HotelBookCityImporter;

class LoadBolGeodata extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mtl:loadBolGeodata';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Загрузка данных стран и городов BedsOnline (собстенных не имеет используется БРОНЕВИК)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Запуск импорта стран BOL');
		BolCountryImporter::start();
		
		$this->info('Запуск импорта городов BOL');
		BolCityImporter::start();
    }
}
