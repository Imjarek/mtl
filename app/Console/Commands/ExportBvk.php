<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\DataUtil;

use App\Mtl\Traits\CmdLoggable;

class ExportBvk extends Command
{
	use CmdLoggable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $signature = 'mtl:ExportBvk';

    /**
     * The console command description.
     *
     * @var string
     */
    public $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
   public function __construct(DataUtil $utility)
    {
        parent::__construct();
		$this->utility = $utility;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {	
		$this->start();
				
        $this->info("Сохраняем гео-данные Броневика в JSON");
		
		$this->utility->ExportBvkCountries();
		
		$this->utility->ExportBvkCities();
		
		$this->info("Сохраняем данные отелей Броневика в JSON");
		
		$this->utility->ExportBvkHotels();
		
		$this->finish();

    }
}
