<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mtl\Provider\Acase\AcaseHotels;

use App\Mtl\Traits\CmdLoggable;

class GetAcaseHotels extends Command
{	
	use CmdLoggable;
	
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	public $signature = 'mtl:getAcaseHotels';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	public $description = 'Получение XML с гостиницами Академии';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
       public function __construct(AcaseHotels $acaseHotels)
	{
	       $this->acaseHotels = $acaseHotels;
		   parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{	
		$this->start();
		$this->acaseHotels->get();
		$this->finish();
	}
}
