<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use \App\Mtl\Provider\Bol\BolHotelImporter;

use App\Mtl\Traits\CmdLoggable;

class LoadBolHotels extends Command
{	
	use CmdLoggable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mtl:loadBolHotels';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Загрузка данных отелей BedsOnline';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$this->start();
		
		BolHotelImporter::start();
		
		$this->finish();
	}
		
}
