<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mtl\Provider\Ostrovok\OstrovokHotelImporter;

use App\Mtl\Traits\CmdLoggable;

class LoadOstrovokHotels extends Command
{	
	use CmdLoggable;
	
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $signature = 'mtl:loadOstrovokHotels';

    /**
     * The console command description.
     *
     * @var string
     */
    public $description = 'Загрузка данных отелей Островка';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$this->start();
		OstrovokHotelImporter::start();
		$this->finish();
	}
}
