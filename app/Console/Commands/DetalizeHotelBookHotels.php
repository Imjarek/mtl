<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mtl\Provider\HotelBook\HotelBookHotelDetails;

use App\Mtl\Traits\CmdLoggable;

class DetalizeHotelBookHotels extends Command
{
	use CmdLoggable;
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'mtl:getHotelBookHotelDetails';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Загрузка статических данных отелей HotelBook';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
       public function __construct(HotelBookHotelDetails $details)
	{
	       $this->details = $details;
		   parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->start();
		
		$this->details->get();
		
		$this->finish();
	}

}
