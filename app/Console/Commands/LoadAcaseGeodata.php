<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mtl\Provider\Acase\AcaseCountryImporter;

use App\Mtl\Provider\Acase\AcaseCityImporter;


class LoadAcaseGeodata extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mtl:loadAcaseGeodata';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Загрузка данных стран и городов Академсервиса из дампов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Запуск импорта стран Академии');
		 AcaseCountryImporter::start();
		
		$this->info('Запуск импорта городов Академии');
		AcaseCityImporter::start();
    }
}
