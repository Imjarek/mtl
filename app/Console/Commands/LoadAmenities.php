<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\DataUtil;

use App\Models\MtlProviderHotel;

use App\Models\MtlHotel;

use App\Models\MtlHotelAmenity;

use App\Services\AmenityLoader;

class LoadAmenities extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'mtl:loadAmenities';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Загрузка и сопоставление удобств (услуг)';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
       public function __construct(AmenityLoader $amenityLoader)
	{
	       $this->amenityLoader = $amenityLoader;
	       parent::__construct();
	    
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->amenityLoader->start();
	}
//	private function process() {
//		
//		$this->info("Загрузка удобств в отелях поставщиков (услуг)");
//		
//		
////		$xml = simplexml_load_file(base_path().'/../../bin/dumps/acase/HotelAmenityListRequestRU.xml');
////		
////		// нужно обрабатывать и англ. и русский xml
////		foreach($xml->HotelAmenity as $amenity) {
////			
////			echo "{$amenity['Name']}\n";
////			
////			MtlHotelAmenity::firstOrCreate(['name' => $amenity['Name']]);
////			
////		}
//	}
}
