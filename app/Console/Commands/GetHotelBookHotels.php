<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mtl\Provider\HotelBook\HotelBookHotels;

use App\Mtl\Traits\CmdLoggable;

class GetHotelBookHotels extends Command
{
	use CmdLoggable;
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	public $signature = 'mtl:getHotelBookHotels';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	public $description = 'Получение XML с отелями Хотелбука';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
       public function __construct(HotelBookHotels $hotels)
	{
	       $this->hotels = $hotels;
		   parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{	
		$this->start();
		$this->hotels->get();
		$this->finish();
	}
}
