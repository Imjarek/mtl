<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mtl\Classes\Geocoding;

class LoadHotelGeocodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mtl:loadHotelGeocodes {--P|provider= : ID Поставщика, для отели которого будут загружены геокоды (необязательно)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Процедура нормализации адресов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
   public function __construct(Geocoding $geocoding)
    {
        parent::__construct();
		$this->geocoding = $geocoding;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
		
		$providerId = $this->option('provider');
		
        $this->info("Запуск нормализации адресов");
	
	$this->geocoding->make($providerId);
		
	
    }
}
