<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\Reporting;

class LoadHotelsReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mtl:loadHotelsReport {email?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Формирование отчета загрузки отелей';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {		
		$this->reporting = new Reporting();
		parent::__construct();
		
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$this->reporting->email = $this->argument('email') ? $this->argument('email') : config('mtl.email_for_reports');
		
		$this->info('Запуск формирования отчета (получатель '.$this->reporting->email.')');
		
		$this->reporting->send();
    }
}
