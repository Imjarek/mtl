<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
		
		// загрузка дампов с геоданными
		Commands\GetHotelBookGeodata::class,			// запрос геоданных хотелбука (создание дампа)
		//
		// импорт геоданных (и сопоставление)

		Commands\LoadGeodata::class,					// загрузка и сопоставление геообъектов
		Commands\LoadAcaseGeodata::class,				// загрузка стран и городов Академии (дамп получаем вручную)
		Commands\LoadHotelBookGeodata::class,			// загрузка геоднанных хотелбука

														// загрузка дампов с отелями поставщиков

		Commands\GetAcaseHotels::class,					// запрашивает список всех гостиниц Академии
		Commands\GetOstrovokHotels::class,				// запрашивает список всех гостиниц Островка
		Commands\GetBolHotels::class,					// запрашивает список всех гостиниц BOL
		Commands\GetHotelBookHotels::class,				// HotelBook
		
														// импорт данных отелей поставщиков
		Commands\LoadHotels::class,						// загрузка отелей ВСЕХ поставщиков
		Commands\LoadBronevikHotels::class,				// загрузка отелей поставщиков по отдельности
		Commands\LoadAcaseHotels::class,
														//		Commands\LoadOstrovokHotels::class,
		Commands\LoadAmadeusHotels::class,
		Commands\LoadBolHotels::class,					// получение списка отелей хотелбук
		Commands\LoadHotelBookHotels::class,
		Commands\DetalizeHotelBookHotels::class,		// получение статических данных отелей Хотелбук
		Commands\LoadHotelBookHotelDetails::class,		// импорт статических данных отелей Хотелбук
		
		Commands\LoadOstrovokHotels::class,				// импорт отелей Островка
		
														// обработка данных
		Commands\MatchHotels::class,					// сопоставление отелей и создание глобальных отелей
	
														// утилиты
		Commands\Utils\SearchSims::class,				// поиск похожих отелей по всем или по одному пост.
		Commands\LoadHotelGeocodes::class,				// нормализация адресов
		Commands\ActualizeHotels::class,				// актуализация отелей, флаг not_exists
		Commands\NormalizeBolCities::class,
	
		Commands\ExportBvk::class,						// экспорт данных броневика в json
		
		Commands\LoadHotelsReport::class,				// формирование отчет
		
														// в разработке и тестировании
		Commands\LoadAmenities::class,					// загрузка удобств
		Commands\LoadRooms::class,						// загрузка комнат
		Commands\TestCmd::class,						// тестовая
		Commands\MatchArrays::class,					// тестовая
		
		Commands\Utils\ActualizeGlobalCity::class		// актуализация глобального города отеля
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {	
		// локальное и серверное расписание должны отличаться
		// осторожно с периодами - здесь есть ДОЛГИЕ скрипты загружающие и обрабатывающие гигабайты данных
		// случайный множественный запуск которых может вызвать перегрузку
		// поэтому делаем разбежку для еженедельных скриптов
		// 
		//$schedule->command('mtl:TestCmd')->everyMinute(); // проверочная команда
		// запуск обновления данных
		// 
		// Запрос новых данных от поставщиков.
		//
		
		// зарузка данных
		$schedule->command('mtl:getOstrovokHotels')->weekly()->saturdays()->at('00:10'); 
		$schedule->command('mtl:getHotelBookHotels')->weekly()->saturdays()->at('03:10');
		$schedule->command('mtl:getAcaseHotels')->weekly()->saturdays()->at('04:10');
		$schedule->command('mtl:getHotelBookHotels')->weekly()->saturdays()->at('04:10');
		$schedule->command('mtl:getBolHotels')->weekly()->saturdays()->at('06:10');
		$schedule->command('mtl:ExportBvk')->weekly()->saturdays()->at('09:10'); // по факту нужно запрашивать обновление БД
		
		// импорт новых отелей
		$schedule->command('mtl:loadOstrovokHotels')->weekly()->tuesdays()->at('00:10');
		$schedule->command('mtl:loadAcaseHotels')->weekly()->tuesdays()->at('02:10');
		$schedule->command('mtl:loadBronevikHotels')->weekly()->tuesdays()->at('02:10');
		$schedule->command('mtl:loadAmadeusHotels')->weekly()->tuesdays()->at('02:10');
		$schedule->command('mtl:loadHotelBookHotels')->weekly()->tuesdays()->at('02:10');
		$schedule->command('mtl:loadBolHotels')->weekly()->tuesdays()->at('02:10');
		
		// вспомогательные операции
		// обновление геокодов
		$schedule->command('mtl:loadHotelGeocodes -P 1')->weekly()->mondays()->at('00:15');
		$schedule->command('mtl:loadHotelGeocodes -P 2')->weekly()->tuesdays()->at('00:25');
		$schedule->command('mtl:loadHotelGeocodes -P 3')->weekly()->wednesdays()->at('00:35');
		$schedule->command('mtl:loadHotelGeocodes -P 4')->weekly()->thursdays()->at('00:45');
		$schedule->command('mtl:loadHotelGeocodes -P 5')->weekly()->fridays()->at('00:55');
		$schedule->command('mtl:loadHotelGeocodes -P 6')->weekly()->saturdays()->at('01:05');
		
		// специфичные для поставщиков операции
		// 
		$schedule->command('mtl:getHotelBookHotelDetails')->weekly()->saturdays()->at('06:10');
		$schedule->command('mtl:loadHotelBookHotelDetails')->weekly()->sundays()->at('02:10');
		$schedule->command('mtl:normalizeBolCities')->weekly()->mondays()->at('20:30');
		// 
		// сопоставление отелей
		$schedule->command('mtl:matchHotels -P 1')->weekly()->mondays()->at('2:15');
		$schedule->command('mtl:matchHotels -P 2')->weekly()->tuesdays()->at('2:25');
		$schedule->command('mtl:matchHotels -P 3')->weekly()->wednesdays()->at('2:35');
		$schedule->command('mtl:matchHotels -P 4')->weekly()->thursdays()->at('2:45');
		$schedule->command('mtl:matchHotels -P 5')->weekly()->fridays()->at('2:55');
		$schedule->command('mtl:matchHotels -P 6')->weekly()->saturdays()->at('2:05');
		
		// актуализация, пометка удаленных
		$schedule->command('mtl:actualizeHotels -P 1')->weekly()->mondays()->at('22:15');
		$schedule->command('mtl:actualizeHotels -P 2')->weekly()->mondays()->at('23:15');
		$schedule->command('mtl:actualizeHotels -P 3')->weekly()->tuesdays()->at('21:15');
		$schedule->command('mtl:actualizeHotels -P 4')->weekly()->tuesdays()->at('22:15');
		$schedule->command('mtl:actualizeHotels -P 5')->weekly()->tuesdays()->at('23:15');
		$schedule->command('mtl:actualizeHotels -P 6')->weekly()->wednesdays()->at('21:15');
		
		// mtl:getHotelBookHotelDetails
		// Загрузки из данных (обновление)

		// запуск утилит
		
		// запуск всего того что раньше запускали вручную
        //$schedule->command('mtl:TestCmd')->everyMinute(); // проверочная команда
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
