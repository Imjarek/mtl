<?php

return [
	'email_for_reports' => env('EMAIL_FOR_REPORTS', 's.cherednichenko@bronevik.com'),
	'restrict_import' => env('RESTRICT_IMPORT', null),
	'restrict_matching' => env('RESTRICT_MATCHING', null),
	'restrict_import_by_country' => env('RESTRICT_IMPORT_BY_COUNTRY', null),
	'primary_matching' => env('PRIMARY_MATCHING', null)
];
