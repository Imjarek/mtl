<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use App\Models\MtlHotel;

use App\Models\MtlCity;

use App\Models\MtlCountry;

use App\Models\Role;

Route::get('/', function () {
	return redirect('hotels');
});
/**
 * Hotels
 */
Route::get('/hotels', 'HotelController@search');

/**
 * Города
 */
Route::get('/cities', 'CityController@search');

Route::get('city/{id}', 'CityController@show');

/**
 * Страны
 */
Route::get('/countries', 'CountryController@search');

Route::get('/country/{id}', 'CountryController@show');

/**
 * Hotels
 */
/*
 * Hotel Edit
 */

Route::post('/hotel/edit', 'HotelController@edit');
Route::post('/hotel/delete', 'HotelController@delete');
Route::get('/hotel/{id}', 'HotelController@show');


/**
 * Provider Hotels
 */
// создание отеля
Route::post('/provider_hotel/create_hotel', 'HotelController@create');

// связывание с похожим
Route::post('/provider_hotel/link_to_similar', 'LinkController@link');

// связывание с глобальным
Route::post('/provider_hotel/link_to_global', 'ProviderHotelController@link');

Route::get('/provider_hotel/{id}', 'ProviderHotelController@show');



/**
 * Provider Hotels
 */
Route::get('/provider_hotels/', 'ProviderHotelController@search');


/**
 * TEST!
 */

Route::get('/test_provider_hotels/', 'TestProviderHotelController@search');

/**
 * Hotels
 */
Route::post('/search', function (Request $request) {
    //
});

/**
 * Delete An Existing Item
 */
Route::delete('/item/{id}', function ($id) {
    //
});

Route::get('/matches', 'LinkController@get');

Route::get('/match/{id}', 'LinkController@show');

Route::get('/test/prov', 'TestController@testProviders');

Route::get('/test/report', 'TestController@testReport');


Route::get('/amenities', 'AmenityController@get');

Route::post('/unlink', 'LinkController@unlink');



Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/config', function () {
	
	return view('config');
});

Route::get('/union', 'UnionController@show');

Route::post('/union/confirm', 'UnionController@confirmUnion');

Route::post('/union/undo', 'UnionController@undoUnion');

/**
 * Выход и переход к логину
 */
Route::get('logout', function()
{
    Auth::logout();
     
    return redirect('login');
});

/**
 * Ссылка подтверждения е-мейла
 */
Route::get('user/activation/{token}', 'Auth\RegisterController@activateUser')->name('user.activate');


Route::post('/search/sims/', 'RobotController@searchSims');
// actions

Route::get('/addToBuffer/{hotelId}/checked/{state}', 'BufferController@add');

Route::get('/resetBuffer', 'BufferController@reset');


Route::get('/log', 'LogController@show');

// Нижеследующие запросы нужно обернуть в группу и назначить к посреднику role:admin
Route::get('/match_log', ['uses'=> 'MatchLogController@show']);
Route::get('/robot_log', ['uses'=> 'RobotLogController@show']);

Route::get('/pms', 'PMSController@search');

Route::get('/pms/create', 'PMSController@create');

Route::get('/pms/{id}', 'PMSController@show');

Route::get('/pms/edit/{id}', 'PMSController@edit');

/*
 * Отчеты
 * 
 */
Route::get('/reports', 'ReportController@configPage');


Route::get('/reports/request', 'ReportController@request');

Route::get('/reports/download/{file_name}', 'ReportController@download');

Route::group(['namespace' => 'Services'], function () {

    Route::get('services/provider_countries', 'ProviderCountryController@search');
    
    Route::get('services/provider_cities', 'ProviderCityController@search');
    
    Route::get('services/countries', 'CountryController@search');
    
    Route::get('services/cities', 'CityController@search');
    
    Route::get('services/provider_hotels', 'ProviderHotelController@search');
    
    Route::get('services/hotels', 'HotelController@search');
	
	Route::get('services/pms_list', 'PMSController@get');
	
});

// форма запроса динамики от поставщиков
// 
Route::get('request_rates', 'ProviderApiController@requestRatesForm');

Route::post('request_rates', 'ProviderApiController@requestRates');
// запросы динамики от поставщиков

Route::get('_test_api', 'TestController@testProviderApi');

Route::get('test_ostrovok_api', 'ProviderApiController@ostrovok');

Route::get('test_acase_api', 'ProviderApiController@acase');

Route::get('test_hotelbook_api', 'ProviderApiController@hotelbook');

Route::get('test_bol_api', 'ProviderApiController@bol');

Route::post('city/edit', 'CityController@edit');

// 
Route::get('mojo', function () {

	$users = DB::table('users')->get();
	
	foreach ($users as $user) {
		
		$pass = bcrypt($user->password);
		echo "{$user->email} - {$pass} <br>";
	}
	
});



